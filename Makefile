all:
	$(MAKE) -C src all
	if [ ! -d bin ]; then mkdir bin; fi
	if [ ! -d bin/AST ]; then mkdir bin/AST; fi
	if [ ! -d bin/Checker ]; then mkdir bin/Checker; fi
	if [ ! -d bin/Exception ]; then mkdir bin/Exception; fi
	if [ ! -d bin/lib ]; then mkdir bin/lib; fi
	if [ ! -d bin/Parser ]; then mkdir bin/Parser; fi
	if [ ! -d bin/Printer ]; then mkdir bin/Printer; fi
	if [ ! -d bin/Translater ]; then mkdir bin/Translater; fi
	cp src/*.class bin/
	cp src/AST/*.class bin/AST/
	cp src/Checker/*.class bin/Checker/
	cp src/Exception/*.class bin/Exception/
	cp src/Parser/*.class bin/Parser/
	cp src/lib/*.class bin/lib/
	cp src/Printer/*.class bin/Printer/
	cp src/Translater/*.class bin/Translater/
	$(MAKE) -C src clean
clean:
	$(MAKE) -C src clean
	-rm -rf bin
