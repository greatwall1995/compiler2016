package Translater;

import java.util.*;

import AST.ClassDec;
import lib.*;

/**
 * Created by Administrator on 2016/4/5.
 */
public class ClassTable {
	private class Node {
		Symbol type, name;
		Node() {}
		Node(Symbol _t, Symbol _n) {
			type = _t;
			name = _n;
		}
		public String toString() {
			return type.toString() + " " + name.toString();
		}
		private Symbol toSymbol() {
			return Symbol.symbol(toString());
		}
	}
	private static Dictionary dict = new Hashtable<>();
	private static Dictionary name = new Hashtable<>();
	private static Dictionary type = new Hashtable<>();
	public ClassTable() {}
	public void add(Symbol type, Symbol d, int offset) {
		name.put(1, 2);
	}
	public void add(Symbol t, List<ClassDec.Var> list) {
		for (int i = 0; i < list.size(); ++i) {
			dict.put((new Node(t, list.get(i).name)).toSymbol(), i * 4);
			type.put((new Node(t, list.get(i).name)).toSymbol(), list.get(i).type);
		}
		name.put(t, list);
	}
	public int offset(Symbol type, Symbol name) {
		return (int)dict.get((new Node(type, name)).toSymbol());
	}
	public Type getType(Symbol t, Symbol name) {
		return (Type)type.get((new Node(t, name)).toSymbol());
	}
	public List<ClassDec.Var> getList(Symbol type) {
		return (List<ClassDec.Var>)name.get(type);
	}
}
