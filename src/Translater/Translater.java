package Translater;

import AST.*;
import lib.*;
import java.util.*;

/**
 * Created by Administrator on 2016/4/25.
 */


public class Translater {

	private String get_name(String s) {
		int t = s.indexOf('$');
		if (t == -1) return null;
		String name;
		if (t != 0) {
			name = s.substring(t, s.length() - 1);
		}
		else {
			name = s.substring(t);
		}
		return name;
	}

	private class Optimizer {
		List<String[]> code;
		Optimizer() {
			code = new ArrayList<>();
		}
		public void add(String[] aug) {
			code.add(aug);
		}
		private void work() {
			erase_load_store();
			erase_move_store();
			erase_store_load();
			erase_load_load();
			erase_store();
			for (int i = 0;  i < code.size(); ++i) {
				println_aug(code.get(i));
			}
		}
		private void erase_load_store() {
			int pos = -1;
			List<Integer> erase = new ArrayList<>();
			Map<String, Integer> dict = new HashMap<>();
			for (int i = 0; i < code.size(); ++i) {
				String[] aug = code.get(i);
				if (aug[0].equals("lw") || aug[0].equals("lb")) {
					dict.put(aug[1], i);
					dict.put(aug[2], i);
				}
				else if ((aug.length == 1 && !aug[0].equals("syscall")) || aug[0].charAt(0) == 'b' || aug[0].charAt(0) == 'j') {
					pos = i;
				}
				else if (aug[0].equals("sw") || aug[0].equals("sb")) {
					if (dict.containsKey(aug[1]) && dict.containsKey(aug[2])) {
						int p1 = dict.get(aug[1]);
						int p2 = dict.get(aug[2]);
						if (p1 == p2 && p1 > pos) {
							erase.add(p1);
							erase.add(i);
							dict.put(aug[1], -2);
							dict.put(aug[2], -2);
						}
					}
					int t = aug[2].indexOf("$");
					if (t != -1) {
						String name = get_name(aug[2]);

						if (name.equals("$zero") || name.equals("$fp") || name.equals("$ra")
								|| name.equals("$sp") || name.equals("$gp") || name.equals("$v0")
								|| name.equals("$v1") || name.equals("$a0") || name.equals("$a1")) {
							continue;
						}
						dict.put(name, -2);
					}
				}
				else {
					for (int j = 0; j < aug.length; ++j) {
						dict.put(aug[j], -2);
					}
				}
			}
			Collections.sort(erase, Collections.reverseOrder());
			for (int i = 0; i < erase.size(); ++i) {
				int t = erase.get(i);
				code.remove(t);
			}
		}
		private void erase_move_store() {
			int pos = -1;
			List<Integer> erase = new ArrayList<>();
			Map<String, Integer> dict = new HashMap<>();
			for (int i = 0; i < code.size(); ++i) {
				String[] aug = code.get(i);
				if (aug[0].equals("move")) {
					dict.put(aug[1], i);
				}
				else if ((aug.length == 1 && !aug[0].equals("syscall")) || aug[0].charAt(0) == 'b' || aug[0].charAt(0) == 'j') {
					pos = i;
				}
				else if (aug[0].equals("sw") || aug[0].equals("sb")) {
					if (dict.containsKey(aug[1])) {
						String name = get_name(aug[2]);
						if (name.equals("$fp") || name.equals("$sp") || name.equals("$gp")) {
							int p = dict.get(aug[1]);
							if (p > pos) {
								erase.add(i);
								String[] tmp = code.get(p);
								tmp[0] = aug[0];
								tmp[1] = tmp[2];
								tmp[2] = aug[2];
								dict.put(aug[1], -2);
							}
						}
					}
				}
				else {
					for (int j = 0; j < aug.length; ++j) {
						dict.put(aug[j], -2);
					}
				}
			}
			Collections.sort(erase, Collections.reverseOrder());
			for (int i = 0; i < erase.size(); ++i) {
				int t = erase.get(i);
				code.remove(t);
			}
		}
		private void erase_store_load() {
			int pos = -1;
			List<Integer> erase = new ArrayList<>();
			Map<String, Integer> dict = new HashMap<>();
			for (int i = 0; i < code.size(); ++i) {
				String[] aug = code.get(i);
				if (aug[0].equals("sw") || aug[0].equals("sb")) {
					dict.put(aug[1], i);
					dict.put(aug[2], i);
					int t = aug[2].indexOf("$");
					if (t != -1) {
						String name = get_name(aug[2]);

						if (name.equals("$zero") || name.equals("$fp") || name.equals("$ra")
								|| name.equals("$sp") || name.equals("$gp") || name.equals("$v0")
								|| name.equals("$v1") || name.equals("$a0") || name.equals("$a1")) {
							continue;
						}
						dict.put(name, -2);
					}
				}
				else if ((aug.length == 1 && !aug[0].equals("syscall")) || aug[0].charAt(0) == 'b' || aug[0].charAt(0) == 'j') {
					pos = i;
				}
				else if (aug[0].equals("lw") || aug[0].equals("lb")) {
					if (dict.containsKey(aug[1]) && dict.containsKey(aug[2])) {
						int p1 = dict.get(aug[1]);
						int p2 = dict.get(aug[2]);
						if (p1 == p2 && p1 > pos) {
							if (i < code.size() - 1) {
								String nxt = (code.get(i + 1))[0];
							}
							int t = aug[2].indexOf("$");
							if (t != -1) {
								String name = get_name(aug[2]);

								if (name.equals("$zero") || name.equals("$fp") || name.equals("$ra")
										|| name.equals("$sp") || name.equals("$gp") || name.equals("$v0")
										|| name.equals("$v1") || name.equals("$a0") || name.equals("$a1")) {
									erase.add(i);
								}
								dict.put(name, -2);
							}
						}
					}
				}
				else {
					for (int j = 0; j < aug.length; ++j) {
						dict.put(aug[j], -2);
					}
				}
			}
			Collections.sort(erase, Collections.reverseOrder());
			for (int i = 0; i < erase.size(); ++i) {
				int t = erase.get(i);
				code.remove(t);
			}
		}
		private void erase_load_load() {
			int pos = -1;
			List<Integer> erase = new ArrayList<>();
			Map<String, Integer> app = new HashMap<>();
			Map<String, Integer> change = new HashMap<>();
			Map<String, String> mem = new HashMap<>();
			for (int i = 0; i < code.size(); ++i) {
				String[] aug = code.get(i);
				if (aug[0].equals("lw") || aug[0].equals("li")) {
					String name = get_name(aug[2]);
					int t;
					if (change.containsKey(aug[2])) {
						t = change.get(aug[2]);
					}
					else t = -1;
					if (name == null || name.contains("p")) {
						if (aug[1].equals("$v0")) continue;
						String now = aug[1];
						if (mem.containsKey(now)) {
							String m = mem.get(now);
							int p = app.get(now);
							if (m.equals(aug[2]) && p > pos && p > t) {
								erase.add(i);
							}
						}
					}
					mem.put(aug[1], aug[2]);
					app.put(aug[1], i);
				}
				else if ((aug.length == 1 && !aug[0].equals("syscall")) || aug[0].charAt(0) == 'b' || aug[0].charAt(0) == 'j') {
					pos = i;
				}
				else if (aug[0].equals("move") || aug[0].equals("neg") || aug[0].equals("not") || aug.length == 4) {
					app.put(aug[1], -2);
				}
				else if (aug[0].equals("sw")) {
					change.put(aug[2], i);
				}
			}
			Collections.sort(erase, Collections.reverseOrder());
			for (int i = 0; i < erase.size(); ++i) {
				int t = erase.get(i);
				code.remove(t);
			}
		}

		String std(String mem) {
			return std(mem, 0);
		}

		String std(String mem, int h) {
			if (mem.contains("$gp") || !mem.contains("p")) return mem;
			int t = Integer.valueOf(mem.substring(0, mem.indexOf('(')));
			t -= h * fSize;
			if (mem.contains("$fp")) t -= fSize;
			return t + "($sp)";
		}

		private boolean check_store(int id) {
			int end = 0;
			String func = null;
			Map<String, Integer> label = new HashMap<>();
			int[] visit = new int[code.size()];
			for (int i = 0; i < code.size(); ++i) {
				String[] aug = code.get(i);
				visit[i] = 0;
				if (aug.length == 1) {
					if (!aug[0].equals("syscall")) {
						String s = aug[0].substring(0, aug[0].length() - 1);
						if (i < id && s.charAt(0) == '_') func = s;
						label.put(s, i);
					}
				} else if (end == 0 && aug.length == 3
						&& aug[1].equals("$v0") && aug[2].equals("10")) {
					end = i;
				}
			}
			Queue<Integer> que = new ArrayDeque<>();
			Queue<Integer> height = new ArrayDeque<>();
			String addr;
			addr = std(code.get(id)[2]);
			que.add(id + 1);
			height.add(0);
			visit[id] = 1;
			for (int i = 0; i < code.size(); ++i) {
				String[] aug = code.get(i);
				if (aug.length == 2) {
					if (aug[0].equals("jal") && aug[1].equals(func)){
						que.add(i + 1);
						height.add(-1);
					}
				}
			}
			while (!que.isEmpty()) {
				int now = que.poll();
				int h = height.poll();
				while (true) {
					if (visit[now] == 2) break;
					if (now == end) break;
					visit[now]++;
					String[] aug = code.get(now++);
					if (aug.length == 1) continue;
					if (aug[0].equals("j")) {
						que.add(label.get(aug[1]));
						height.add(h);
						break;
					}
					else if (aug[0].equals("jal")) {
						if (h >= 0 || addr.contains("gp")) {
							if (label.containsKey(aug[1])) {
								que.add(label.get(aug[1]));
								height.add(h + 1);
							}
						}
					}
					else if (aug[0].equals("jr")) {
						break;
					}
					else if (aug[0].charAt(0) == 'b') {
						que.add(label.get(aug[aug.length - 1]));
						height.add(h);
					}
					else if (aug[0].equals("lw") || aug[0].equals("lb")) {
						if ((std(aug[2], h)).equals(addr)) {
							return false;
						}
					}
					else if (aug[0].equals("sw") || aug[0].equals("sb")) {
						if (std(aug[2], h).equals(addr)) {
							break;
						}
					}
				}
			}
			return true;
		}

		private void erase_store() {
			int n = code.size();
			for (int i = n - 1; i >= 0; --i) {
				String[] aug = code.get(i);
				if (!aug[0].equals("sw") || !aug[2].contains("p") || aug[1].contains("sp")
						|| aug[1].contains("ra") || aug[1].contains("v")) {
					continue;
				}
				if (check_store(i)) {
					code.remove(i);
				}
			}
		}

		boolean check(String name) {
			String mem = null;
			for (int i = 0; i < code.size(); ++i) {
				String[] aug = code.get(i);
				if (aug[0].equals("lw") || aug[0].equals("sw")) {
					if (aug[1].equals(name)) {
						if (mem == null) mem = aug[2];
						else if (!mem.equals(aug[2])) return false;
					}
				}
			}
			return true;
		}

		void remove(String name) {
			for (int i = code.size() - 1; i >= 0; --i) {
				String[] aug = code.get(i);
				if (aug[0].equals("lw") || aug[0].equals("sw")) {
					if (aug[1].equals(name)) {
						code.remove(i);
					}
				}
			}
		}

		private void println_aug(String[] aug) {
			for (int i = 0; i < aug.length; ++i) {
				if (i != 0) System.out.print(" ");
				System.out.print(aug[i]);
			}
			System.out.println();
		}
	}

	private ClassTable classTable;
	private Register register;
	private SymbolTable symbolTable;

	private int nBlock, nWord;
	private Stack<String> inLoop, outLoop;
	private static Dictionary<Symbol, Type> funcTable = new Hashtable<>();
	private static Map<Symbol, String> string = new HashMap<>();

	private int sPos, nOver;
	private final int fSize = 4000;

	private Optimizer optimizer;
	private static Map<String, String> target = new HashMap<>();
	private String[] reg_name = {"$t0", "$t1", "$t2", "$t3", "$t4", "$t5", "$t6", "$t7", "$t8", "$t9", "$s0", "$s1", "$s2", "$s3", "$s4", "$s5", "$s6", "$s7"};
	private int tot_reg;

	private void println_lw(String s, String addr) {
		String[] aug;
		if (addr.contains("p")) {
			aug = new String[3];
			aug[0] = "lw";
			aug[1] = s;
			aug[2] = addr;
		}
		else {
			int t = addr.indexOf('$');
			String tmp = addr.substring(t, addr.length() - 1);
			println_lw("$a3", register.getAddr(tmp));
			tmp = addr.substring(0, t) + "$a3" + ")";
			aug = new String[3];
			aug[0] = "lw";
			aug[1] = s;
			aug[2] = tmp;
		}
		optimizer.add(aug);
	}

	private void println_sw(String s, String addr) {
		String[] aug;
		if (addr.contains("p")) {
			aug = new String[3];
			aug[0] = "sw";
			aug[1] = s;
			aug[2] = addr;
		}
		else {
			int t = addr.indexOf('$');
			String tmp = addr.substring(t, addr.length() - 1);
			println_lw("$a3", register.getAddr(tmp));
			tmp = addr.substring(0, t) + "$a3" + ")";
			aug = new String[3];
			aug[0] = "sw";
			aug[1] = s;
			aug[2] = tmp;
		}
		optimizer.add(aug);
	}

	private void println(String s) {
		String[] aug = s.split(" ");
		int n = aug.length;
		for (int i = 0; i < n; ++i) {
			String name = get_name(aug[i]);
			if (name == null || name.equals("$zero") || name.equals("$fp") || name.equals("$ra")
					|| name.equals("$sp") || name.equals("$gp") || name.equals("$v0")
					|| name.equals("$v1") || name.equals("$a0") || name.equals("$a1")) {
				continue;
			}
			if (!target.containsKey(name)) {
				target.put(name, reg_name[tot_reg]);
				++tot_reg;
				tot_reg %= 18;
			}
		}
		List<Integer> load, store;
		load = new ArrayList<>();
		store = new ArrayList<>();
		if (aug[0].charAt(0) == 'j' || aug[0].charAt(0) == 'b') {
			for (int i = 1; i < n; ++i) {
				String name = get_name(aug[i]);
				if (name != null) load.add(i);
			}
		}
		else if (aug[0].equals("move")) {
			load.add(2);
			store.add(1);
		}
		else if (aug[0].equals("lw") || aug[0].equals("lb") || aug[0].equals("li")) {
			load.add(2);
			store.add(1);
		}
		else if (aug[0].equals("sw") || aug[0].equals("sb")) {
			load.add(1);
			load.add(2);
		}
		else if (aug[0].equals("neg") || aug[0].equals("not")) {
			load.add(2);
			store.add(1);
		}
		else if (n == 4){
			load.add(2);
			load.add(3);
			store.add(1);
		}
		boolean flag = true;
		if (load.size() == 2) {
			String name1 = get_name(aug[load.get(0)]);
			String name2 = get_name(aug[load.get(1)]);
			if (name1 == null || name2 == null) flag = false;
			else if (name1.equals("$zero") || name1.equals("$fp") || name1.equals("$ra")
					|| name1.equals("$sp") || name1.equals("$gp") || name1.equals("$v0")
					|| name1.equals("$v1") || name1.equals("$a0") || name1.equals("$a1")) {
				flag = false;
			}
			else if (name2.equals("$zero") || name2.equals("$fp") || name2.equals("$ra")
					|| name2.equals("$sp") || name2.equals("$gp") || name2.equals("$v0")
					|| name2.equals("$v1") || name2.equals("$a0") || name2.equals("$a1")) {
				flag = false;
			}
			else if (!target.get(name1).equals(target.get(name2))) {
				flag = false;
			}
		}
		else flag = false;
		// load
		if (flag) {
			String name1, name2;
			name1 = get_name(aug[load.get(0)]);
			name2 = get_name(aug[load.get(1)]);
			if (name1.equals(name2)) {
				println_lw(target.get(name1), register.getAddr(name1));
				flag = false;
			}
			else {
				println_lw(target.get(name1), register.getAddr(name1));
				println_lw("$a3", register.getAddr(name2));
			}
		}
		else {
			for (int i = 0; i < load.size(); ++i) {
				int j = load.get(i);
				int t = aug[j].indexOf("$");
				if (t != -1) {
					String name = get_name(aug[j]);
					if (name.equals("$zero") || name.equals("$fp") || name.equals("$ra")
							|| name.equals("$sp") || name.equals("$gp") || name.equals("$v0")
							|| name.equals("$v1") || name.equals("$a0") || name.equals("$a1")) {
						continue;
					}
					println_lw(target.get(name), register.getAddr(name));
				}
			}
		}
		// operation
		if (flag) {
			//System.out.println("!!!");
			String name2;
			name2 = get_name(aug[load.get(1)]);
			String[] ret = new String[aug.length];
			for (int i = 0; i < aug.length; ++i) {
				int t = aug[i].indexOf("$");
				if (t != -1) {
					String name = get_name(aug[i]);

					if (name.equals("$zero") || name.equals("$fp") || name.equals("$ra")
							|| name.equals("$sp") || name.equals("$gp") || name.equals("$v0")
							|| name.equals("$v1") || name.equals("$a0") || name.equals("$a1")) {
						ret[i] = aug[i];
						continue;
					}
					ret[i] = aug[i].substring(0, t);
					if (!name.equals(name2)) {
						if (t != 0) {
							ret[i] = ret[i] + target.get(name) + ")";
						}
						else ret[i] = ret[i] + target.get(name);
					}
					else {
						if (t != 0) {
							ret[i] = ret[i] + "$a3)";
						}
						else ret[i] = ret[i] + "$a3";
					}
				}
				else ret[i] = aug[i];
			}
			optimizer.add(ret);
		}
		else {
			String[] ret = new String[aug.length];
			for (int i = 0; i < aug.length; ++i) {
				int t = aug[i].indexOf("$");
				if (t != -1) {
					String name = get_name(aug[i]);

					if (name.equals("$zero") || name.equals("$fp") || name.equals("$ra")
							|| name.equals("$sp") || name.equals("$gp") || name.equals("$v0")
							|| name.equals("$v1") || name.equals("$a0") || name.equals("$a1")) {
						ret[i] = aug[i];
						continue;
					}
					ret[i] = aug[i].substring(0, t);
					if (t != 0) {
						ret[i] = ret[i] + target.get(name) + ")";
					} else ret[i] = ret[i] + target.get(name);
				}
				else ret[i] = aug[i];
			}
			optimizer.add(ret);
		}
		// store
		for (int j = 0; j < store.size(); ++j) {
			int i = store.get(j);
			int t = aug[i].indexOf("$");
			if (t != -1) {
				String name = get_name(aug[i]);

				if (name.equals("$zero") || name.equals("$fp") || name.equals("$ra")
						|| name.equals("$sp") || name.equals("$gp") || name.equals("$v0")
						|| name.equals("$v1") || name.equals("$a0") || name.equals("$a1")) {
					continue;
				}
				if (flag) {
					String name2;
					name2 = get_name(aug[load.get(1)]);
					if (name.equals(name2)) {
						println_sw("$a3", register.getAddr(name));
					}
					else {
						println_sw(target.get(name), register.getAddr(name));
					}
				}
				else {
					println_sw(target.get(name), register.getAddr(name));
				}
			}
		}
	}
	public void translate(Prog e) {
		classTable = new ClassTable();
		register = new Register();
		symbolTable = new SymbolTable();
		nBlock = 0;
		nWord = -2;
		inLoop = new Stack<>();
		outLoop = new Stack<>();
		sPos = nOver = 0;
		optimizer = new Optimizer();
		tot_reg = 0;
		System.out.println(".text");
		System.out.println("main:");
		println("_main:");
		println("addi $fp $sp " + (-fSize));
		for (int i = 0; i < e.decList.size(); ++i) {
			visit1(e.decList.get(i));
			find_out_string(e.decList.get(i));
		}
		println("jal _Main");
		println("li $v0 10");
		println("syscall");
		for (int i = 0; i < e.decList.size(); ++i) {
			visit2(e.decList.get(i));
		}
		optimizer.work();
		set_toString();
		set_add_string();
	}
	private void find_out_string(Dec e) {
		if (e instanceof FuncDec) find_out_string(((FuncDec)e).stmt);
		else if (e instanceof VarDec1) find_out_string((VarDec1)e);
	}
	private void find_out_string(Stmt e) {
		if (e instanceof Expr)  find_out_string((Expr) e);
		else if (e instanceof ReturnStmt) find_out_string((ReturnStmt) e);
		else if (e instanceof IfStmt) find_out_string((IfStmt) e);
		else if (e instanceof WhileStmt) find_out_string((WhileStmt) e);
		else if (e instanceof ForStmt) find_out_string((ForStmt) e);
		else if (e instanceof CompoundStmt) find_out_string((CompoundStmt) e);
		else if (e instanceof VarDec2) find_out_string((VarDec2) e);
	}
	private void find_out_string(VarDec1 e) {
		if (e.expr != null) find_out_string(e.expr);
	}
	private void find_out_string(VarDec2 e) {
		if (e.expr != null) find_out_string(e.expr);
	}
	private void find_out_string(Expr e) {
		if (e instanceof BinaryExpr) find_out_string((BinaryExpr) e);
		else if (e instanceof ParenExpr) find_out_string((ParenExpr) e);
		else if (e instanceof StringExpr) find_out_string((StringExpr) e);
		else if (e instanceof FuncExpr) find_out_string((FuncExpr) e);
		else if (e instanceof ClassExpr) find_out_string((ClassExpr) e);
	}
	private void find_out_string(BinaryExpr e) {
		find_out_string(e.left);
		find_out_string(e.right);
	}
	private void find_out_string(ParenExpr e) {
		find_out_string(e.expr);
	}
	private void find_out_string(ClassExpr e) {
		find_out_string(e.left);
	}
	private void find_out_string(StringExpr e) {
		if (string.containsKey(e.value)) return;
		String m = 4 * (nWord--) + "($gp)";
		String r = register.add("string", m);
		++nOver;
		String value = e.value.toString();
		int len = value.length();
		len = len / 4 * 4 + 4;
		println("li $a0 " + len);
		println("li $v0 9");
		println("syscall");
		println("move " + r + " $v0");
		String m_ = 4 * (nWord--) + "($gp)";
		String r_ = register.tadd("string", m_);
		int p = 0;
		for (int i = 0; i < value.length(); ++i) {
			if (value.charAt(i) == '\\') {
				if (value.charAt(i + 1) == 'n') {
					println("li " + r_ + " " + (int)'\n');
				}
				else if (value.charAt(i + 1) == '\"') {
					println("li " + r_ + " " + (int)'\"');
				}
				else if (value.charAt(i + 1) == '\\') {
					println("li " + r_ + " " + (int)'\\');
				}
				println("sb " + r_ + " " + p + "(" + r + ")");
				++i;
				++p;
			}
			else {
				println("li " + r_ + " " + (int)value.charAt(i));
				println("sb " + r_ + " " + p + "(" + r + ")");
				++p;
			}
		}
		println("li " + r_ + " 0");
		println("sb " + r_ + " " + value.length() + "(" + r + ")");
		string.put(e.value, r);
	}
	private void find_out_string(FuncExpr e) {
		for (int i = 0; i < e.param.size(); ++i) {
			find_out_string(e.param.get(i));
		}
	}
	private void find_out_string(ReturnStmt e) {
		if (e.expr != null) find_out_string(e.expr);
	}
	private void find_out_string(IfStmt e) {
		find_out_string(e.cond);
		find_out_string(e.stmt1);
		if (e.stmt2 != null) find_out_string(e.stmt2);
	}
	private void find_out_string(ForStmt e) {
		if (e.exp1 != null) find_out_string(e.exp1);
		if (e.exp2 != null) find_out_string(e.exp2);
		if (e.exp3 != null) find_out_string(e.exp3);
		find_out_string(e.stmt);
	}
	private void find_out_string(WhileStmt e) {
		find_out_string(e.cond);
		find_out_string(e.stmt);
	}
	private void find_out_string(CompoundStmt e) {
		for (int i = 0; i < e.stmt.size(); ++i) {
			find_out_string(e.stmt.get(i));
		}
	}
	private void set_toString() {
		System.out.println("toString:");
		System.out.println("addi $sp $sp " + (-fSize));
		String r = "$t0";
		System.out.println("move " + r + " $a0");
		System.out.println("li $a0 20");
		System.out.println("li $v0 9");
		System.out.println("syscall");
		String r0 = "$t1";
		System.out.println("move " + r0 + " $v0");
		System.out.println("addi " + r0 + " " + r0 + " 1");
		String digit = "$t2";
		String tmp = "$t3";
		int pos = 0;
		int t3 = nBlock++;
		System.out.println("bgez " + r + " L" + t3);
		System.out.println("move $v0 " + r);
		System.out.println("L" + t3 + ":");
		for (int i = 1000000000; i > 0; i /= 10) {
			System.out.println("li " + tmp + " " + i);
			System.out.println("div " + digit + " " + r + " " + tmp);
			System.out.println("abs " + digit + " " + digit);
			System.out.println("addi " + digit + " " + digit + " 48");
			System.out.println("sb " + digit + " " + pos + "(" + r0 + ")");
			if (i != 1) System.out.println("rem " + r + " " + r + " " + tmp);
			++pos;
		}
		System.out.println("li " + digit + " 0");
		System.out.println("sb " + digit + " 10(" + r0 + ")");
		int t1 = nBlock++;
		int t2 = nBlock++;
		String r48 = "$t4";
		System.out.println("li " + r48 + " 48");
		for (int i = 0; i < 9; ++i) {
			System.out.println("lb " + digit + " 0(" + r0 + ")");
			System.out.println("bne " + digit + " " + r48 + " L" + t1);
			System.out.println("addi " + r0 + " " + r0 + " 1");
		}
		System.out.println("L" + t1 + ":");
		System.out.println("bgez $v0 L" + t2);
		System.out.println("addi " + r0 + " " + r0 + " -1");
		System.out.println("li " + digit + " 45");
		System.out.println("sb " + digit + " 0(" + r0 + ")");
		System.out.println("L" + t2 + ":");
		System.out.println("move $v0 " + r0);
		System.out.println("addi $sp $sp " + fSize);
		System.out.println("jr $ra");
	}
	private void set_add_string() {
		System.out.println("add_string:");
		System.out.println("addi $sp $sp " + (-fSize));
		String r2 = "$t0";
		String r3 = "$t1";
		String cnt = "$t2";
		System.out.println("move " + r2 + " $a0");
		System.out.println("move " + r3 + " $a1");
		System.out.println("li " + cnt + " 0");
		String c = "$t3";
		String c1 = "$t4";
		System.out.println("move " + c + " " + r2);
		int tLoop1 = nBlock++;
		int tDone1 = nBlock++;
		System.out.println("L" + tLoop1 + ":");
		System.out.println("lb " + c1 + " 0(" + c + ")");
		System.out.println("beqz " + c1 + " L" + tDone1);
		System.out.println("addi " + cnt + " " + cnt + " 1");
		System.out.println("addi " + c + " " + c + " 1");
		System.out.println("j L" + tLoop1);
		System.out.println("L" + tDone1 + ":");
		System.out.println("move " + c + " " + r3);
		int tLoop2 = nBlock++;
		int tDone2 = nBlock++;
		System.out.println("L" + tLoop2 + ":");
		System.out.println("lb " + c1 + " 0(" + c + ")");
		System.out.println("beqz " + c1 + " L" + tDone2);
		System.out.println("addi " + cnt + " " + cnt + " 1");
		System.out.println("addi " + c + " " + c + " 1");
		System.out.println("j L" + tLoop2);
		System.out.println("L" + tDone2 + ":");
		System.out.println("div " + cnt + " " + cnt + " 4");
		System.out.println("addi " + cnt + " " + cnt + " 1");
		System.out.println("mul " + cnt + " " + cnt + " 4");
		System.out.println("move $a0 " + cnt);
		System.out.println("li $v0 9");
		System.out.println("syscall");
		String tc = "$t6";
		System.out.println("move " + tc + " $v0");
		System.out.println("move " + c + " " + r2);
		int tLoop3 = nBlock++;
		int tDone3 = nBlock++;
		System.out.println("L" + tLoop3 + ":");
		System.out.println("lb " + c1 + " 0(" + c + ")");
		System.out.println("beqz " + c1 + " L" + tDone3);
		System.out.println("sb " + c1 + " 0(" + tc + ")");
		System.out.println("addi " + tc + " " + tc + " 1");
		System.out.println("addi " + c + " " + c + " 1");
		System.out.println("j L" + tLoop3);
		System.out.println("L" + tDone3 + ":");
		System.out.println("move " + c + " " + r3);
		int tLoop4 = nBlock++;
		int tDone4 = nBlock++;
		System.out.println("L" + tLoop4 + ":");
		System.out.println("lb " + c1 + " 0(" + c + ")");
		System.out.println("beqz " + c1 + " L" + tDone4);
		System.out.println("sb " + c1 + " 0(" + tc + ")");
		System.out.println("addi " + tc + " " + tc + " 1");
		System.out.println("addi " + c + " " + c + " 1");
		System.out.println("j L" + tLoop4);
		System.out.println("L" + tDone4 + ":");
		System.out.println("li " + c1 + " 0");
		System.out.println("sb " + c1 + " 0(" + tc + ")");
		System.out.println("addi $sp $sp " + fSize);
		System.out.println("jr $ra");
	}
	private void visit1(Dec e) {
		if (e instanceof VarDec1) visit((VarDec1) e);
		else if (e instanceof ClassDec) visit((ClassDec) e);
	}
	private void visit2(Dec e) {
		if (e instanceof FuncDec) visit((FuncDec) e);
	}
	private void visit(FuncDec e) {
		funcTable.put(e.name, e.type);
		if (e.name.equals("main")) println("_Main:");
		else println("_" + e.name.toString() + ":");
		sPos = register.save.size();
		println("sw $sp 0($fp)");
		println("sw $ra -4($fp)");
		println("move $sp $fp");
		println("addi $fp $sp " + (-fSize));
		nWord = -2 - nOver;

		symbolTable.beginScope();
		for (int i = 0; i < e.param.size(); ++i) {
			Type type = e.param.get(i).type;
			String m = 4 * (nWord--) + "($sp)";
			String r = register.add(type, m);
			symbolTable.put(e.param.get(i).name, r);
			println("lw " + r + " " + m);
		}
		visit(e.stmt);
		symbolTable.endScope();
		println("lw $ra -4($sp)");
		println("lw $sp 0($sp)");
		println("addi $fp $sp " + (-fSize));
		println("jr $ra");
	}
	private void visit(ClassDec e) {
		classTable.add(e.name, e.list);
	}
	private void visit(VarDec1 e) {
		++nOver;
		if (e.type.dms == 0 && (e.type.name.equals("int") || e.type.name.equals("bool"))) {
			String m1 = 4 * (nWord--) + "($gp)";
			String r1 = register.add(e.type, m1);
			symbolTable.put(e.name, r1);
			if (e.expr != null) {
				if (e.expr.type.dms == 0 && e.expr.type.name.equals("int") && e.expr.constant != null) {
					println("li " + r1 + " " + e.expr.constant);
				}
				else {
					String r2 = visit(e.expr);
					println("move " + r1 + " " + r2);
				}
			}
		} else {
			String m1 = 4 * (nWord--) + "($gp)";
			String r1 = register.add(e.type, m1);
			symbolTable.put(e.name, r1);
			if (e.expr != null) {
				if (e.expr.type.dms == 0 && e.expr.type.name.equals("int") && e.expr.constant != null) {
					println("li " + r1 + " " + e.expr.constant);
				}
				else {
					String r2 = visit(e.expr);
					println("move " + r1 + " " + r2);
				}
			}
		}
	}
	private void visit(VarDec2 e) {
		if (e.type.dms == 0 && (e.type.name.equals("int") || e.type.name.equals("bool"))) {
			String m1 = 4 * (nWord--) + "($sp)";
			String r1 = register.add(e.type, m1);
			symbolTable.put(e.name, r1);
			if (e.expr != null) {
				if (e.expr.type.dms == 0 && e.expr.type.name.equals("int") && e.expr.constant != null) {
					println("li " + r1 + " " + e.expr.constant);
				}
				else {
					String r2 = visit(e.expr);
					println("move " + r1 + " " + r2);
				}
			}
		} else {
			String m1 = 4 * (nWord--) + "($sp)";
			String r1 = register.add(e.type, m1);
			symbolTable.put(e.name, r1);
			if (e.expr != null) {
				if (e.expr.type.dms == 0 && e.expr.type.name.equals("int") && e.expr.constant != null) {
					println("li " + r1 + " " + e.expr.constant);
				}
				else {
					String r2 = visit(e.expr);
					println("move " + r1 + " " + r2);
				}
			}
		}
	}
	private void visit(Stmt e) {
		if (e instanceof Expr) {
			if (e instanceof RightExpr) {
				LeftExpr e0 = new LeftExpr();
				e0.expr = ((RightExpr)e).expr;
				e0.opt = ((RightExpr)e).opt;
				visit(e0);
			}
			else visit((Expr) e);
		}
		else if (e instanceof ReturnStmt) visit((ReturnStmt) e);
		else if (e instanceof ContinueStmt) visit((ContinueStmt) e);
		else if (e instanceof BreakStmt) visit((BreakStmt) e);
		else if (e instanceof IfStmt) visit((IfStmt) e);
		else if (e instanceof WhileStmt) visit((WhileStmt) e);
		else if (e instanceof ForStmt) visit((ForStmt) e);
		else if (e instanceof CompoundStmt) visit((CompoundStmt) e);
		else if (e instanceof VarDec2) visit((VarDec2) e);
	}
	private void visit(ReturnStmt e) {
		if (e.expr != null) {
			String r1 = visit(e.expr);
			println("move $v0 " + r1);
		}
		println("lw $ra -4($sp)");
		println("lw $sp 0($sp)");
		println("addi $fp $sp " + (-fSize));
		println("jr $ra");
	}
	private void visit(ContinueStmt e) {
		println("j " + inLoop.peek());
	}
	private void visit(BreakStmt e) {
		println("j " + outLoop.peek());
	}
	private void visit(IfStmt e) {
		int t1 = nBlock++;
		int t2 = nBlock++;
		String opt = "&&";
		if (e.cond instanceof BinaryExpr) {
			if (((BinaryExpr)e.cond).opt.equals("||")) {
				opt = "||";
			}
		}
		Expr now = e.cond;
		List<Expr> list = new ArrayList<>();
		while (now instanceof BinaryExpr) {
			if (((BinaryExpr)now).opt.equals(opt)) {
				list.add(((BinaryExpr)now).left);
				now = ((BinaryExpr)now).right;
			}
			else {
				break;
			}
		}
		list.add(now);

		if (opt.equals("&&")) {
			for (int i = 0; i < list.size(); ++i) {
				Expr e0 = list.get(i);
				int id = 0;
				if (e0 instanceof BinaryExpr) {
					BinaryExpr e1 = (BinaryExpr)e0;
					if (e1.opt.equals("==")) id = 1;
					if (e1.opt.equals("!=")) id = 2;
					if (e1.opt.equals("<")) id = 3;
					if (e1.opt.equals("<=")) id = 4;
					if (e1.opt.equals(">")) id = 5;
					if (e1.opt.equals(">=")) id = 6;
					if (e1.left.type.name.equals("string")) id = 0;
				}
				if (id != 0) {
					BinaryExpr e1 = (BinaryExpr)e0;
					String r1 = visit(e1.left);
					String r2;
					if (e1.right.constant != null) {
						r2 = e1.right.constant;
					}
					else r2 = visit(e1.right);
					if (e.stmt2 != null) {
						switch (id) {
							case 1:
								println("bne " + r1 + " " + r2 + " L_Else" + t1);
								break;
							case 2:
								println("beq " + r1 + " " + r2 + " L_Else" + t1);
								break;
							case 3:
								println("bge " + r1 + " " + r2 + " L_Else" + t1);
								break;
							case 4:
								println("bgt " + r1 + " " + r2 + " L_Else" + t1);
								break;
							case 5:
								println("ble " + r1 + " " + r2 + " L_Else" + t1);
								break;
							case 6:
								println("blt " + r1 + " " + r2 + " L_Else" + t1);
								break;
						}
					}
					else {
						switch (id) {
							case 1:
								println("bne " + r1 + " " + r2 + " L_IfDone" + t1);
								break;
							case 2:
								println("beq " + r1 + " " + r2 + " L_IfDone" + t1);
								break;
							case 3:
								println("bge " + r1 + " " + r2 + " L_IfDone" + t1);
								break;
							case 4:
								println("bgt " + r1 + " " + r2 + " L_IfDone" + t1);
								break;
							case 5:
								println("ble " + r1 + " " + r2 + " L_IfDone" + t1);
								break;
							case 6:
								println("blt " + r1 + " " + r2 + " L_IfDone" + t1);
								break;
						}
					}
				}
				else {
					String r = visit(e0);
					if (e.stmt2 != null) {
						println("beqz " + r + " L_Else" + t1);
					}
					else {
						println("beqz " + r + " L_IfDone" + t1);
					}
				}
			}
			symbolTable.beginScope();
			visit(e.stmt1);
			symbolTable.endScope();
			if (e.stmt2 != null) {
				println("j L_IfDone" + t2);
				println("L_Else" + t1 + ":");
				symbolTable.beginScope();
				visit(e.stmt2);
				symbolTable.endScope();
				println("L_IfDone" + t2 + ":");
			}
			else println("L_IfDone" + t1 + ":");
		}
		else {
			for (int i = 0; i < list.size(); ++i) {
				Expr e0 = list.get(i);
				int id = 0;
				if (e0 instanceof BinaryExpr) {
					BinaryExpr e1 = (BinaryExpr) e0;
					if (e1.opt.equals("==")) id = 1;
					else if (e1.opt.equals("!=")) id = 2;
					else if (e1.opt.equals("<")) id = 3;
					else if (e1.opt.equals("<=")) id = 4;
					else if (e1.opt.equals(">")) id = 5;
					else if (e1.opt.equals(">=")) id = 6;
					if (e1.left.type.name.equals("string")) id = 0;
				}
				if (id != 0) {
					BinaryExpr e1 = (BinaryExpr) e0;
					String r1 = visit(e1.left);
					String r2;
					if (e1.right.constant != null) {
						r2 = e1.right.constant;
					} else r2 = visit(e1.right);
					switch (id) {
						case 1:
							println("beq " + r1 + " " + r2 + " L_Else" + t1);
							break;
						case 2:
							println("bnq " + r1 + " " + r2 + " L_Else" + t1);
							break;
						case 3:
							println("blt " + r1 + " " + r2 + " L_Else" + t1);
							break;
						case 4:
							println("ble " + r1 + " " + r2 + " L_Else" + t1);
							break;
						case 5:
							println("bgt " + r1 + " " + r2 + " L_Else" + t1);
							break;
						case 6:
							println("bge " + r1 + " " + r2 + " L_Else" + t1);
							break;
					}
				} else {
					String r = visit(e0);
					println("bnez " + r + " L_Else" + t1);
				}
			}
			if (e.stmt2 != null) {
				symbolTable.beginScope();
				visit(e.stmt2);
				symbolTable.endScope();
			}
			println("j L_IfDone" + t2);
			println("L_Else" + t1 + ":");
			symbolTable.beginScope();
			visit(e.stmt1);
			symbolTable.endScope();
			println("L_IfDone" + t2 + ":");
		}
	}
	private void visit(WhileStmt e) {
		symbolTable.beginScope();
		int t1 = nBlock++;
		int t2 = nBlock++;
		println("L_While" + t1 + ":");

		int id = 0;
		if (e.cond instanceof BinaryExpr) {
			BinaryExpr e0 = (BinaryExpr)e.cond;
			if (e0.opt.equals("==")) id = 1;
			else if (e0.opt.equals("!=")) id = 2;
			else if (e0.opt.equals("<")) id = 3;
			else if (e0.opt.equals("<=")) id = 4;
			else if (e0.opt.equals(">")) id = 5;
			else if (e0.opt.equals(">=")) id = 6;
		}
		if (id != 0) {
			BinaryExpr e0 = (BinaryExpr)e.cond;
			String r1 = visit(e0.left);
			String r2 = visit(e0.right);
			switch(id) {
				case 1: println("bne " + r1 + " " + r2 + " L_WhileDone" + t2); break;
				case 2: println("beq " + r1 + " " + r2 + " L_WhileDone" + t2); break;
				case 3: println("bge " + r1 + " " + r2 + " L_WhileDone" + t2); break;
				case 4: println("bgt " + r1 + " " + r2 + " L_WhileDone" + t2); break;
				case 5: println("ble " + r1 + " " + r2 + " L_WhileDone" + t2); break;
				case 6: println("blt " + r1 + " " + r2 + " L_WhileDone" + t2); break;
			}
		}
		if (id == 0) {
			String r = visit(e.cond);
			println("beqz " + r + " L_WhileDone" + t2);
		}

		inLoop.push("L_While" + t1);
		outLoop.push("L_WhileDone" + t2);
		symbolTable.beginScope();
		visit(e.stmt);
		symbolTable.endScope();
		println("j L_While" + t1);
		println("L_WhileDone" + t2 + ":");
		symbolTable.endScope();
		inLoop.pop();
		outLoop.pop();
	}
	private void visit(ForStmt e) {
		symbolTable.beginScope();
		if (e.exp1 != null) visit(e.exp1);
		int t1 = nBlock++;
		int t2 = nBlock++;
		println("L_For" + t1 + ":");
		if (e.exp2 != null) {
			int id = 0;	// == != < <= > >=
			if (e.exp2 instanceof BinaryExpr) {
				BinaryExpr e0 = (BinaryExpr)e.exp2;
				if (e0.opt.equals("==")) id = 1;
				else if (e0.opt.equals("!=")) id = 2;
				else if (e0.opt.equals("<")) id = 3;
				else if (e0.opt.equals("<=")) id = 4;
				else if (e0.opt.equals(">")) id = 5;
				else if (e0.opt.equals(">=")) id = 6;

			}
			if (id != 0) {
				BinaryExpr e0 = (BinaryExpr)e.exp2;
				String r1 = visit(e0.left);
				String r2 = visit(e0.right);
				switch(id) {
					case 1: println("bne " + r1 + " " + r2 + " L_For" + t2); break;
					case 2: println("beq " + r1 + " " + r2 + " L_For" + t2); break;
					case 3: println("bge " + r1 + " " + r2 + " L_For" + t2); break;
					case 4: println("bgt " + r1 + " " + r2 + " L_For" + t2); break;
					case 5: println("ble " + r1 + " " + r2 + " L_For" + t2); break;
					case 6: println("blt " + r1 + " " + r2 + " L_For" + t2); break;
				}
			}
			if (id == 0) {
				String r = visit(e.exp2);
				println("beqz " + r + " L_For" + t2);
			}
		}
		inLoop.push("L_For" + t1);
		outLoop.push("L_For" + t2);
		symbolTable.beginScope();
		visit(e.stmt);
		symbolTable.endScope();
		if (e.exp3 != null) {
			if (e.exp3 instanceof RightExpr) {
				LeftExpr exp = new LeftExpr();
				exp.expr = ((RightExpr)e.exp3).expr;
				exp.opt = ((RightExpr)e.exp3).opt;
				visit(exp);
			}
			else {
				visit(e.exp3);
			}
		}
		println("j L_For" + t1);
		println("L_For" + t2 + ":");
		symbolTable.endScope();
		inLoop.pop();
		outLoop.pop();
	}
	private void visit(CompoundStmt e) {
		symbolTable.beginScope();
		for (int i = 0; i < e.stmt.size(); ++i) {
			visit(e.stmt.get(i));
		}
		symbolTable.endScope();
	}
	private String visit(Expr e) {
		if (e instanceof BinaryExpr) return visit((BinaryExpr) e);
		else if (e instanceof LeftExpr) return visit((LeftExpr) e);
		else if (e instanceof RightExpr) return visit((RightExpr) e);
		else if (e instanceof ClassExpr) return visit((ClassExpr) e);
		else if (e instanceof ArrayExpr) return visit((ArrayExpr) e);
		else if (e instanceof CreationExpr) return visit((CreationExpr) e);
		else if (e instanceof ParenExpr) return visit((ParenExpr) e);
		else if (e instanceof NullExpr) return visit((NullExpr) e);
		else if (e instanceof StringExpr) return visit((StringExpr) e);
		else if (e instanceof IntExpr) return visit((IntExpr) e);
		else if (e instanceof BoolExpr) return visit((BoolExpr) e);
		else if (e instanceof FuncExpr) return visit((FuncExpr) e);
		else if (e instanceof VariableExpr) return visit((VariableExpr) e);
		return "err";
	}
	private String visit(BinaryExpr e) {
		if (e.opt.equals("=")) {
			String r1 = visit(e.left);
			if (e.type.dms == 0 && e.type.name.equals("int") && e.right.constant != null) {
				println("li " + r1 + " " + e.right.constant);
				return r1;
			}
			else {
				boolean flag;
				if (e.right instanceof BinaryExpr) {
					if (((BinaryExpr)e.right).opt.equals("+")) {
						if (((BinaryExpr)e.right).left instanceof VariableExpr) {
							VariableExpr e2 = (VariableExpr)((BinaryExpr)e.right).left;
							if (e.left instanceof VariableExpr) {
								VariableExpr e1 = (VariableExpr)e.left;
								if (e1.name.equals(e2.name)) {
									flag = true;
								}
								else flag = false;
							}
							else flag = false;
						}
						else flag = false;
					}
					else flag = false;
				}
				else flag = false;
				if (flag) {
					String r2;
					if (((BinaryExpr)e.right).right.constant != null) {
						r2 =((BinaryExpr)e.right).right.constant;
					} else {
						r2 = visit(((BinaryExpr)e.right).right);
					}
					println("add " + r1 + " " + r1 + " " + r2);
				}
				else {
					String r2 = visit(e.right);
					println("move " + r1 + " " + r2);
				}
				if (r1.charAt(1) == 't') {
					println("sw " + r1 + " " + register.getAddr(r1));
				}
				return r1;
			}
		}
		else {
			String r2 = visit(e.left);
			Type t = register.getType(r2);
			String m1 = 4 * (nWord--) + "($sp)";
			String r1 = null;
			if (!(t.name.equals("string") && t.dms == 0) && e.opt.equals("==")) {
				String r3 = visit(e.right);
				r1 = register.tadd("bool", m1);
				println("beq " + r2 + " " + r3 + " L" + nBlock);
				println("li " + r1 + " 0");
				println("j L" + (nBlock + 1));
				println("L" + (nBlock++) + ":");
				println("li " + r1 + " 1");
				println("L" + (nBlock++) + ":");
				return r1;
			}
			else if (!(t.name.equals("string") && t.dms == 0) && e.opt.equals("!=")) {
				String r3 = visit(e.right);
				r1 = register.tadd("bool", m1);
				println("bne " + r2 + " " + r3 + " L" + nBlock);
				println("li " + r1 + " 0");
				println("j L" + (nBlock + 1));
				println("L" + (nBlock++) + ":");
				println("li " + r1 + " 1");
				println("L" + (nBlock++) + ":");
				return r1;
			}
			else if (t.name.equals("int")) {
				String r3;
				if (e.right.constant != null) {
					r3 = e.right.constant;
				}
				else r3 = visit(e.right);
				if (e.opt.equals("+")) {
					r1 = register.tadd(t, m1);
					println("add " + r1 + " " + r2 + " " + r3);
				}
				else if (e.opt.equals("-")) {
					r1 = register.tadd(t, m1);
					println("sub " + r1 + " " + r2 + " " + r3);
				}
				else if (e.opt.equals("*")) {
					r1 = register.tadd(t, m1);
					println("mul " + r1 + " " + r2 + " " + r3);
				}
				else if (e.opt.equals("/")) {
					r1 = register.tadd(t, m1);
					println("div " + r1 + " " + r2 + " " + r3);
				}
				else if (e.opt.equals("%")) {
					r1 = register.tadd(t, m1);
					println("rem " + r1 + " " + r2 + " " + r3);
				}
				else if (e.opt.equals("<<")) {
					r1 = register.tadd(t, m1);
					println("sll " + r1 + " " + r2 + " " + r3);
				}
				else if (e.opt.equals(">>")) {
					r1 = register.tadd(t, m1);
					println("srl " + r1 + " " + r2 + " " + r3);
				}
				else if (e.opt.equals("&")) {
					r1 = register.tadd(t, m1);
					println("and " + r1 + " " + r2 + " " + r3);
				}
				else if (e.opt.equals("|")) {
					r1 = register.tadd(t, m1);
					println("or " + r1 + " " + r2 + " " + r3);
				}
				else if (e.opt.equals("^")) {
					r1 = register.tadd(t, m1);
					println("xor " + r1 + " " + r2 + " " + r3);
				}
				else if (e.opt.equals("<")) {
					r1 = register.tadd("bool", m1);
					println("slt " + r1 + " " + r2 + " " + r3);
				}
				else if (e.opt.equals("<=")) {
					r1 = register.tadd("bool", m1);
					println("sle " + r1 + " " + r2 + " " + r3);
				}
				else if (e.opt.equals(">=")) {
					r1 = register.tadd("bool", m1);
					println("sge " + r1 + " " + r2 + " " + r3);
				}
				else if (e.opt.equals(">")) {
					r1 = register.tadd("bool", m1);
					println("sgt " + r1 + " " + r2 + " " + r3);
				}
				return r1;
			}
			else if (t.name.equals("bool")) {
				r1 = register.tadd("bool", m1);
				if (e.opt.equals("&&")) {
					int tb1 = nBlock++;
					int tb2 = nBlock++;
					int tb3 = nBlock++;
					println("beqz " + r2 + " " + "L" + tb1);
					String r3 = visit(e.right);
					println("bnez " + r3 + " " + "L" + tb2);
					println("L" + tb1 + ":");
					println("li " + r1 + " 0");
					println("j L" + tb3);
					println("L" + tb2 + ":");
					println("li " + r1 + " 1");
					println("L" + tb3 + ":");
					return r1;
				}
				else if (e.opt.equals("||")) {
					int tb1 = nBlock++;
					int tb2 = nBlock++;
					int tb3 = nBlock++;
					println("bnez " + r2 + " " + "L" + tb1);
					String r3 = visit(e.right);
					println("beqz " + r3 + " " + "L" + tb2);
					println("L" + tb1 + ":");
					println("li " + r1 + " 1");
					println("j L" + tb3);
					println("L" + tb2 + ":");
					println("li " + r1 + " 0");
					println("L" + tb3 + ":");
					return r1;
				}
				else {
					System.err.println("Unknown operation!");
					return "err";
				}
			}
			else if (t.name.equals("string")) {
				String r3 = visit(e.right);
				if (e.opt.equals("+")) {
					println("move $a0 " + r2);
					println("move $a1 " + r3);
					for (int i = 0; i < 7; ++i) {
						if (i >= register.temp.size()) break;
						println("sw $t" + i + " " + register.getAddr("$t" + i));
					}
					println("jal add_string");
					for (int i = 0; i < 7; ++i) {
						if (i >= register.temp.size()) break;
						println("lw $t" + i + " " + register.getAddr("$t" + i));
					}
					r1 = register.tadd("string", m1);
					println("move " + r1 + " $v0");
					return r1;
				}
				else if (e.opt.equals("==")) {
					r1 = register.tadd("bool", m1);
					String mc1 = 4 * (nWord--) + "($sp)";
					String c1 = register.tadd("char", mc1);
					String mc2 = 4 * (nWord--) + "($sp)";
					String c2 = register.tadd("char", mc2);
					String mnow1 = 4 * (nWord--) + "($sp)";
					String now1 = register.tadd("char", mnow1);
					String mnow2 = 4 * (nWord--) + "($sp)";
					String now2 = register.tadd("char", mnow2);
					println("move " + c1 + " " + r2);
					println("lb " + now1 + " 0(" + c1 + ")");
					println("move " + c2 + " " + r3);
					println("lb " + now2 + " 0(" + c2 + ")");
					int tLoop = nBlock++;
					int tDone = nBlock++;
					println("L" + tLoop + ":");
					String mtest = 4 * (nWord--) + "($sp)";
					String test = register.tadd("char", mtest);
					int tIf1 = nBlock++;
					int tIf2 = nBlock++;
					int tIf3 = nBlock++;
					println("beqz " + now1 + " L" + tIf1);
					println("bnez " + now2 + " L" + tIf2);
					println("L" + tIf1 + ":");
					println("li " + test + " 0");
					println("j L" + tIf3);
					println("L" + tIf2 + ":");
					println("li " + test + " 1");
					println("L" + tIf3 + ":");
					println("beqz " + test + " L" + tDone);
					println("bne " + now1 + " " + now2 + " L" + tDone);
					println("addi " + c1 + " " + c1 + " 1");
					println("addi " + c2 + " " + c2 + " 1");
					println("lb " + now1 + " 0(" + c1 + ")");
					println("lb " + now2 + " 0(" + c2 + ")");
					println("j L" + tLoop);
					println("L" + tDone + ":");
					int tRes0 = nBlock++;
					int tRes1 = nBlock++;
					int tResDone = nBlock++;
					println("bne " + now1 + " " + now2 + " L" + tRes0);
					println("j L" + tRes1);
					println("L" + tRes0 + ":");
					println("li " + r1 + " 0");
					println("j L" + tResDone);
					println("L" + tRes1 + ":");
					println("li " + r1 + " 1");
					println("L" + tResDone + ":");
					return r1;
				}
				else if (e.opt.equals("!=")) {
					r1 = register.tadd("bool", m1);
					String mc1 = 4 * (nWord--) + "($sp)";
					String c1 = register.tadd("char", mc1);
					String mc2 = 4 * (nWord--) + "($sp)";
					String c2 = register.tadd("char", mc2);
					String mnow1 = 4 * (nWord--) + "($sp)";
					String now1 = register.tadd("char", mnow1);
					String mnow2 = 4 * (nWord--) + "($sp)";
					String now2 = register.tadd("char", mnow2);
					println("move " + c1 + " " + r2);
					println("lb " + now1 + " 0(" + c1 + ")");
					println("move " + c2 + " " + r3);
					println("lb " + now2 + " 0(" + c2 + ")");
					int tLoop = nBlock++;
					int tDone = nBlock++;
					println("L" + tLoop + ":");
					String mtest = 4 * (nWord--) + "($sp)";
					String test = register.tadd("char", mtest);
					int tIf1 = nBlock++;
					int tIf2 = nBlock++;
					int tIf3 = nBlock++;
					println("beqz " + now1 + " L" + tIf1);
					println("bnez " + now2 + " L" + tIf2);
					println("L" + tIf1 + ":");
					println("li " + test + " 0");
					println("j L" + tIf3);
					println("L" + tIf2 + ":");
					println("li " + test + " 1");
					println("L" + tIf3 + ":");
					println("beqz " + test + " L" + tDone);
					println("bne " + now1 + " " + now2 + " L" + tDone);
					println("addi " + c1 + " " + c1 + " 1");
					println("addi " + c2 + " " + c2 + " 1");
					println("lb " + now1 + " 0(" + c1 + ")");
					println("lb " + now2 + " 0(" + c2 + ")");
					println("j L" + tLoop);
					println("L" + tDone + ":");
					int tRes1 = nBlock++;
					int tRes0 = nBlock++;
					int tResDone = nBlock++;
					println("bne " + now1 + " " + now2 + " L" + tRes1);
					println("j L" + tRes0);
					println("L" + tRes1 + ":");
					println("li " + r1 + " 1");
					println("j L" + tResDone);
					println("L" + tRes0 + ":");
					println("li " + r1 + " 0");
					println("L" + tResDone + ":");
					return r1;
				}
				else if (e.opt.equals("<")) {
					r1 = register.tadd("bool", m1);
					String mc1 = 4 * (nWord--) + "($sp)";
					String c1 = register.tadd("char", mc1);
					String mc2 = 4 * (nWord--) + "($sp)";
					String c2 = register.tadd("char", mc2);
					String mnow1 = 4 * (nWord--) + "($sp)";
					String now1 = register.tadd("char", mnow1);
					String mnow2 = 4 * (nWord--) + "($sp)";
					String now2 = register.tadd("char", mnow2);
					println("move " + c1 + " " + r2);
					println("lb " + now1 + " 0(" + c1 + ")");
					println("move " + c2 + " " + r3);
					println("lb " + now2 + " 0(" + c2 + ")");
					int tLoop = nBlock++;
					int tDone = nBlock++;
					println("L" + tLoop + ":");
					String mtest = 4 * (nWord--) + "($sp)";
					String test = register.tadd("char", mtest);
					int tIf1 = nBlock++;
					int tIf2 = nBlock++;
					int tIf3 = nBlock++;
					println("beqz " + now1 + " L" + tIf1);
					println("bnez " + now2 + " L" + tIf2);
					println("L" + tIf1 + ":");
					println("li " + test + " 0");
					println("j L" + tIf3);
					println("L" + tIf2 + ":");
					println("li " + test + " 1");
					println("L" + tIf3 + ":");
					println("beqz " + test + " L" + tDone);
					println("bne " + now1 + " " + now2 + " L" + tDone);
					println("addi " + c1 + " " + c1 + " 1");
					println("addi " + c2 + " " + c2 + " 1");
					println("lb " + now1 + " 0(" + c1 + ")");
					println("lb " + now2 + " 0(" + c2 + ")");
					println("j L" + tLoop);
					println("L" + tDone + ":");
					int tRes1 = nBlock++;
					int tRes0 = nBlock++;
					int tResDone = nBlock++;
					println("blt " + now1 + " " + now2 + " L" + tRes1);
					println("j L" + tRes0);
					println("L" + tRes1 + ":");
					println("li " + r1 + " 1");
					println("j L" + tResDone);
					println("L" + tRes0 + ":");
					println("li " + r1 + " 0");
					println("L" + tResDone + ":");
					return r1;
				}
				else if (e.opt.equals("<=")) {
					r1 = register.tadd("bool", m1);
					String mc1 = 4 * (nWord--) + "($sp)";
					String c1 = register.tadd("char", mc1);
					String mc2 = 4 * (nWord--) + "($sp)";
					String c2 = register.tadd("char", mc2);
					String mnow1 = 4 * (nWord--) + "($sp)";
					String now1 = register.tadd("char", mnow1);
					String mnow2 = 4 * (nWord--) + "($sp)";
					String now2 = register.tadd("char", mnow2);
					println("move " + c1 + " " + r2);
					println("lb " + now1 + " 0(" + c1 + ")");
					println("move " + c2 + " " + r3);
					println("lb " + now2 + " 0(" + c2 + ")");
					int tLoop = nBlock++;
					int tDone = nBlock++;
					println("L" + tLoop + ":");
					String mtest = 4 * (nWord--) + "($sp)";
					String test = register.tadd("char", mtest);
					int tIf1 = nBlock++;
					int tIf2 = nBlock++;
					int tIf3 = nBlock++;
					println("beqz " + now1 + " L" + tIf1);
					println("bnez " + now2 + " L" + tIf2);
					println("L" + tIf1 + ":");
					println("li " + test + " 0");
					println("j L" + tIf3);
					println("L" + tIf2 + ":");
					println("li " + test + " 1");
					println("L" + tIf3 + ":");
					println("beqz " + test + " L" + tDone);
					println("bne " + now1 + " " + now2 + " L" + tDone);
					println("addi " + c1 + " " + c1 + " 1");
					println("addi " + c2 + " " + c2 + " 1");
					println("lb " + now1 + " 0(" + c1 + ")");
					println("lb " + now2 + " 0(" + c2 + ")");
					println("j L" + tLoop);
					println("L" + tDone + ":");
					int tRes0 = nBlock++;
					int tRes1 = nBlock++;
					int tResDone = nBlock++;
					println("blt " + now2 + " " + now1 + " L" + tRes0);
					println("j L" + tRes1);
					println("L" + tRes0 + ":");
					println("li " + r1 + " 0");
					println("j L" + tResDone);
					println("L" + tRes1 + ":");
					println("li " + r1 + " 1");
					println("L" + tResDone + ":");
					return r1;
				}
				else if (e.opt.equals(">")) {
					r1 = register.tadd("bool", m1);
					String mc1 = 4 * (nWord--) + "($sp)";
					String c1 = register.tadd("char", mc1);
					String mc2 = 4 * (nWord--) + "($sp)";
					String c2 = register.tadd("char", mc2);
					String mnow1 = 4 * (nWord--) + "($sp)";
					String now1 = register.tadd("char", mnow1);
					String mnow2 = 4 * (nWord--) + "($sp)";
					String now2 = register.tadd("char", mnow2);
					println("move " + c1 + " " + r2);
					println("lb " + now1 + " 0(" + c1 + ")");
					println("move " + c2 + " " + r3);
					println("lb " + now2 + " 0(" + c2 + ")");
					int tLoop = nBlock++;
					int tDone = nBlock++;
					println("L" + tLoop + ":");
					String mtest = 4 * (nWord--) + "($sp)";
					String test = register.tadd("char", mtest);
					int tIf1 = nBlock++;
					int tIf2 = nBlock++;
					int tIf3 = nBlock++;
					println("beqz " + now1 + " L" + tIf1);
					println("bnez " + now2 + " L" + tIf2);
					println("L" + tIf1 + ":");
					println("li " + test + " 0");
					println("j L" + tIf3);
					println("L" + tIf2 + ":");
					println("li " + test + " 1");
					println("L" + tIf3 + ":");
					println("beqz " + test + " L" + tDone);
					println("bne " + now1 + " " + now2 + " L" + tDone);
					println("addi " + c1 + " " + c1 + " 1");
					println("addi " + c2 + " " + c2 + " 1");
					println("lb " + now1 + " 0(" + c1 + ")");
					println("lb " + now2 + " 0(" + c2 + ")");
					println("j L" + tLoop);
					println("L" + tDone + ":");
					int tRes1 = nBlock++;
					int tRes0 = nBlock++;
					int tResDone = nBlock++;
					println("blt " + now2 + " " + now1 + " L" + tRes1);
					println("j L" + tRes0);
					println("L" + tRes1 + ":");
					println("li " + r1 + " 1");
					println("j L" + tResDone);
					println("L" + tRes0 + ":");
					println("li " + r1 + " 0");
					println("L" + tResDone + ":");
					return r1;
				}
				else if (e.opt.equals(">=")) {
					r1 = register.tadd("bool", m1);
					String mc1 = 4 * (nWord--) + "($sp)";
					String c1 = register.tadd("char", mc1);
					String mc2 = 4 * (nWord--) + "($sp)";
					String c2 = register.tadd("char", mc2);
					String mnow1 = 4 * (nWord--) + "($sp)";
					String now1 = register.tadd("char", mnow1);
					String mnow2 = 4 * (nWord--) + "($sp)";
					String now2 = register.tadd("char", mnow2);
					println("move " + c1 + " " + r2);
					println("lb " + now1 + " 0(" + c1 + ")");
					println("move " + c2 + " " + r3);
					println("lb " + now2 + " 0(" + c2 + ")");
					int tLoop = nBlock++;
					int tDone = nBlock++;
					println("L" + tLoop + ":");
					String mtest = 4 * (nWord--) + "($sp)";
					String test = register.tadd("char", mtest);
					int tIf1 = nBlock++;
					int tIf2 = nBlock++;
					int tIf3 = nBlock++;
					println("beqz " + now1 + " L" + tIf1);
					println("bnez " + now2 + " L" + tIf2);
					println("L" + tIf1 + ":");
					println("li " + test + " 0");
					println("j L" + tIf3);
					println("L" + tIf2 + ":");
					println("li " + test + " 1");
					println("L" + tIf3 + ":");
					println("beqz " + test + " L" + tDone);
					println("bne " + now1 + " " + now2 + " L" + tDone);
					println("addi " + c1 + " " + c1 + " 1");
					println("addi " + c2 + " " + c2 + " 1");
					println("lb " + now1 + " 0(" + c1 + ")");
					println("lb " + now2 + " 0(" + c2 + ")");
					println("j L" + tLoop);
					println("L" + tDone + ":");
					int tRes0 = nBlock++;
					int tRes1 = nBlock++;
					int tResDone = nBlock++;
					println("blt " + now1 + " " + now2 + " L" + tRes0);
					println("j L" + tRes1);
					println("L" + tRes0 + ":");
					println("li " + r1 + " 0");
					println("j L" + tResDone);
					println("L" + tRes1 + ":");
					println("li " + r1 + " 1");
					println("L" + tResDone + ":");
					return r1;
				}
				return "err";
			}
			else {
				System.err.println("Unknown operation!");
				return "err";
			}
		}
	}
	private String visit(LeftExpr e) {	// has bug
		String r = visit(e.expr);
		if (e.opt.equals("~")) {
			println("not " + r + " " + r);
			return r;
		}
		else if (e.opt.equals("++")) {
			println("addi " + r + " " + r + " " + 1);
			if (!(e.expr instanceof VariableExpr)) {
				println("sw " + r + " " + register.getAddr(r));
			}
			return r;
		}
		else if (e.opt.equals("--")) {
			println("addi " + r + " " + r + " " + -1);
			if (!(e.expr instanceof VariableExpr)) {
				println("sw " + r + " " + register.getAddr(r));
			}
			return r;
		}
		else if (e.opt.equals("!")) {
			println("xori " + r + " " + r + " 1");
			return r;
		}
		else if (e.opt.equals("+")) {
			return r;
		}
		else if (e.opt.equals("-")) {
			println("neg " + r + " " + r);
			return r;
		}
		else {
			System.err.println("Unknown operation!");
			return "err";
		}
	}
	private String visit(RightExpr e) {
		String r1 = visit(e.expr);
		Type t = register.getType(r1);
		String m2 = 4 * (nWord--) + "($sp)";
		String r2 = register.tadd(t, m2);
		if (e.opt.equals("++")) {
			println("move " + r2 + " " + r1);
			println("addi " + r1 + " " + r1 + " 1");
			if (!(e.expr instanceof VariableExpr)) {
				println("sw " + r1 + " " + register.getAddr(r1));
			}
			return r2;
		}
		else if (e.opt.equals("--")) {
			println("move " + r2 + " " + r1);
			println("addi " + r1 + " " + r1 + " -1");
			if (!(e.expr instanceof VariableExpr)) {
				println("sw " + r1 + " " + register.getAddr(r1));
			}
			return r2;
		}
		else {
			System.err.println("Unknown operation!");
			return "err";
		}
	}
	private String visit(ClassExpr e) {
		if (e.right instanceof VariableExpr) {
			String r1 = visit(e.left);
			Type t1 = register.getType(r1);
			Type t = classTable.getType(t1.name, ((VariableExpr)e.right).name);
			int offset = classTable.offset(t1.name, ((VariableExpr)e.right).name);
			String m2 = offset + "(" + r1 + ")";
			String r2 = register.tadd(t, m2);
			println("lw " + r2 + " " + m2);
			return r2;
		}
		else {
			FuncExpr e0 = (FuncExpr)e.right;
			if (e0.name.equals("size")) {
				String r1 = visit(e.left);
				String m2 = 4 * (nWord--) + "($sp)";
				String r2 = register.tadd(register.getType(r1), m2);
				println("addi " + r2 + " " + r1 + " -4");
				String m = 4 * (nWord--) + "($sp)";
				String r = register.tadd("int", m);
				println("lw " + r + " 0(" + r2 + ")");
				return r;
			}
			else if (e0.name.equals("length")) {
				String r1 = visit(e.left);
				String m = 4 * (nWord--) + "($sp)";
				String r = register.tadd("int", m);
				String m2 = 4 * (nWord--) + "($sp)";
				String r2 = register.tadd("string", m2);
				String m3 = 4 * (nWord--) + "($sp)";
				String r3 = register.tadd("char", m3);
				println("li " + r + " 0");
				println("move " + r2 + " " + r1);
				int tLoop = nBlock++;
				int tDone = nBlock++;
				println("L" + tLoop + ":");
				println("lb " + r3 + " 0(" + r2 + ")");
				println("beqz " + r3 + " L" + tDone);
				println("addi " + r + " " + r + " 1");
				println("addi " + r2 + " " + r2 + " 1");
				println("j L" + tLoop);
				println("L" + tDone + ":");
				return r;
			}
			else if (e0.name.equals("parseInt")) {
				String r1 = visit(e.left);
				String mte = 4 * (nWord--) + "($sp)";
				String te = register.tadd("string", mte);
				String mr = 4 * (nWord--) + "($sp)";
				String r = register.tadd("string", mr);
				String mc = 4 * (nWord--) + "($sp)";
				String c = register.tadd("char", mc);
				int t1 = nBlock++;
				int t2 = nBlock++;
				println("move " + te + " " + r1);
				println("li " + r + " 0");
				println("L" + t1 + ":");
				println("lb " + c + " 0(" + te + ")");
				println("addi " + c + " " + c + " -47");
				println("blez " + c + " L" + t2);
				println("addi " + c + " " + c + " -10");
				println("bgtz " + c + " L" + t2);
				println("addi " + c + " " + c + " 9");
				println("mul " + r + " " + r + " 10");
				println("add " + r + " " + r + " " + c);
				println("addi " + te + " " + te + " 1");
				println("j L" + t1);
				println("L" + t2 + ":");
				return r;
			}
			else if (e0.name.equals("substring")) {
				String r1 = visit(e.left);
				String rp1 = visit(e0.param.get(0));
				String rp2 = visit(e0.param.get(1));
				String mte = 4 * (nWord--) + "($sp)";
				String te = register.tadd("string", mte);
				String mr = 4 * (nWord--) + "($sp)";
				String r = register.tadd("string", mr);
				String mc = 4 * (nWord--) + "($sp)";
				String c = register.tadd("string", mc);
				String mtrp1 = 4 * (nWord--) + "($sp)";
				String trp1 = register.tadd("int", mtrp1);
				String mtrp2 = 4 * (nWord--) + "($sp)";
				String trp2 = register.tadd("int", mtrp2);
				println("move " + trp1 + " " + rp1);
				println("move " + trp2 + " " + rp2);
				println("sub $a0 " + rp2 + " " + rp1);
				println("addi $a0 $a0" + " 6");
				println("li $v0 9");
				println("syscall");
				println("move " + r + " $v0");
				println("add $v0 $v0 $a0");
				println("sb $zero -1($v0)");
				println("move $v0 " + r);
				int t1 = nBlock++;
				int t2 = nBlock++;
				int t3 = nBlock++;
				println("move " + te + " " + r1);
				println("L" + t1 + ":");
				println("bgtz " + trp1 + " L" + t2);
				println("blez " + trp2 + " L" + t3);
				println("lb " + c + " 0(" + te + ")");
				println("sb " + c + " 0($v0)");
				println("addi $v0 $v0 1");
				println("L" + t2 + ":");
				println("addi " + te + " " + te + " 1");
				println("addi " + trp1 + " " + trp1 + " -1");
				println("addi " + trp2 + " " + trp2 + " -1");
				println("j L" + t1);
				println("L" + t3 + ":");
				println("lb " + c + " 0(" + te + ")");
				println("sb " + c + " 0($v0)");
				return r;
			}
			else if (e0.name.equals("ord")) {
				String r1 = visit(e.left);
				String rp = visit(e0.param.get(0));
				String mr = 4 * (nWord--) + "($sp)";
				String r = register.tadd("int", mr);
				String mrt = 4 * (nWord--) + "($sp)";
				String rt = register.tadd("int", mrt);
				println("add " + rt + " " + rp + " " + r1);
				println("lb " + r + " 0(" + rt + ")");
				return r;
			}
			return "err";
		}
	}
	private String visit(ArrayExpr e) {
		String r2 = visit(e.left);
		String r3 = visit(e.right);
		Type t = register.getType(r2);
		--t.dms;
		String m1 = 4 * (nWord--) + "($sp)";
		String r1 = register.tadd(t, m1);
		println("mul " + r1 + " " + r3 + " 4");
		println("add " + r1 + " " + r2 + " " + r1);
		String m4 = "0(" + r1 + ")";
		String r4 = register.tadd(t, m4);
		println("lw " + r4 + " " + m4);
		return r4;
	}
	private String visit(CreationExpr e) {
		if (e.expr.size() == 0) {
			Symbol type = e.type.name;
			String m = 4 * (nWord--) + "($sp)";
			String r = register.tadd(type.toString(), m);
			List<ClassDec.Var> list = classTable.getList(type);
			println("li $a0 " + 4 * list.size());
			println("li $v0 9");
			println("syscall");
			println("move " + r + " $v0");
			return r;
		}
		else {
			String m1 = 4 * (nWord--) + "($sp)";
			String r1 = register.tadd(e.type, m1);
			String r2;
			if (e.expr.get(0).constant != null) {
				String m2 = 4 * (nWord--) + "($sp)";
				r2 = register.tadd(e.type, m2);
				println("li " + r2 + " " + e.expr.get(0).constant);
			}
			else {
				r2 = visit(e.expr.get(0));
			}
			println("move $a0 " + r2);
			println("addi $a0 $a0 1");
			println("mul $a0 $a0 4");
			println("li $v0 9");
			println("syscall");
			println("move " + r1 + " $v0");
			println("sw " + r2 + " 0(" + r1 + ")");
			println("addi " + r1 + " " + r1 + " 4");
			return r1;
		}
	}
	private String visit(ParenExpr e) {
		return visit(e.expr);
	}
	private String  visit(NullExpr e) {
		String m = 4 * (nWord--) + "($sp)";
		return register.tadd("null", m);
	}
	private String visit(StringExpr e) {
		return string.get(e.value);
	}
	private String visit(IntExpr e) {
		String m = 4 * (nWord--) + "($sp)";
		String t = register.tadd("int", m);
		println("li " + t + " " + e.value);
		return t;
	}
	private String visit(BoolExpr e) {
		String m = 4 * (nWord--) + "($sp)";
		String t = register.tadd("bool", m);
		if (e.value) println("li " + t + " 1");
		else println("li " + t + " 0");
		return t;
	}
	private String visit(FuncExpr e) {
		if (e.name.equals("getString")) {
			println("li $a0 20");
			println("li $v0 9");
			println("syscall");
			String m = 4 * nWord-- + "($sp)";
			String r = register.tadd("string", m);
			println("move $a0 $v0");
			println("li $a1 20");
			println("li $v0 8");
			println("syscall");
			println("move " + r + " $a0");
			return r;
		}
		else if (e.name.equals("getInt")) {
			String m = 4 * nWord-- + "($sp)";
			String r = register.tadd("int", m);
			println("li $v0 5");
			println("syscall");
			println("move " + r + " $v0");
			return r;
		}
		else if (e.name.equals("print") || e.name.equals("println")) {
			List<Expr> exprs = new ArrayList<>();
			Expr root = e.param.get(0);
			Stack<Expr> stk1 = new Stack<>();
			Stack<Integer> stk2 = new Stack<>();
			stk1.push(root);
			stk2.push(0);
			while (!stk1.empty()) {
				Expr now = stk1.pop();
				int stt = stk2.pop();
				if (now instanceof BinaryExpr) {
					if (stt == 0) {
						stk1.push(now);
						stk2.push(1);
						stk1.push(((BinaryExpr)now).left);
						stk2.push(0);
					}
					else {
						stk1.push(((BinaryExpr)now).right);
						stk2.push(0);
					}
				}
				else {
					exprs.add(now);
				}
			}
			for (int i = 0; i < exprs.size(); ++i) {
				Expr expr = exprs.get(i);
				boolean flag = false;
				String r = null;
				if (expr instanceof FuncExpr) {
					if (((FuncExpr)expr).name.equals("toString")) {
						r = visit(((FuncExpr)expr).param.get(0));
						flag = true;
					}
				}
				if (flag) {
					println("move $a0 " + r);
					println("li $v0 1");
					println("syscall");
				}
				else {
					r = visit(expr);
					println("move $a0 " + r);
					println("li $v0 4");
					println("syscall");
				}
			}
			if (e.name.equals("println")) {
				println("li $a0 4");
				println("li $v0 9");
				println("syscall");
				println("li $v1 10");
				println("sb $v1 0($v0)");
				println("move $a0 $v0");
				println("li $v0 4");
				println("syscall");
			}
			return null;
		}
		else if (e.name.equals("toString")) {
			for (int i = 0; i < 5; ++i) {
				if (i >= register.temp.size()) break;
				println("lw $t" + i + " " + register.getAddr("$t" + i));
			}
			String mr = 4 * (nWord--) + "($sp)";
			String r = register.tadd("string", mr);
			println("move " + r + " $v0");
			return r;
		}
		else {
			String[] r = new String[e.param.size()];
			for (int i = 0; i < e.param.size(); ++i) {
				r[i] = visit(e.param.get(i));
			}
			for (int i = sPos; i < register.save.size(); ++i) {
				String name = "$s" + i;
				println("sw " + name + " " + register.getAddr(name));
			}
			for (int i = 0; i < e.param.size(); ++i) {
				println("sw " + r[i] + " " + (-4) * (i + 2 + nOver) +"($fp)");
			}
			println("jal _" + e.name);
			for (int i = sPos; i < register.save.size(); ++i) {
				String name = "$s" + i;
				println("lw " + name + " " + register.getAddr(name));
			}
			if (!(funcTable.get(e.name)).equals(new Type("void"))) {
				String mret = 4 * (nWord--) + "($sp)";
				String ret = register.tadd((funcTable.get(e.name)), mret);
				println("move " + ret + " $v0");
				return ret;
			}
			else return "err";
		}
	}
	private String visit(VariableExpr e) {
		return symbolTable.get(e.name);
	}
}
