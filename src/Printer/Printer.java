package Printer;

import AST.*;
import lib.*;

/**
 * Created by Administrator on 2016/4/2.
 */
public class Printer {
	final int INDENT = 4;
	void print(int d) {
		for (int i = 0; i < d * INDENT; ++i) {
			System.out.print(' ');
		}
	}
	public void print(Prog e) {
		System.out.println("Prog");
		for (int i = 0; i < e.decList.size(); ++i) {
			visit(e.decList.get(i), 1);
		}
	}
	void visit(Dec e, int d) {
		if (e instanceof FuncDec) visit((FuncDec) e, d);
		else if (e instanceof ClassDec) visit((ClassDec) e, d);
		else if (e instanceof VarDec1) visit((VarDec1) e, d);
	}
	void visit(FuncDec e, int d) {
		print(d);
		System.out.print(e.type.name.toString());
		for (int i = 0; i < e.type.dms; ++i) {
			System.out.print("[]");
		}
		System.out.print(" " + e.name.toString() + "(");
		if (e.param.size() != 0) {
			System.out.print(e.param.get(0).type.name.toString());
			for (int i = 0; i < e.param.get(0).type.dms; ++i) {
				System.out.print("[]");
			}
			System.out.print(" " + e.param.get(0).name.toString());
			for (int i = 1; i < e.param.size(); ++i) {
				System.out.print(", " + e.param.get(i).type.name.toString());
				for (int j = 0; j < e.param.get(i).type.dms; ++j) {
					System.out.print("[]");
				}
				System.out.print(" " + e.param.get(i).name.toString());
			}
		}
		System.out.println(")");
		visit(e.stmt, d + 1);
	}
	void visit(ClassDec e, int d) {
		print(d);
		Symbol x = e.name;
		System.out.print("class " + e.name.toString() + "(");
		System.out.print(e.list.get(0).type.name.toString());
		for (int i = 0; i < e.list.get(0).type.dms; ++i) {
			System.out.print("[]");
		}
		System.out.print(" " + e.list.get(0).name.toString());
		for (int i = 1; i < e.list.size(); ++i) {
			System.out.print(", " + e.list.get(i).type.name.toString());
			for (int j = 0; j < e.list.get(i).type.dms; ++j) {
				System.out.print("[]");
			}
			System.out.print(" " + e.list.get(i).name.toString());
		}
		System.out.println(")");
	}
	void visit(VarDec1 e, int d) {
		print(d);
		System.out.print(e.type.name.toString());
		for (int i = 0; i < e.type.dms; ++i) {
			System.out.print("[]");
		}
		System.out.print(' ' + e.name.toString());
		if (e.expr != null) {
			System.out.println(" =");
			visit(e.expr, d + 1);
		}
		else {
			System.out.println("");
		}
	}
	void visit(Expr e, int d) {
		if (e instanceof BinaryExpr) visit((BinaryExpr) e, d);
		else if (e instanceof LeftExpr) visit((LeftExpr) e, d);
		else if (e instanceof RightExpr) visit((RightExpr) e, d);
		else if (e instanceof ClassExpr) visit((ClassExpr) e, d);
		else if (e instanceof ArrayExpr) visit((ArrayExpr) e, d);
		else if (e instanceof CreationExpr) visit((CreationExpr) e, d);
		else if (e instanceof ParenExpr) visit((ParenExpr) e, d);
		else if (e instanceof NullExpr) visit((NullExpr) e, d);
		else if (e instanceof StringExpr) visit((StringExpr) e, d);
		else if (e instanceof IntExpr) visit((IntExpr) e, d);
		else if (e instanceof BoolExpr) visit((BoolExpr) e, d);
		else if (e instanceof FuncExpr) visit((FuncExpr) e, d);
		else if (e instanceof VariableExpr) visit((VariableExpr) e, d);
	}
	void visit(BinaryExpr e, int d) {
		print(d);
		System.out.println("BinaryExpr: " + e.opt.toString());
		visit(e.left, d + 1);
		visit(e.right, d + 1);
	}
	void visit(LeftExpr e, int d) {
		print(d);
		System.out.println("LeftExpr: " + e.opt.toString());
		visit(e.expr, d + 1);
	}
	void  visit(RightExpr e, int d) {
		print(d);
		System.out.println("RightExpr: " + e.opt.toString());
		visit(e.expr, d + 1);
	}
	void visit(ClassExpr e, int d) {
		print(d);
		System.out.println("ClassExpr:");
		visit(e.left, d + 1);
		visit(e.right, d + 1);
	}
	void visit(ArrayExpr e, int d) {
		print(d);
		System.out.println("ArrayExpr:");
		visit(e.left, d + 1);
		visit(e.right, d + 1);
	}
	void visit(CreationExpr e, int d) {
		print(d);
		System.out.println("CreationExpr:");
		for (int i = 0; i < e.expr.size(); ++i) {
			visit(e.expr.get(i), d + 1);
		}
		for (int i = 0; i < e.expr.size(); ++i) {
			visit(new NullExpr(), d + 1);
		}
	}
	void visit(ParenExpr e, int d) {
		print(d);
		System.out.println("ParenExpr:");
		visit(e.expr, d + 1);
	}
	void visit(NullExpr e, int d) {
		print(d);
		System.out.println("NullExpr");
	}
	void visit(StringExpr e, int d) {
		print(d);
		System.out.println("StringExpr: " + e.value.toString());
	}
	void visit(IntExpr e, int d) {
		print(d);
		System.out.print("IntExpr: ");
		System.out.println(e.value);
	}
	void visit(BoolExpr e, int d) {
		print(d);
		System.out.print("BoolExpr: ");
		System.out.println(e.value);
	}
	void visit(FuncExpr e, int d) {
		print(d);
		System.out.println("FuncExpr: " + e.name.toString());
		for (int i = 0; i < e.param.size(); ++i) {
			visit(e.param.get(i), d + 1);
		}
	}
	void visit(VariableExpr e, int d) {
		print(d);
		System.out.println("VariableExpr: " + e.name.toString());
	}
	void visit(Stmt e, int d) {
		if (e instanceof Expr) visit((Expr) e, d);
		else if (e instanceof ReturnStmt) visit((ReturnStmt) e, d);
		else if (e instanceof ContinueStmt) visit((ContinueStmt) e, d);
		else if (e instanceof BreakStmt) visit((BreakStmt) e, d);
		else if (e instanceof IfStmt) visit((IfStmt) e, d);
		else if (e instanceof WhileStmt) visit((WhileStmt) e, d);
		else if (e instanceof ForStmt) visit((ForStmt) e, d);
		else if (e instanceof CompoundStmt) visit((CompoundStmt) e, d);
		else if (e instanceof VarDec2) visit((VarDec2) e, d);
	}
	void visit(ReturnStmt e, int d) {
		print(d);
		System.out.println("Return");
		visit(e.expr, d + 1);
	}
	void visit(ContinueStmt e,int d) {
		print(d);
		System.out.println("Comtinue");
	}
	void visit(BreakStmt e, int d) {
		print(d);
		System.out.println("Break");
	}
	void visit(IfStmt e, int d) {
		print(d);
		System.out.println("If");
		visit(e.cond, d + 1);
		visit(e.stmt1, d + 1);
		if (e.stmt2 != null) {
			visit(e.stmt2, d + 1);
		}
		else {
			visit(new NullExpr(), d + 1);
		}
	}
	void visit(WhileStmt e,int d) {
		print(d);
		System.out.println("While");
		visit(e.cond, d + 1);
		visit(e.stmt, d + 1);
	}
	void visit(ForStmt e,int d) {
		print(d);
		System.out.println("For");
		if (e.exp1 != null) {
			visit(e.exp1, d + 1);
		}
		else {
			visit(new NullExpr(), d + 1);
		}
		if (e.exp2 != null) {
			visit(e.exp2, d + 1);
		}
		else {
			visit(new NullExpr(), d + 1);
		}
		if (e.exp3 != null) {
			visit(e.exp3, d + 1);
		}
		else {
			visit(new NullExpr(), d + 1);
		}
		visit(e.stmt, d + 1);
	}
	void visit(CompoundStmt e,int d) {
		print(d);
		System.out.println("Compound");
		for (int i = 0; i < e.stmt.size(); ++i) {
			visit(e.stmt.get(i), d + 1);
		}
	}
	void visit(VarDec2 e, int d) {
		print(d);
		System.out.print(e.type.name.toString());
		for (int i = 0; i < e.type.dms; ++i) {
			System.out.print("[]");
		}
		System.out.print(' ' + e.name.toString());
		if (e.expr != null) {
			System.out.println(" =");
			visit(e.expr, d + 1);
		}
		else {
			System.out.println("");
		}
	}
}
