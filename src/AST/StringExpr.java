package AST;

import lib.*;

/**
 * Created by Administrator on 2016/4/2.
 */
public class StringExpr extends SingleExpr {
	public Symbol value;
	public StringExpr() {}
	public StringExpr(String v) {
		value = Symbol.symbol(v);
	}
	@Override
	public boolean isLvalue() {
		return false;
	}
}
