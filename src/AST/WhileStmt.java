package AST;

/**
 * Created by Administrator on 2016/4/1.
 */
public class WhileStmt extends Stmt {
	public Expr cond;
	public Stmt stmt;
	public WhileStmt() {
		cond = null;
		stmt = null;
	}
	public WhileStmt(Expr e, Stmt s) {
		cond = e;
		stmt = s;
	}
}
