package AST;

/**
 * Created by Administrator on 2016/4/2.
 */
public class IntExpr extends SingleExpr {
	public int value;
	public IntExpr() {}
	public IntExpr(int v) {
		value = v;
	}
	@Override
	public boolean isLvalue() {
		return false;
	}
}
