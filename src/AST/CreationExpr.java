package AST;

import lib.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2016/4/2.
 */
public class CreationExpr extends SingleExpr {
	public List<Expr> expr;
	public CreationExpr() {
		expr = new ArrayList<Expr>();
	}
	public CreationExpr(List<Expr> e) {
		expr = e;
	}
	@Override
	public boolean isLvalue() {
		return false;
	}
}
