package AST;

import lib.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2016/4/1.
 */
public class ClassDec extends Dec {
	public class Var {
		public Type type;
		public Symbol name;
		Var() {}
		Var(Type t, String n) {
			type = t;
			name = Symbol.symbol(n);
		}
	}
	public Symbol name;
	public List<Var> list;
	public ClassDec() {
		name = null;
		list = new ArrayList<Var>();
	}
	public void add(Type t, String n) {
		list.add(new Var(t, n));
	}
}
