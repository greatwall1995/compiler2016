package AST;

import lib.*;
import java.util.*;

/**
 * Created by Administrator on 2016/4/1.
 */
public class FuncDec extends Dec {
	public class Var {
		public Type type;
		public Symbol name;
		Var() {}
		Var(Type t, String n) {
			type = t;
			name = Symbol.symbol(n);
		}
	}
	public Type type;
	public Symbol name;
	public List<Var> param;
	public Stmt stmt;
	public void addParam(Type t, String n) {
		param.add(new Var(t, n));
	}
	FuncDec() {
		param = new ArrayList<Var>();
		type = null;
		name = null;
	}
}
