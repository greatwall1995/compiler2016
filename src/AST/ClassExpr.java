package AST;

/**
 * Created by Administrator on 2016/4/2.
 */
public class ClassExpr extends UnaryExpr {
	public UnaryExpr left;
	public SingleExpr right;
	public ClassExpr() {}
	@Override
	public boolean isLvalue() {
		return true;
	}
}
