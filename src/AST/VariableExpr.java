package AST;

import lib.*;

/**
 * Created by Administrator on 2016/4/2.
 */
public class VariableExpr extends SingleExpr {
	public Symbol name;
	public VariableExpr() {}
	public VariableExpr(String n) {
		name = Symbol.symbol(n);
	}
	@Override
	public boolean isLvalue() {
		return true;
	}
}
