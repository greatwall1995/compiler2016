package AST;

import lib.*;

/**
 * Created by Administrator on 2016/4/2.
 */
public class RightExpr extends UnaryExpr{
	public Symbol opt;
	public UnaryExpr expr;
	public RightExpr() {}
	@Override
	public boolean isLvalue() {
		return false;
	}
}
