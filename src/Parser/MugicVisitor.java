// Generated from Mugic.g4 by ANTLR 4.5.1

package Parser;

import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link MugicParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface MugicVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by the {@code compound}
	 * labeled alternative in {@link MugicParser#stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCompound(MugicParser.CompoundContext ctx);
	/**
	 * Visit a parse tree produced by the {@code for}
	 * labeled alternative in {@link MugicParser#stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFor(MugicParser.ForContext ctx);
	/**
	 * Visit a parse tree produced by the {@code while}
	 * labeled alternative in {@link MugicParser#stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWhile(MugicParser.WhileContext ctx);
	/**
	 * Visit a parse tree produced by the {@code if}
	 * labeled alternative in {@link MugicParser#stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIf(MugicParser.IfContext ctx);
	/**
	 * Visit a parse tree produced by the {@code jump}
	 * labeled alternative in {@link MugicParser#stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitJump(MugicParser.JumpContext ctx);
	/**
	 * Visit a parse tree produced by the {@code varDef}
	 * labeled alternative in {@link MugicParser#stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVarDef(MugicParser.VarDefContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ep}
	 * labeled alternative in {@link MugicParser#stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEp(MugicParser.EpContext ctx);
	/**
	 * Visit a parse tree produced by the {@code nullExp}
	 * labeled alternative in {@link MugicParser#stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNullExp(MugicParser.NullExpContext ctx);
	/**
	 * Visit a parse tree produced by {@link MugicParser#compoundStmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCompoundStmt(MugicParser.CompoundStmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link MugicParser#forStmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitForStmt(MugicParser.ForStmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link MugicParser#whileStmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWhileStmt(MugicParser.WhileStmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link MugicParser#ifStmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIfStmt(MugicParser.IfStmtContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Break}
	 * labeled alternative in {@link MugicParser#jumpStmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBreak(MugicParser.BreakContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Continue}
	 * labeled alternative in {@link MugicParser#jumpStmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitContinue(MugicParser.ContinueContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Return}
	 * labeled alternative in {@link MugicParser#jumpStmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitReturn(MugicParser.ReturnContext ctx);
	/**
	 * Visit a parse tree produced by {@link MugicParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpr(MugicParser.ExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code VariableExpr}
	 * labeled alternative in {@link MugicParser#primaryExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVariableExpr(MugicParser.VariableExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code FuncExpr1}
	 * labeled alternative in {@link MugicParser#primaryExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFuncExpr1(MugicParser.FuncExpr1Context ctx);
	/**
	 * Visit a parse tree produced by the {@code FuncExpr2}
	 * labeled alternative in {@link MugicParser#primaryExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFuncExpr2(MugicParser.FuncExpr2Context ctx);
	/**
	 * Visit a parse tree produced by the {@code ConstantExpr}
	 * labeled alternative in {@link MugicParser#primaryExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConstantExpr(MugicParser.ConstantExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ParenExpr}
	 * labeled alternative in {@link MugicParser#primaryExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParenExpr(MugicParser.ParenExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code CreationExpr}
	 * labeled alternative in {@link MugicParser#primaryExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCreationExpr(MugicParser.CreationExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code prExpr}
	 * labeled alternative in {@link MugicParser#postfixExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrExpr(MugicParser.PrExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code rightExpr}
	 * labeled alternative in {@link MugicParser#postfixExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRightExpr(MugicParser.RightExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code arrayExpr}
	 * labeled alternative in {@link MugicParser#postfixExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArrayExpr(MugicParser.ArrayExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code classExpr}
	 * labeled alternative in {@link MugicParser#postfixExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitClassExpr(MugicParser.ClassExprContext ctx);
	/**
	 * Visit a parse tree produced by {@link MugicParser#unaryExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnaryExpr(MugicParser.UnaryExprContext ctx);
	/**
	 * Visit a parse tree produced by {@link MugicParser#multiplicativeExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMultiplicativeExpr(MugicParser.MultiplicativeExprContext ctx);
	/**
	 * Visit a parse tree produced by {@link MugicParser#additiveExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAdditiveExpr(MugicParser.AdditiveExprContext ctx);
	/**
	 * Visit a parse tree produced by {@link MugicParser#shiftExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitShiftExpr(MugicParser.ShiftExprContext ctx);
	/**
	 * Visit a parse tree produced by {@link MugicParser#relationalExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRelationalExpr(MugicParser.RelationalExprContext ctx);
	/**
	 * Visit a parse tree produced by {@link MugicParser#equalityExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEqualityExpr(MugicParser.EqualityExprContext ctx);
	/**
	 * Visit a parse tree produced by {@link MugicParser#andExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAndExpr(MugicParser.AndExprContext ctx);
	/**
	 * Visit a parse tree produced by {@link MugicParser#exclusiveOrExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExclusiveOrExpr(MugicParser.ExclusiveOrExprContext ctx);
	/**
	 * Visit a parse tree produced by {@link MugicParser#inclusiveOrExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInclusiveOrExpr(MugicParser.InclusiveOrExprContext ctx);
	/**
	 * Visit a parse tree produced by {@link MugicParser#logicalAndExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLogicalAndExpr(MugicParser.LogicalAndExprContext ctx);
	/**
	 * Visit a parse tree produced by {@link MugicParser#logicalOrExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLogicalOrExpr(MugicParser.LogicalOrExprContext ctx);
	/**
	 * Visit a parse tree produced by {@link MugicParser#assignmentExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAssignmentExpr(MugicParser.AssignmentExprContext ctx);
	/**
	 * Visit a parse tree produced by {@link MugicParser#main}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMain(MugicParser.MainContext ctx);
	/**
	 * Visit a parse tree produced by {@link MugicParser#classDefinition}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitClassDefinition(MugicParser.ClassDefinitionContext ctx);
	/**
	 * Visit a parse tree produced by {@link MugicParser#classList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitClassList(MugicParser.ClassListContext ctx);
	/**
	 * Visit a parse tree produced by the {@code funcDec}
	 * labeled alternative in {@link MugicParser#translationUnit}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFuncDec(MugicParser.FuncDecContext ctx);
	/**
	 * Visit a parse tree produced by the {@code varDec}
	 * labeled alternative in {@link MugicParser#translationUnit}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVarDec(MugicParser.VarDecContext ctx);
	/**
	 * Visit a parse tree produced by the {@code classDec}
	 * labeled alternative in {@link MugicParser#translationUnit}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitClassDec(MugicParser.ClassDecContext ctx);
	/**
	 * Visit a parse tree produced by the {@code funcDec_}
	 * labeled alternative in {@link MugicParser#translationUnit}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFuncDec_(MugicParser.FuncDec_Context ctx);
	/**
	 * Visit a parse tree produced by the {@code varDec_}
	 * labeled alternative in {@link MugicParser#translationUnit}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVarDec_(MugicParser.VarDec_Context ctx);
	/**
	 * Visit a parse tree produced by the {@code classDec_}
	 * labeled alternative in {@link MugicParser#translationUnit}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitClassDec_(MugicParser.ClassDec_Context ctx);
	/**
	 * Visit a parse tree produced by {@link MugicParser#functionDefinition}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunctionDefinition(MugicParser.FunctionDefinitionContext ctx);
	/**
	 * Visit a parse tree produced by {@link MugicParser#functionDefinition1}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunctionDefinition1(MugicParser.FunctionDefinition1Context ctx);
	/**
	 * Visit a parse tree produced by {@link MugicParser#functionDefinition2}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunctionDefinition2(MugicParser.FunctionDefinition2Context ctx);
	/**
	 * Visit a parse tree produced by {@link MugicParser#paramList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParamList(MugicParser.ParamListContext ctx);
	/**
	 * Visit a parse tree produced by {@link MugicParser#variableDefinition1}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVariableDefinition1(MugicParser.VariableDefinition1Context ctx);
	/**
	 * Visit a parse tree produced by {@link MugicParser#variableDefinition2}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVariableDefinition2(MugicParser.VariableDefinition2Context ctx);
	/**
	 * Visit a parse tree produced by {@link MugicParser#constant}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConstant(MugicParser.ConstantContext ctx);
}