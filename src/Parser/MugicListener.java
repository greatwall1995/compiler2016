// Generated from Mugic.g4 by ANTLR 4.5.1

package Parser;

import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link MugicParser}.
 */
public interface MugicListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by the {@code compound}
	 * labeled alternative in {@link MugicParser#stmt}.
	 * @param ctx the parse tree
	 */
	void enterCompound(MugicParser.CompoundContext ctx);
	/**
	 * Exit a parse tree produced by the {@code compound}
	 * labeled alternative in {@link MugicParser#stmt}.
	 * @param ctx the parse tree
	 */
	void exitCompound(MugicParser.CompoundContext ctx);
	/**
	 * Enter a parse tree produced by the {@code for}
	 * labeled alternative in {@link MugicParser#stmt}.
	 * @param ctx the parse tree
	 */
	void enterFor(MugicParser.ForContext ctx);
	/**
	 * Exit a parse tree produced by the {@code for}
	 * labeled alternative in {@link MugicParser#stmt}.
	 * @param ctx the parse tree
	 */
	void exitFor(MugicParser.ForContext ctx);
	/**
	 * Enter a parse tree produced by the {@code while}
	 * labeled alternative in {@link MugicParser#stmt}.
	 * @param ctx the parse tree
	 */
	void enterWhile(MugicParser.WhileContext ctx);
	/**
	 * Exit a parse tree produced by the {@code while}
	 * labeled alternative in {@link MugicParser#stmt}.
	 * @param ctx the parse tree
	 */
	void exitWhile(MugicParser.WhileContext ctx);
	/**
	 * Enter a parse tree produced by the {@code if}
	 * labeled alternative in {@link MugicParser#stmt}.
	 * @param ctx the parse tree
	 */
	void enterIf(MugicParser.IfContext ctx);
	/**
	 * Exit a parse tree produced by the {@code if}
	 * labeled alternative in {@link MugicParser#stmt}.
	 * @param ctx the parse tree
	 */
	void exitIf(MugicParser.IfContext ctx);
	/**
	 * Enter a parse tree produced by the {@code jump}
	 * labeled alternative in {@link MugicParser#stmt}.
	 * @param ctx the parse tree
	 */
	void enterJump(MugicParser.JumpContext ctx);
	/**
	 * Exit a parse tree produced by the {@code jump}
	 * labeled alternative in {@link MugicParser#stmt}.
	 * @param ctx the parse tree
	 */
	void exitJump(MugicParser.JumpContext ctx);
	/**
	 * Enter a parse tree produced by the {@code varDef}
	 * labeled alternative in {@link MugicParser#stmt}.
	 * @param ctx the parse tree
	 */
	void enterVarDef(MugicParser.VarDefContext ctx);
	/**
	 * Exit a parse tree produced by the {@code varDef}
	 * labeled alternative in {@link MugicParser#stmt}.
	 * @param ctx the parse tree
	 */
	void exitVarDef(MugicParser.VarDefContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ep}
	 * labeled alternative in {@link MugicParser#stmt}.
	 * @param ctx the parse tree
	 */
	void enterEp(MugicParser.EpContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ep}
	 * labeled alternative in {@link MugicParser#stmt}.
	 * @param ctx the parse tree
	 */
	void exitEp(MugicParser.EpContext ctx);
	/**
	 * Enter a parse tree produced by the {@code nullExp}
	 * labeled alternative in {@link MugicParser#stmt}.
	 * @param ctx the parse tree
	 */
	void enterNullExp(MugicParser.NullExpContext ctx);
	/**
	 * Exit a parse tree produced by the {@code nullExp}
	 * labeled alternative in {@link MugicParser#stmt}.
	 * @param ctx the parse tree
	 */
	void exitNullExp(MugicParser.NullExpContext ctx);
	/**
	 * Enter a parse tree produced by {@link MugicParser#compoundStmt}.
	 * @param ctx the parse tree
	 */
	void enterCompoundStmt(MugicParser.CompoundStmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link MugicParser#compoundStmt}.
	 * @param ctx the parse tree
	 */
	void exitCompoundStmt(MugicParser.CompoundStmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link MugicParser#forStmt}.
	 * @param ctx the parse tree
	 */
	void enterForStmt(MugicParser.ForStmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link MugicParser#forStmt}.
	 * @param ctx the parse tree
	 */
	void exitForStmt(MugicParser.ForStmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link MugicParser#whileStmt}.
	 * @param ctx the parse tree
	 */
	void enterWhileStmt(MugicParser.WhileStmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link MugicParser#whileStmt}.
	 * @param ctx the parse tree
	 */
	void exitWhileStmt(MugicParser.WhileStmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link MugicParser#ifStmt}.
	 * @param ctx the parse tree
	 */
	void enterIfStmt(MugicParser.IfStmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link MugicParser#ifStmt}.
	 * @param ctx the parse tree
	 */
	void exitIfStmt(MugicParser.IfStmtContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Break}
	 * labeled alternative in {@link MugicParser#jumpStmt}.
	 * @param ctx the parse tree
	 */
	void enterBreak(MugicParser.BreakContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Break}
	 * labeled alternative in {@link MugicParser#jumpStmt}.
	 * @param ctx the parse tree
	 */
	void exitBreak(MugicParser.BreakContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Continue}
	 * labeled alternative in {@link MugicParser#jumpStmt}.
	 * @param ctx the parse tree
	 */
	void enterContinue(MugicParser.ContinueContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Continue}
	 * labeled alternative in {@link MugicParser#jumpStmt}.
	 * @param ctx the parse tree
	 */
	void exitContinue(MugicParser.ContinueContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Return}
	 * labeled alternative in {@link MugicParser#jumpStmt}.
	 * @param ctx the parse tree
	 */
	void enterReturn(MugicParser.ReturnContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Return}
	 * labeled alternative in {@link MugicParser#jumpStmt}.
	 * @param ctx the parse tree
	 */
	void exitReturn(MugicParser.ReturnContext ctx);
	/**
	 * Enter a parse tree produced by {@link MugicParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterExpr(MugicParser.ExprContext ctx);
	/**
	 * Exit a parse tree produced by {@link MugicParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitExpr(MugicParser.ExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code VariableExpr}
	 * labeled alternative in {@link MugicParser#primaryExpr}.
	 * @param ctx the parse tree
	 */
	void enterVariableExpr(MugicParser.VariableExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code VariableExpr}
	 * labeled alternative in {@link MugicParser#primaryExpr}.
	 * @param ctx the parse tree
	 */
	void exitVariableExpr(MugicParser.VariableExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code FuncExpr1}
	 * labeled alternative in {@link MugicParser#primaryExpr}.
	 * @param ctx the parse tree
	 */
	void enterFuncExpr1(MugicParser.FuncExpr1Context ctx);
	/**
	 * Exit a parse tree produced by the {@code FuncExpr1}
	 * labeled alternative in {@link MugicParser#primaryExpr}.
	 * @param ctx the parse tree
	 */
	void exitFuncExpr1(MugicParser.FuncExpr1Context ctx);
	/**
	 * Enter a parse tree produced by the {@code FuncExpr2}
	 * labeled alternative in {@link MugicParser#primaryExpr}.
	 * @param ctx the parse tree
	 */
	void enterFuncExpr2(MugicParser.FuncExpr2Context ctx);
	/**
	 * Exit a parse tree produced by the {@code FuncExpr2}
	 * labeled alternative in {@link MugicParser#primaryExpr}.
	 * @param ctx the parse tree
	 */
	void exitFuncExpr2(MugicParser.FuncExpr2Context ctx);
	/**
	 * Enter a parse tree produced by the {@code ConstantExpr}
	 * labeled alternative in {@link MugicParser#primaryExpr}.
	 * @param ctx the parse tree
	 */
	void enterConstantExpr(MugicParser.ConstantExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ConstantExpr}
	 * labeled alternative in {@link MugicParser#primaryExpr}.
	 * @param ctx the parse tree
	 */
	void exitConstantExpr(MugicParser.ConstantExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ParenExpr}
	 * labeled alternative in {@link MugicParser#primaryExpr}.
	 * @param ctx the parse tree
	 */
	void enterParenExpr(MugicParser.ParenExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ParenExpr}
	 * labeled alternative in {@link MugicParser#primaryExpr}.
	 * @param ctx the parse tree
	 */
	void exitParenExpr(MugicParser.ParenExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code CreationExpr}
	 * labeled alternative in {@link MugicParser#primaryExpr}.
	 * @param ctx the parse tree
	 */
	void enterCreationExpr(MugicParser.CreationExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code CreationExpr}
	 * labeled alternative in {@link MugicParser#primaryExpr}.
	 * @param ctx the parse tree
	 */
	void exitCreationExpr(MugicParser.CreationExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code prExpr}
	 * labeled alternative in {@link MugicParser#postfixExpr}.
	 * @param ctx the parse tree
	 */
	void enterPrExpr(MugicParser.PrExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code prExpr}
	 * labeled alternative in {@link MugicParser#postfixExpr}.
	 * @param ctx the parse tree
	 */
	void exitPrExpr(MugicParser.PrExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code rightExpr}
	 * labeled alternative in {@link MugicParser#postfixExpr}.
	 * @param ctx the parse tree
	 */
	void enterRightExpr(MugicParser.RightExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code rightExpr}
	 * labeled alternative in {@link MugicParser#postfixExpr}.
	 * @param ctx the parse tree
	 */
	void exitRightExpr(MugicParser.RightExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code arrayExpr}
	 * labeled alternative in {@link MugicParser#postfixExpr}.
	 * @param ctx the parse tree
	 */
	void enterArrayExpr(MugicParser.ArrayExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code arrayExpr}
	 * labeled alternative in {@link MugicParser#postfixExpr}.
	 * @param ctx the parse tree
	 */
	void exitArrayExpr(MugicParser.ArrayExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code classExpr}
	 * labeled alternative in {@link MugicParser#postfixExpr}.
	 * @param ctx the parse tree
	 */
	void enterClassExpr(MugicParser.ClassExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code classExpr}
	 * labeled alternative in {@link MugicParser#postfixExpr}.
	 * @param ctx the parse tree
	 */
	void exitClassExpr(MugicParser.ClassExprContext ctx);
	/**
	 * Enter a parse tree produced by {@link MugicParser#unaryExpr}.
	 * @param ctx the parse tree
	 */
	void enterUnaryExpr(MugicParser.UnaryExprContext ctx);
	/**
	 * Exit a parse tree produced by {@link MugicParser#unaryExpr}.
	 * @param ctx the parse tree
	 */
	void exitUnaryExpr(MugicParser.UnaryExprContext ctx);
	/**
	 * Enter a parse tree produced by {@link MugicParser#multiplicativeExpr}.
	 * @param ctx the parse tree
	 */
	void enterMultiplicativeExpr(MugicParser.MultiplicativeExprContext ctx);
	/**
	 * Exit a parse tree produced by {@link MugicParser#multiplicativeExpr}.
	 * @param ctx the parse tree
	 */
	void exitMultiplicativeExpr(MugicParser.MultiplicativeExprContext ctx);
	/**
	 * Enter a parse tree produced by {@link MugicParser#additiveExpr}.
	 * @param ctx the parse tree
	 */
	void enterAdditiveExpr(MugicParser.AdditiveExprContext ctx);
	/**
	 * Exit a parse tree produced by {@link MugicParser#additiveExpr}.
	 * @param ctx the parse tree
	 */
	void exitAdditiveExpr(MugicParser.AdditiveExprContext ctx);
	/**
	 * Enter a parse tree produced by {@link MugicParser#shiftExpr}.
	 * @param ctx the parse tree
	 */
	void enterShiftExpr(MugicParser.ShiftExprContext ctx);
	/**
	 * Exit a parse tree produced by {@link MugicParser#shiftExpr}.
	 * @param ctx the parse tree
	 */
	void exitShiftExpr(MugicParser.ShiftExprContext ctx);
	/**
	 * Enter a parse tree produced by {@link MugicParser#relationalExpr}.
	 * @param ctx the parse tree
	 */
	void enterRelationalExpr(MugicParser.RelationalExprContext ctx);
	/**
	 * Exit a parse tree produced by {@link MugicParser#relationalExpr}.
	 * @param ctx the parse tree
	 */
	void exitRelationalExpr(MugicParser.RelationalExprContext ctx);
	/**
	 * Enter a parse tree produced by {@link MugicParser#equalityExpr}.
	 * @param ctx the parse tree
	 */
	void enterEqualityExpr(MugicParser.EqualityExprContext ctx);
	/**
	 * Exit a parse tree produced by {@link MugicParser#equalityExpr}.
	 * @param ctx the parse tree
	 */
	void exitEqualityExpr(MugicParser.EqualityExprContext ctx);
	/**
	 * Enter a parse tree produced by {@link MugicParser#andExpr}.
	 * @param ctx the parse tree
	 */
	void enterAndExpr(MugicParser.AndExprContext ctx);
	/**
	 * Exit a parse tree produced by {@link MugicParser#andExpr}.
	 * @param ctx the parse tree
	 */
	void exitAndExpr(MugicParser.AndExprContext ctx);
	/**
	 * Enter a parse tree produced by {@link MugicParser#exclusiveOrExpr}.
	 * @param ctx the parse tree
	 */
	void enterExclusiveOrExpr(MugicParser.ExclusiveOrExprContext ctx);
	/**
	 * Exit a parse tree produced by {@link MugicParser#exclusiveOrExpr}.
	 * @param ctx the parse tree
	 */
	void exitExclusiveOrExpr(MugicParser.ExclusiveOrExprContext ctx);
	/**
	 * Enter a parse tree produced by {@link MugicParser#inclusiveOrExpr}.
	 * @param ctx the parse tree
	 */
	void enterInclusiveOrExpr(MugicParser.InclusiveOrExprContext ctx);
	/**
	 * Exit a parse tree produced by {@link MugicParser#inclusiveOrExpr}.
	 * @param ctx the parse tree
	 */
	void exitInclusiveOrExpr(MugicParser.InclusiveOrExprContext ctx);
	/**
	 * Enter a parse tree produced by {@link MugicParser#logicalAndExpr}.
	 * @param ctx the parse tree
	 */
	void enterLogicalAndExpr(MugicParser.LogicalAndExprContext ctx);
	/**
	 * Exit a parse tree produced by {@link MugicParser#logicalAndExpr}.
	 * @param ctx the parse tree
	 */
	void exitLogicalAndExpr(MugicParser.LogicalAndExprContext ctx);
	/**
	 * Enter a parse tree produced by {@link MugicParser#logicalOrExpr}.
	 * @param ctx the parse tree
	 */
	void enterLogicalOrExpr(MugicParser.LogicalOrExprContext ctx);
	/**
	 * Exit a parse tree produced by {@link MugicParser#logicalOrExpr}.
	 * @param ctx the parse tree
	 */
	void exitLogicalOrExpr(MugicParser.LogicalOrExprContext ctx);
	/**
	 * Enter a parse tree produced by {@link MugicParser#assignmentExpr}.
	 * @param ctx the parse tree
	 */
	void enterAssignmentExpr(MugicParser.AssignmentExprContext ctx);
	/**
	 * Exit a parse tree produced by {@link MugicParser#assignmentExpr}.
	 * @param ctx the parse tree
	 */
	void exitAssignmentExpr(MugicParser.AssignmentExprContext ctx);
	/**
	 * Enter a parse tree produced by {@link MugicParser#main}.
	 * @param ctx the parse tree
	 */
	void enterMain(MugicParser.MainContext ctx);
	/**
	 * Exit a parse tree produced by {@link MugicParser#main}.
	 * @param ctx the parse tree
	 */
	void exitMain(MugicParser.MainContext ctx);
	/**
	 * Enter a parse tree produced by {@link MugicParser#classDefinition}.
	 * @param ctx the parse tree
	 */
	void enterClassDefinition(MugicParser.ClassDefinitionContext ctx);
	/**
	 * Exit a parse tree produced by {@link MugicParser#classDefinition}.
	 * @param ctx the parse tree
	 */
	void exitClassDefinition(MugicParser.ClassDefinitionContext ctx);
	/**
	 * Enter a parse tree produced by {@link MugicParser#classList}.
	 * @param ctx the parse tree
	 */
	void enterClassList(MugicParser.ClassListContext ctx);
	/**
	 * Exit a parse tree produced by {@link MugicParser#classList}.
	 * @param ctx the parse tree
	 */
	void exitClassList(MugicParser.ClassListContext ctx);
	/**
	 * Enter a parse tree produced by the {@code funcDec}
	 * labeled alternative in {@link MugicParser#translationUnit}.
	 * @param ctx the parse tree
	 */
	void enterFuncDec(MugicParser.FuncDecContext ctx);
	/**
	 * Exit a parse tree produced by the {@code funcDec}
	 * labeled alternative in {@link MugicParser#translationUnit}.
	 * @param ctx the parse tree
	 */
	void exitFuncDec(MugicParser.FuncDecContext ctx);
	/**
	 * Enter a parse tree produced by the {@code varDec}
	 * labeled alternative in {@link MugicParser#translationUnit}.
	 * @param ctx the parse tree
	 */
	void enterVarDec(MugicParser.VarDecContext ctx);
	/**
	 * Exit a parse tree produced by the {@code varDec}
	 * labeled alternative in {@link MugicParser#translationUnit}.
	 * @param ctx the parse tree
	 */
	void exitVarDec(MugicParser.VarDecContext ctx);
	/**
	 * Enter a parse tree produced by the {@code classDec}
	 * labeled alternative in {@link MugicParser#translationUnit}.
	 * @param ctx the parse tree
	 */
	void enterClassDec(MugicParser.ClassDecContext ctx);
	/**
	 * Exit a parse tree produced by the {@code classDec}
	 * labeled alternative in {@link MugicParser#translationUnit}.
	 * @param ctx the parse tree
	 */
	void exitClassDec(MugicParser.ClassDecContext ctx);
	/**
	 * Enter a parse tree produced by the {@code funcDec_}
	 * labeled alternative in {@link MugicParser#translationUnit}.
	 * @param ctx the parse tree
	 */
	void enterFuncDec_(MugicParser.FuncDec_Context ctx);
	/**
	 * Exit a parse tree produced by the {@code funcDec_}
	 * labeled alternative in {@link MugicParser#translationUnit}.
	 * @param ctx the parse tree
	 */
	void exitFuncDec_(MugicParser.FuncDec_Context ctx);
	/**
	 * Enter a parse tree produced by the {@code varDec_}
	 * labeled alternative in {@link MugicParser#translationUnit}.
	 * @param ctx the parse tree
	 */
	void enterVarDec_(MugicParser.VarDec_Context ctx);
	/**
	 * Exit a parse tree produced by the {@code varDec_}
	 * labeled alternative in {@link MugicParser#translationUnit}.
	 * @param ctx the parse tree
	 */
	void exitVarDec_(MugicParser.VarDec_Context ctx);
	/**
	 * Enter a parse tree produced by the {@code classDec_}
	 * labeled alternative in {@link MugicParser#translationUnit}.
	 * @param ctx the parse tree
	 */
	void enterClassDec_(MugicParser.ClassDec_Context ctx);
	/**
	 * Exit a parse tree produced by the {@code classDec_}
	 * labeled alternative in {@link MugicParser#translationUnit}.
	 * @param ctx the parse tree
	 */
	void exitClassDec_(MugicParser.ClassDec_Context ctx);
	/**
	 * Enter a parse tree produced by {@link MugicParser#functionDefinition}.
	 * @param ctx the parse tree
	 */
	void enterFunctionDefinition(MugicParser.FunctionDefinitionContext ctx);
	/**
	 * Exit a parse tree produced by {@link MugicParser#functionDefinition}.
	 * @param ctx the parse tree
	 */
	void exitFunctionDefinition(MugicParser.FunctionDefinitionContext ctx);
	/**
	 * Enter a parse tree produced by {@link MugicParser#functionDefinition1}.
	 * @param ctx the parse tree
	 */
	void enterFunctionDefinition1(MugicParser.FunctionDefinition1Context ctx);
	/**
	 * Exit a parse tree produced by {@link MugicParser#functionDefinition1}.
	 * @param ctx the parse tree
	 */
	void exitFunctionDefinition1(MugicParser.FunctionDefinition1Context ctx);
	/**
	 * Enter a parse tree produced by {@link MugicParser#functionDefinition2}.
	 * @param ctx the parse tree
	 */
	void enterFunctionDefinition2(MugicParser.FunctionDefinition2Context ctx);
	/**
	 * Exit a parse tree produced by {@link MugicParser#functionDefinition2}.
	 * @param ctx the parse tree
	 */
	void exitFunctionDefinition2(MugicParser.FunctionDefinition2Context ctx);
	/**
	 * Enter a parse tree produced by {@link MugicParser#paramList}.
	 * @param ctx the parse tree
	 */
	void enterParamList(MugicParser.ParamListContext ctx);
	/**
	 * Exit a parse tree produced by {@link MugicParser#paramList}.
	 * @param ctx the parse tree
	 */
	void exitParamList(MugicParser.ParamListContext ctx);
	/**
	 * Enter a parse tree produced by {@link MugicParser#variableDefinition1}.
	 * @param ctx the parse tree
	 */
	void enterVariableDefinition1(MugicParser.VariableDefinition1Context ctx);
	/**
	 * Exit a parse tree produced by {@link MugicParser#variableDefinition1}.
	 * @param ctx the parse tree
	 */
	void exitVariableDefinition1(MugicParser.VariableDefinition1Context ctx);
	/**
	 * Enter a parse tree produced by {@link MugicParser#variableDefinition2}.
	 * @param ctx the parse tree
	 */
	void enterVariableDefinition2(MugicParser.VariableDefinition2Context ctx);
	/**
	 * Exit a parse tree produced by {@link MugicParser#variableDefinition2}.
	 * @param ctx the parse tree
	 */
	void exitVariableDefinition2(MugicParser.VariableDefinition2Context ctx);
	/**
	 * Enter a parse tree produced by {@link MugicParser#constant}.
	 * @param ctx the parse tree
	 */
	void enterConstant(MugicParser.ConstantContext ctx);
	/**
	 * Exit a parse tree produced by {@link MugicParser#constant}.
	 * @param ctx the parse tree
	 */
	void exitConstant(MugicParser.ConstantContext ctx);
}