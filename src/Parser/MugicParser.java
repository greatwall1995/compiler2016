// Generated from Mugic.g4 by ANTLR 4.5.1

package Parser;

import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class MugicParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.5.1", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, T__8=9, 
		T__9=10, T__10=11, T__11=12, T__12=13, T__13=14, T__14=15, T__15=16, T__16=17, 
		T__17=18, T__18=19, T__19=20, T__20=21, T__21=22, T__22=23, T__23=24, 
		T__24=25, T__25=26, T__26=27, T__27=28, T__28=29, T__29=30, T__30=31, 
		T__31=32, T__32=33, T__33=34, T__34=35, T__35=36, T__36=37, T__37=38, 
		T__38=39, T__39=40, New=41, Braket=42, Dot=43, TypeName=44, Bool=45, Int=46, 
		Void=47, String=48, Null=49, BoolConstant=50, True=51, False=52, IntegerConstant=53, 
		StringConstant=54, ID=55, WS=56, Comment=57, Letter=58, Digit=59;
	public static final int
		RULE_stmt = 0, RULE_compoundStmt = 1, RULE_forStmt = 2, RULE_whileStmt = 3, 
		RULE_ifStmt = 4, RULE_jumpStmt = 5, RULE_expr = 6, RULE_primaryExpr = 7, 
		RULE_postfixExpr = 8, RULE_unaryExpr = 9, RULE_multiplicativeExpr = 10, 
		RULE_additiveExpr = 11, RULE_shiftExpr = 12, RULE_relationalExpr = 13, 
		RULE_equalityExpr = 14, RULE_andExpr = 15, RULE_exclusiveOrExpr = 16, 
		RULE_inclusiveOrExpr = 17, RULE_logicalAndExpr = 18, RULE_logicalOrExpr = 19, 
		RULE_assignmentExpr = 20, RULE_main = 21, RULE_classDefinition = 22, RULE_classList = 23, 
		RULE_translationUnit = 24, RULE_functionDefinition = 25, RULE_functionDefinition1 = 26, 
		RULE_functionDefinition2 = 27, RULE_paramList = 28, RULE_variableDefinition1 = 29, 
		RULE_variableDefinition2 = 30, RULE_constant = 31;
	public static final String[] ruleNames = {
		"stmt", "compoundStmt", "forStmt", "whileStmt", "ifStmt", "jumpStmt", 
		"expr", "primaryExpr", "postfixExpr", "unaryExpr", "multiplicativeExpr", 
		"additiveExpr", "shiftExpr", "relationalExpr", "equalityExpr", "andExpr", 
		"exclusiveOrExpr", "inclusiveOrExpr", "logicalAndExpr", "logicalOrExpr", 
		"assignmentExpr", "main", "classDefinition", "classList", "translationUnit", 
		"functionDefinition", "functionDefinition1", "functionDefinition2", "paramList", 
		"variableDefinition1", "variableDefinition2", "constant"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "';'", "'{'", "'}'", "'for'", "'('", "')'", "'while'", "'if'", "'else'", 
		"'break'", "'continue'", "'return'", "'()'", "','", "'['", "']'", "'++'", 
		"'--'", "'!'", "'~'", "'+'", "'-'", "'*'", "'/'", "'%'", "'<<'", "'>>'", 
		"'<'", "'>'", "'<='", "'>='", "'=='", "'!='", "'&'", "'^'", "'|'", "'&&'", 
		"'||'", "'='", "'class'", "'new'", "'[]'", "'.'", null, "'bool'", "'int'", 
		"'void'", "'string'", "'null'", null, "'true'", "'false'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, "New", "Braket", "Dot", "TypeName", "Bool", 
		"Int", "Void", "String", "Null", "BoolConstant", "True", "False", "IntegerConstant", 
		"StringConstant", "ID", "WS", "Comment", "Letter", "Digit"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "Mugic.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public MugicParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class StmtContext extends ParserRuleContext {
		public StmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_stmt; }
	 
		public StmtContext() { }
		public void copyFrom(StmtContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class ForContext extends StmtContext {
		public ForStmtContext forStmt() {
			return getRuleContext(ForStmtContext.class,0);
		}
		public ForContext(StmtContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).enterFor(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).exitFor(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MugicVisitor ) return ((MugicVisitor<? extends T>)visitor).visitFor(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class VarDefContext extends StmtContext {
		public VariableDefinition2Context variableDefinition2() {
			return getRuleContext(VariableDefinition2Context.class,0);
		}
		public VarDefContext(StmtContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).enterVarDef(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).exitVarDef(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MugicVisitor ) return ((MugicVisitor<? extends T>)visitor).visitVarDef(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class EpContext extends StmtContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public EpContext(StmtContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).enterEp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).exitEp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MugicVisitor ) return ((MugicVisitor<? extends T>)visitor).visitEp(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class WhileContext extends StmtContext {
		public WhileStmtContext whileStmt() {
			return getRuleContext(WhileStmtContext.class,0);
		}
		public WhileContext(StmtContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).enterWhile(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).exitWhile(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MugicVisitor ) return ((MugicVisitor<? extends T>)visitor).visitWhile(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class CompoundContext extends StmtContext {
		public CompoundStmtContext compoundStmt() {
			return getRuleContext(CompoundStmtContext.class,0);
		}
		public CompoundContext(StmtContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).enterCompound(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).exitCompound(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MugicVisitor ) return ((MugicVisitor<? extends T>)visitor).visitCompound(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IfContext extends StmtContext {
		public IfStmtContext ifStmt() {
			return getRuleContext(IfStmtContext.class,0);
		}
		public IfContext(StmtContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).enterIf(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).exitIf(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MugicVisitor ) return ((MugicVisitor<? extends T>)visitor).visitIf(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class NullExpContext extends StmtContext {
		public NullExpContext(StmtContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).enterNullExp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).exitNullExp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MugicVisitor ) return ((MugicVisitor<? extends T>)visitor).visitNullExp(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class JumpContext extends StmtContext {
		public JumpStmtContext jumpStmt() {
			return getRuleContext(JumpStmtContext.class,0);
		}
		public JumpContext(StmtContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).enterJump(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).exitJump(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MugicVisitor ) return ((MugicVisitor<? extends T>)visitor).visitJump(this);
			else return visitor.visitChildren(this);
		}
	}

	public final StmtContext stmt() throws RecognitionException {
		StmtContext _localctx = new StmtContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_stmt);
		try {
			setState(74);
			switch ( getInterpreter().adaptivePredict(_input,0,_ctx) ) {
			case 1:
				_localctx = new CompoundContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(64);
				compoundStmt();
				}
				break;
			case 2:
				_localctx = new ForContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(65);
				forStmt();
				}
				break;
			case 3:
				_localctx = new WhileContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(66);
				whileStmt();
				}
				break;
			case 4:
				_localctx = new IfContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(67);
				ifStmt();
				}
				break;
			case 5:
				_localctx = new JumpContext(_localctx);
				enterOuterAlt(_localctx, 5);
				{
				setState(68);
				jumpStmt();
				}
				break;
			case 6:
				_localctx = new VarDefContext(_localctx);
				enterOuterAlt(_localctx, 6);
				{
				setState(69);
				variableDefinition2();
				}
				break;
			case 7:
				_localctx = new EpContext(_localctx);
				enterOuterAlt(_localctx, 7);
				{
				setState(70);
				expr();
				setState(71);
				match(T__0);
				}
				break;
			case 8:
				_localctx = new NullExpContext(_localctx);
				enterOuterAlt(_localctx, 8);
				{
				setState(73);
				match(T__0);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CompoundStmtContext extends ParserRuleContext {
		public List<StmtContext> stmt() {
			return getRuleContexts(StmtContext.class);
		}
		public StmtContext stmt(int i) {
			return getRuleContext(StmtContext.class,i);
		}
		public CompoundStmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_compoundStmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).enterCompoundStmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).exitCompoundStmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MugicVisitor ) return ((MugicVisitor<? extends T>)visitor).visitCompoundStmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CompoundStmtContext compoundStmt() throws RecognitionException {
		CompoundStmtContext _localctx = new CompoundStmtContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_compoundStmt);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(76);
			match(T__1);
			setState(80);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__0) | (1L << T__1) | (1L << T__3) | (1L << T__4) | (1L << T__6) | (1L << T__7) | (1L << T__9) | (1L << T__10) | (1L << T__11) | (1L << T__16) | (1L << T__17) | (1L << T__18) | (1L << T__19) | (1L << T__20) | (1L << T__21) | (1L << New) | (1L << TypeName) | (1L << Null) | (1L << BoolConstant) | (1L << IntegerConstant) | (1L << StringConstant) | (1L << ID))) != 0)) {
				{
				{
				setState(77);
				stmt();
				}
				}
				setState(82);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(83);
			match(T__2);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ForStmtContext extends ParserRuleContext {
		public ExprContext exp1;
		public ExprContext exp2;
		public ExprContext exp3;
		public StmtContext stmt() {
			return getRuleContext(StmtContext.class,0);
		}
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public ForStmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_forStmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).enterForStmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).exitForStmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MugicVisitor ) return ((MugicVisitor<? extends T>)visitor).visitForStmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ForStmtContext forStmt() throws RecognitionException {
		ForStmtContext _localctx = new ForStmtContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_forStmt);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(85);
			match(T__3);
			setState(86);
			match(T__4);
			{
			setState(88);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__4) | (1L << T__16) | (1L << T__17) | (1L << T__18) | (1L << T__19) | (1L << T__20) | (1L << T__21) | (1L << New) | (1L << Null) | (1L << BoolConstant) | (1L << IntegerConstant) | (1L << StringConstant) | (1L << ID))) != 0)) {
				{
				setState(87);
				((ForStmtContext)_localctx).exp1 = expr();
				}
			}

			}
			setState(90);
			match(T__0);
			{
			setState(92);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__4) | (1L << T__16) | (1L << T__17) | (1L << T__18) | (1L << T__19) | (1L << T__20) | (1L << T__21) | (1L << New) | (1L << Null) | (1L << BoolConstant) | (1L << IntegerConstant) | (1L << StringConstant) | (1L << ID))) != 0)) {
				{
				setState(91);
				((ForStmtContext)_localctx).exp2 = expr();
				}
			}

			}
			setState(94);
			match(T__0);
			{
			setState(96);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__4) | (1L << T__16) | (1L << T__17) | (1L << T__18) | (1L << T__19) | (1L << T__20) | (1L << T__21) | (1L << New) | (1L << Null) | (1L << BoolConstant) | (1L << IntegerConstant) | (1L << StringConstant) | (1L << ID))) != 0)) {
				{
				setState(95);
				((ForStmtContext)_localctx).exp3 = expr();
				}
			}

			}
			setState(98);
			match(T__5);
			setState(99);
			stmt();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class WhileStmtContext extends ParserRuleContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public StmtContext stmt() {
			return getRuleContext(StmtContext.class,0);
		}
		public WhileStmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_whileStmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).enterWhileStmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).exitWhileStmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MugicVisitor ) return ((MugicVisitor<? extends T>)visitor).visitWhileStmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final WhileStmtContext whileStmt() throws RecognitionException {
		WhileStmtContext _localctx = new WhileStmtContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_whileStmt);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(101);
			match(T__6);
			setState(102);
			match(T__4);
			setState(103);
			expr();
			setState(104);
			match(T__5);
			setState(105);
			stmt();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IfStmtContext extends ParserRuleContext {
		public StmtContext stmt1;
		public StmtContext stmt2;
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public List<StmtContext> stmt() {
			return getRuleContexts(StmtContext.class);
		}
		public StmtContext stmt(int i) {
			return getRuleContext(StmtContext.class,i);
		}
		public IfStmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ifStmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).enterIfStmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).exitIfStmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MugicVisitor ) return ((MugicVisitor<? extends T>)visitor).visitIfStmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final IfStmtContext ifStmt() throws RecognitionException {
		IfStmtContext _localctx = new IfStmtContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_ifStmt);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(107);
			match(T__7);
			setState(108);
			match(T__4);
			setState(109);
			expr();
			setState(110);
			match(T__5);
			setState(111);
			((IfStmtContext)_localctx).stmt1 = stmt();
			setState(114);
			switch ( getInterpreter().adaptivePredict(_input,5,_ctx) ) {
			case 1:
				{
				setState(112);
				match(T__8);
				setState(113);
				((IfStmtContext)_localctx).stmt2 = stmt();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class JumpStmtContext extends ParserRuleContext {
		public JumpStmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_jumpStmt; }
	 
		public JumpStmtContext() { }
		public void copyFrom(JumpStmtContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class ReturnContext extends JumpStmtContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public ReturnContext(JumpStmtContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).enterReturn(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).exitReturn(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MugicVisitor ) return ((MugicVisitor<? extends T>)visitor).visitReturn(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BreakContext extends JumpStmtContext {
		public BreakContext(JumpStmtContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).enterBreak(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).exitBreak(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MugicVisitor ) return ((MugicVisitor<? extends T>)visitor).visitBreak(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ContinueContext extends JumpStmtContext {
		public ContinueContext(JumpStmtContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).enterContinue(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).exitContinue(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MugicVisitor ) return ((MugicVisitor<? extends T>)visitor).visitContinue(this);
			else return visitor.visitChildren(this);
		}
	}

	public final JumpStmtContext jumpStmt() throws RecognitionException {
		JumpStmtContext _localctx = new JumpStmtContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_jumpStmt);
		int _la;
		try {
			setState(125);
			switch (_input.LA(1)) {
			case T__9:
				_localctx = new BreakContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(116);
				match(T__9);
				setState(117);
				match(T__0);
				}
				break;
			case T__10:
				_localctx = new ContinueContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(118);
				match(T__10);
				setState(119);
				match(T__0);
				}
				break;
			case T__11:
				_localctx = new ReturnContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(120);
				match(T__11);
				setState(122);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__4) | (1L << T__16) | (1L << T__17) | (1L << T__18) | (1L << T__19) | (1L << T__20) | (1L << T__21) | (1L << New) | (1L << Null) | (1L << BoolConstant) | (1L << IntegerConstant) | (1L << StringConstant) | (1L << ID))) != 0)) {
					{
					setState(121);
					expr();
					}
				}

				setState(124);
				match(T__0);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExprContext extends ParserRuleContext {
		public AssignmentExprContext assignmentExpr() {
			return getRuleContext(AssignmentExprContext.class,0);
		}
		public ExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).enterExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).exitExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MugicVisitor ) return ((MugicVisitor<? extends T>)visitor).visitExpr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExprContext expr() throws RecognitionException {
		ExprContext _localctx = new ExprContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_expr);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(127);
			assignmentExpr();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PrimaryExprContext extends ParserRuleContext {
		public PrimaryExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_primaryExpr; }
	 
		public PrimaryExprContext() { }
		public void copyFrom(PrimaryExprContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class FuncExpr2Context extends PrimaryExprContext {
		public TerminalNode ID() { return getToken(MugicParser.ID, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public FuncExpr2Context(PrimaryExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).enterFuncExpr2(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).exitFuncExpr2(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MugicVisitor ) return ((MugicVisitor<? extends T>)visitor).visitFuncExpr2(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ConstantExprContext extends PrimaryExprContext {
		public ConstantContext constant() {
			return getRuleContext(ConstantContext.class,0);
		}
		public ConstantExprContext(PrimaryExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).enterConstantExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).exitConstantExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MugicVisitor ) return ((MugicVisitor<? extends T>)visitor).visitConstantExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class FuncExpr1Context extends PrimaryExprContext {
		public TerminalNode ID() { return getToken(MugicParser.ID, 0); }
		public FuncExpr1Context(PrimaryExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).enterFuncExpr1(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).exitFuncExpr1(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MugicVisitor ) return ((MugicVisitor<? extends T>)visitor).visitFuncExpr1(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class CreationExprContext extends PrimaryExprContext {
		public Token type;
		public TerminalNode New() { return getToken(MugicParser.New, 0); }
		public TerminalNode TypeName() { return getToken(MugicParser.TypeName, 0); }
		public TerminalNode ID() { return getToken(MugicParser.ID, 0); }
		public List<TerminalNode> Braket() { return getTokens(MugicParser.Braket); }
		public TerminalNode Braket(int i) {
			return getToken(MugicParser.Braket, i);
		}
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public CreationExprContext(PrimaryExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).enterCreationExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).exitCreationExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MugicVisitor ) return ((MugicVisitor<? extends T>)visitor).visitCreationExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class VariableExprContext extends PrimaryExprContext {
		public TerminalNode ID() { return getToken(MugicParser.ID, 0); }
		public VariableExprContext(PrimaryExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).enterVariableExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).exitVariableExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MugicVisitor ) return ((MugicVisitor<? extends T>)visitor).visitVariableExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ParenExprContext extends PrimaryExprContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public ParenExprContext(PrimaryExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).enterParenExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).exitParenExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MugicVisitor ) return ((MugicVisitor<? extends T>)visitor).visitParenExpr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PrimaryExprContext primaryExpr() throws RecognitionException {
		PrimaryExprContext _localctx = new PrimaryExprContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_primaryExpr);
		int _la;
		try {
			int _alt;
			setState(161);
			switch ( getInterpreter().adaptivePredict(_input,11,_ctx) ) {
			case 1:
				_localctx = new VariableExprContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(129);
				match(ID);
				}
				break;
			case 2:
				_localctx = new FuncExpr1Context(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(130);
				match(ID);
				setState(131);
				match(T__12);
				}
				break;
			case 3:
				_localctx = new FuncExpr2Context(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(132);
				match(ID);
				setState(133);
				match(T__4);
				setState(134);
				expr();
				setState(139);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__13) {
					{
					{
					setState(135);
					match(T__13);
					setState(136);
					expr();
					}
					}
					setState(141);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(142);
				match(T__5);
				}
				break;
			case 4:
				_localctx = new ConstantExprContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(144);
				constant();
				}
				break;
			case 5:
				_localctx = new ParenExprContext(_localctx);
				enterOuterAlt(_localctx, 5);
				{
				setState(145);
				match(T__4);
				setState(146);
				expr();
				setState(147);
				match(T__5);
				}
				break;
			case 6:
				_localctx = new CreationExprContext(_localctx);
				enterOuterAlt(_localctx, 6);
				{
				setState(149);
				match(New);
				setState(150);
				((CreationExprContext)_localctx).type = _input.LT(1);
				_la = _input.LA(1);
				if ( !(_la==TypeName || _la==ID) ) {
					((CreationExprContext)_localctx).type = (Token)_errHandler.recoverInline(this);
				} else {
					consume();
				}
				setState(158);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,10,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						setState(156);
						switch (_input.LA(1)) {
						case T__14:
							{
							{
							setState(151);
							match(T__14);
							setState(152);
							expr();
							setState(153);
							match(T__15);
							}
							}
							break;
						case Braket:
							{
							setState(155);
							match(Braket);
							}
							break;
						default:
							throw new NoViableAltException(this);
						}
						} 
					}
					setState(160);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,10,_ctx);
				}
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PostfixExprContext extends ParserRuleContext {
		public PostfixExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_postfixExpr; }
	 
		public PostfixExprContext() { }
		public void copyFrom(PostfixExprContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class PrExprContext extends PostfixExprContext {
		public PrimaryExprContext primaryExpr() {
			return getRuleContext(PrimaryExprContext.class,0);
		}
		public PrExprContext(PostfixExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).enterPrExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).exitPrExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MugicVisitor ) return ((MugicVisitor<? extends T>)visitor).visitPrExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class RightExprContext extends PostfixExprContext {
		public Token op;
		public PostfixExprContext postfixExpr() {
			return getRuleContext(PostfixExprContext.class,0);
		}
		public RightExprContext(PostfixExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).enterRightExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).exitRightExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MugicVisitor ) return ((MugicVisitor<? extends T>)visitor).visitRightExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ArrayExprContext extends PostfixExprContext {
		public PostfixExprContext postfixExpr() {
			return getRuleContext(PostfixExprContext.class,0);
		}
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public ArrayExprContext(PostfixExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).enterArrayExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).exitArrayExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MugicVisitor ) return ((MugicVisitor<? extends T>)visitor).visitArrayExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ClassExprContext extends PostfixExprContext {
		public PostfixExprContext postfixExpr() {
			return getRuleContext(PostfixExprContext.class,0);
		}
		public TerminalNode Dot() { return getToken(MugicParser.Dot, 0); }
		public PrimaryExprContext primaryExpr() {
			return getRuleContext(PrimaryExprContext.class,0);
		}
		public ClassExprContext(PostfixExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).enterClassExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).exitClassExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MugicVisitor ) return ((MugicVisitor<? extends T>)visitor).visitClassExpr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PostfixExprContext postfixExpr() throws RecognitionException {
		return postfixExpr(0);
	}

	private PostfixExprContext postfixExpr(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		PostfixExprContext _localctx = new PostfixExprContext(_ctx, _parentState);
		PostfixExprContext _prevctx = _localctx;
		int _startState = 16;
		enterRecursionRule(_localctx, 16, RULE_postfixExpr, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			_localctx = new PrExprContext(_localctx);
			_ctx = _localctx;
			_prevctx = _localctx;

			setState(164);
			primaryExpr();
			}
			_ctx.stop = _input.LT(-1);
			setState(178);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,13,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(176);
					switch ( getInterpreter().adaptivePredict(_input,12,_ctx) ) {
					case 1:
						{
						_localctx = new ArrayExprContext(new PostfixExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_postfixExpr);
						setState(166);
						if (!(precpred(_ctx, 3))) throw new FailedPredicateException(this, "precpred(_ctx, 3)");
						setState(167);
						match(T__14);
						setState(168);
						expr();
						setState(169);
						match(T__15);
						}
						break;
					case 2:
						{
						_localctx = new ClassExprContext(new PostfixExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_postfixExpr);
						setState(171);
						if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
						setState(172);
						match(Dot);
						setState(173);
						primaryExpr();
						}
						break;
					case 3:
						{
						_localctx = new RightExprContext(new PostfixExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_postfixExpr);
						setState(174);
						if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
						setState(175);
						((RightExprContext)_localctx).op = _input.LT(1);
						_la = _input.LA(1);
						if ( !(_la==T__16 || _la==T__17) ) {
							((RightExprContext)_localctx).op = (Token)_errHandler.recoverInline(this);
						} else {
							consume();
						}
						}
						break;
					}
					} 
				}
				setState(180);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,13,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class UnaryExprContext extends ParserRuleContext {
		public Token op;
		public PostfixExprContext postfixExpr() {
			return getRuleContext(PostfixExprContext.class,0);
		}
		public UnaryExprContext unaryExpr() {
			return getRuleContext(UnaryExprContext.class,0);
		}
		public UnaryExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_unaryExpr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).enterUnaryExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).exitUnaryExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MugicVisitor ) return ((MugicVisitor<? extends T>)visitor).visitUnaryExpr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final UnaryExprContext unaryExpr() throws RecognitionException {
		UnaryExprContext _localctx = new UnaryExprContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_unaryExpr);
		int _la;
		try {
			setState(184);
			switch (_input.LA(1)) {
			case T__4:
			case New:
			case Null:
			case BoolConstant:
			case IntegerConstant:
			case StringConstant:
			case ID:
				enterOuterAlt(_localctx, 1);
				{
				setState(181);
				postfixExpr(0);
				}
				break;
			case T__16:
			case T__17:
			case T__18:
			case T__19:
			case T__20:
			case T__21:
				enterOuterAlt(_localctx, 2);
				{
				setState(182);
				((UnaryExprContext)_localctx).op = _input.LT(1);
				_la = _input.LA(1);
				if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__16) | (1L << T__17) | (1L << T__18) | (1L << T__19) | (1L << T__20) | (1L << T__21))) != 0)) ) {
					((UnaryExprContext)_localctx).op = (Token)_errHandler.recoverInline(this);
				} else {
					consume();
				}
				setState(183);
				unaryExpr();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MultiplicativeExprContext extends ParserRuleContext {
		public Token op;
		public UnaryExprContext unaryExpr() {
			return getRuleContext(UnaryExprContext.class,0);
		}
		public MultiplicativeExprContext multiplicativeExpr() {
			return getRuleContext(MultiplicativeExprContext.class,0);
		}
		public MultiplicativeExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_multiplicativeExpr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).enterMultiplicativeExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).exitMultiplicativeExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MugicVisitor ) return ((MugicVisitor<? extends T>)visitor).visitMultiplicativeExpr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MultiplicativeExprContext multiplicativeExpr() throws RecognitionException {
		return multiplicativeExpr(0);
	}

	private MultiplicativeExprContext multiplicativeExpr(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		MultiplicativeExprContext _localctx = new MultiplicativeExprContext(_ctx, _parentState);
		MultiplicativeExprContext _prevctx = _localctx;
		int _startState = 20;
		enterRecursionRule(_localctx, 20, RULE_multiplicativeExpr, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(187);
			unaryExpr();
			}
			_ctx.stop = _input.LT(-1);
			setState(194);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,15,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new MultiplicativeExprContext(_parentctx, _parentState);
					pushNewRecursionContext(_localctx, _startState, RULE_multiplicativeExpr);
					setState(189);
					if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
					setState(190);
					((MultiplicativeExprContext)_localctx).op = _input.LT(1);
					_la = _input.LA(1);
					if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__22) | (1L << T__23) | (1L << T__24))) != 0)) ) {
						((MultiplicativeExprContext)_localctx).op = (Token)_errHandler.recoverInline(this);
					} else {
						consume();
					}
					setState(191);
					unaryExpr();
					}
					} 
				}
				setState(196);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,15,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class AdditiveExprContext extends ParserRuleContext {
		public Token op;
		public MultiplicativeExprContext multiplicativeExpr() {
			return getRuleContext(MultiplicativeExprContext.class,0);
		}
		public AdditiveExprContext additiveExpr() {
			return getRuleContext(AdditiveExprContext.class,0);
		}
		public AdditiveExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_additiveExpr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).enterAdditiveExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).exitAdditiveExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MugicVisitor ) return ((MugicVisitor<? extends T>)visitor).visitAdditiveExpr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AdditiveExprContext additiveExpr() throws RecognitionException {
		return additiveExpr(0);
	}

	private AdditiveExprContext additiveExpr(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		AdditiveExprContext _localctx = new AdditiveExprContext(_ctx, _parentState);
		AdditiveExprContext _prevctx = _localctx;
		int _startState = 22;
		enterRecursionRule(_localctx, 22, RULE_additiveExpr, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(198);
			multiplicativeExpr(0);
			}
			_ctx.stop = _input.LT(-1);
			setState(205);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,16,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new AdditiveExprContext(_parentctx, _parentState);
					pushNewRecursionContext(_localctx, _startState, RULE_additiveExpr);
					setState(200);
					if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
					setState(201);
					((AdditiveExprContext)_localctx).op = _input.LT(1);
					_la = _input.LA(1);
					if ( !(_la==T__20 || _la==T__21) ) {
						((AdditiveExprContext)_localctx).op = (Token)_errHandler.recoverInline(this);
					} else {
						consume();
					}
					setState(202);
					multiplicativeExpr(0);
					}
					} 
				}
				setState(207);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,16,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class ShiftExprContext extends ParserRuleContext {
		public Token op;
		public AdditiveExprContext additiveExpr() {
			return getRuleContext(AdditiveExprContext.class,0);
		}
		public ShiftExprContext shiftExpr() {
			return getRuleContext(ShiftExprContext.class,0);
		}
		public ShiftExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_shiftExpr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).enterShiftExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).exitShiftExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MugicVisitor ) return ((MugicVisitor<? extends T>)visitor).visitShiftExpr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ShiftExprContext shiftExpr() throws RecognitionException {
		return shiftExpr(0);
	}

	private ShiftExprContext shiftExpr(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		ShiftExprContext _localctx = new ShiftExprContext(_ctx, _parentState);
		ShiftExprContext _prevctx = _localctx;
		int _startState = 24;
		enterRecursionRule(_localctx, 24, RULE_shiftExpr, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(209);
			additiveExpr(0);
			}
			_ctx.stop = _input.LT(-1);
			setState(216);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,17,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new ShiftExprContext(_parentctx, _parentState);
					pushNewRecursionContext(_localctx, _startState, RULE_shiftExpr);
					setState(211);
					if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
					setState(212);
					((ShiftExprContext)_localctx).op = _input.LT(1);
					_la = _input.LA(1);
					if ( !(_la==T__25 || _la==T__26) ) {
						((ShiftExprContext)_localctx).op = (Token)_errHandler.recoverInline(this);
					} else {
						consume();
					}
					setState(213);
					additiveExpr(0);
					}
					} 
				}
				setState(218);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,17,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class RelationalExprContext extends ParserRuleContext {
		public Token op;
		public ShiftExprContext shiftExpr() {
			return getRuleContext(ShiftExprContext.class,0);
		}
		public RelationalExprContext relationalExpr() {
			return getRuleContext(RelationalExprContext.class,0);
		}
		public RelationalExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_relationalExpr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).enterRelationalExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).exitRelationalExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MugicVisitor ) return ((MugicVisitor<? extends T>)visitor).visitRelationalExpr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final RelationalExprContext relationalExpr() throws RecognitionException {
		return relationalExpr(0);
	}

	private RelationalExprContext relationalExpr(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		RelationalExprContext _localctx = new RelationalExprContext(_ctx, _parentState);
		RelationalExprContext _prevctx = _localctx;
		int _startState = 26;
		enterRecursionRule(_localctx, 26, RULE_relationalExpr, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(220);
			shiftExpr(0);
			}
			_ctx.stop = _input.LT(-1);
			setState(227);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,18,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new RelationalExprContext(_parentctx, _parentState);
					pushNewRecursionContext(_localctx, _startState, RULE_relationalExpr);
					setState(222);
					if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
					setState(223);
					((RelationalExprContext)_localctx).op = _input.LT(1);
					_la = _input.LA(1);
					if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__27) | (1L << T__28) | (1L << T__29) | (1L << T__30))) != 0)) ) {
						((RelationalExprContext)_localctx).op = (Token)_errHandler.recoverInline(this);
					} else {
						consume();
					}
					setState(224);
					shiftExpr(0);
					}
					} 
				}
				setState(229);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,18,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class EqualityExprContext extends ParserRuleContext {
		public Token op;
		public RelationalExprContext relationalExpr() {
			return getRuleContext(RelationalExprContext.class,0);
		}
		public EqualityExprContext equalityExpr() {
			return getRuleContext(EqualityExprContext.class,0);
		}
		public EqualityExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_equalityExpr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).enterEqualityExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).exitEqualityExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MugicVisitor ) return ((MugicVisitor<? extends T>)visitor).visitEqualityExpr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final EqualityExprContext equalityExpr() throws RecognitionException {
		return equalityExpr(0);
	}

	private EqualityExprContext equalityExpr(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		EqualityExprContext _localctx = new EqualityExprContext(_ctx, _parentState);
		EqualityExprContext _prevctx = _localctx;
		int _startState = 28;
		enterRecursionRule(_localctx, 28, RULE_equalityExpr, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(231);
			relationalExpr(0);
			}
			_ctx.stop = _input.LT(-1);
			setState(238);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,19,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new EqualityExprContext(_parentctx, _parentState);
					pushNewRecursionContext(_localctx, _startState, RULE_equalityExpr);
					setState(233);
					if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
					setState(234);
					((EqualityExprContext)_localctx).op = _input.LT(1);
					_la = _input.LA(1);
					if ( !(_la==T__31 || _la==T__32) ) {
						((EqualityExprContext)_localctx).op = (Token)_errHandler.recoverInline(this);
					} else {
						consume();
					}
					setState(235);
					relationalExpr(0);
					}
					} 
				}
				setState(240);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,19,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class AndExprContext extends ParserRuleContext {
		public Token op;
		public EqualityExprContext equalityExpr() {
			return getRuleContext(EqualityExprContext.class,0);
		}
		public AndExprContext andExpr() {
			return getRuleContext(AndExprContext.class,0);
		}
		public AndExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_andExpr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).enterAndExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).exitAndExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MugicVisitor ) return ((MugicVisitor<? extends T>)visitor).visitAndExpr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AndExprContext andExpr() throws RecognitionException {
		return andExpr(0);
	}

	private AndExprContext andExpr(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		AndExprContext _localctx = new AndExprContext(_ctx, _parentState);
		AndExprContext _prevctx = _localctx;
		int _startState = 30;
		enterRecursionRule(_localctx, 30, RULE_andExpr, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(242);
			equalityExpr(0);
			}
			_ctx.stop = _input.LT(-1);
			setState(249);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,20,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new AndExprContext(_parentctx, _parentState);
					pushNewRecursionContext(_localctx, _startState, RULE_andExpr);
					setState(244);
					if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
					setState(245);
					((AndExprContext)_localctx).op = match(T__33);
					setState(246);
					equalityExpr(0);
					}
					} 
				}
				setState(251);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,20,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class ExclusiveOrExprContext extends ParserRuleContext {
		public Token op;
		public AndExprContext andExpr() {
			return getRuleContext(AndExprContext.class,0);
		}
		public ExclusiveOrExprContext exclusiveOrExpr() {
			return getRuleContext(ExclusiveOrExprContext.class,0);
		}
		public ExclusiveOrExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_exclusiveOrExpr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).enterExclusiveOrExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).exitExclusiveOrExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MugicVisitor ) return ((MugicVisitor<? extends T>)visitor).visitExclusiveOrExpr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExclusiveOrExprContext exclusiveOrExpr() throws RecognitionException {
		return exclusiveOrExpr(0);
	}

	private ExclusiveOrExprContext exclusiveOrExpr(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		ExclusiveOrExprContext _localctx = new ExclusiveOrExprContext(_ctx, _parentState);
		ExclusiveOrExprContext _prevctx = _localctx;
		int _startState = 32;
		enterRecursionRule(_localctx, 32, RULE_exclusiveOrExpr, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(253);
			andExpr(0);
			}
			_ctx.stop = _input.LT(-1);
			setState(260);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,21,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new ExclusiveOrExprContext(_parentctx, _parentState);
					pushNewRecursionContext(_localctx, _startState, RULE_exclusiveOrExpr);
					setState(255);
					if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
					setState(256);
					((ExclusiveOrExprContext)_localctx).op = match(T__34);
					setState(257);
					andExpr(0);
					}
					} 
				}
				setState(262);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,21,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class InclusiveOrExprContext extends ParserRuleContext {
		public Token op;
		public ExclusiveOrExprContext exclusiveOrExpr() {
			return getRuleContext(ExclusiveOrExprContext.class,0);
		}
		public InclusiveOrExprContext inclusiveOrExpr() {
			return getRuleContext(InclusiveOrExprContext.class,0);
		}
		public InclusiveOrExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_inclusiveOrExpr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).enterInclusiveOrExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).exitInclusiveOrExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MugicVisitor ) return ((MugicVisitor<? extends T>)visitor).visitInclusiveOrExpr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final InclusiveOrExprContext inclusiveOrExpr() throws RecognitionException {
		return inclusiveOrExpr(0);
	}

	private InclusiveOrExprContext inclusiveOrExpr(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		InclusiveOrExprContext _localctx = new InclusiveOrExprContext(_ctx, _parentState);
		InclusiveOrExprContext _prevctx = _localctx;
		int _startState = 34;
		enterRecursionRule(_localctx, 34, RULE_inclusiveOrExpr, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(264);
			exclusiveOrExpr(0);
			}
			_ctx.stop = _input.LT(-1);
			setState(271);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,22,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new InclusiveOrExprContext(_parentctx, _parentState);
					pushNewRecursionContext(_localctx, _startState, RULE_inclusiveOrExpr);
					setState(266);
					if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
					setState(267);
					((InclusiveOrExprContext)_localctx).op = match(T__35);
					setState(268);
					exclusiveOrExpr(0);
					}
					} 
				}
				setState(273);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,22,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class LogicalAndExprContext extends ParserRuleContext {
		public Token op;
		public InclusiveOrExprContext inclusiveOrExpr() {
			return getRuleContext(InclusiveOrExprContext.class,0);
		}
		public LogicalAndExprContext logicalAndExpr() {
			return getRuleContext(LogicalAndExprContext.class,0);
		}
		public LogicalAndExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_logicalAndExpr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).enterLogicalAndExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).exitLogicalAndExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MugicVisitor ) return ((MugicVisitor<? extends T>)visitor).visitLogicalAndExpr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LogicalAndExprContext logicalAndExpr() throws RecognitionException {
		LogicalAndExprContext _localctx = new LogicalAndExprContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_logicalAndExpr);
		try {
			setState(279);
			switch ( getInterpreter().adaptivePredict(_input,23,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(274);
				inclusiveOrExpr(0);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(275);
				inclusiveOrExpr(0);
				setState(276);
				((LogicalAndExprContext)_localctx).op = match(T__36);
				setState(277);
				logicalAndExpr();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LogicalOrExprContext extends ParserRuleContext {
		public Token op;
		public LogicalAndExprContext logicalAndExpr() {
			return getRuleContext(LogicalAndExprContext.class,0);
		}
		public LogicalOrExprContext logicalOrExpr() {
			return getRuleContext(LogicalOrExprContext.class,0);
		}
		public LogicalOrExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_logicalOrExpr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).enterLogicalOrExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).exitLogicalOrExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MugicVisitor ) return ((MugicVisitor<? extends T>)visitor).visitLogicalOrExpr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LogicalOrExprContext logicalOrExpr() throws RecognitionException {
		LogicalOrExprContext _localctx = new LogicalOrExprContext(_ctx, getState());
		enterRule(_localctx, 38, RULE_logicalOrExpr);
		try {
			setState(286);
			switch ( getInterpreter().adaptivePredict(_input,24,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(281);
				logicalAndExpr();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(282);
				logicalAndExpr();
				setState(283);
				((LogicalOrExprContext)_localctx).op = match(T__37);
				setState(284);
				logicalOrExpr();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AssignmentExprContext extends ParserRuleContext {
		public Token op;
		public LogicalOrExprContext logicalOrExpr() {
			return getRuleContext(LogicalOrExprContext.class,0);
		}
		public UnaryExprContext unaryExpr() {
			return getRuleContext(UnaryExprContext.class,0);
		}
		public AssignmentExprContext assignmentExpr() {
			return getRuleContext(AssignmentExprContext.class,0);
		}
		public AssignmentExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_assignmentExpr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).enterAssignmentExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).exitAssignmentExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MugicVisitor ) return ((MugicVisitor<? extends T>)visitor).visitAssignmentExpr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AssignmentExprContext assignmentExpr() throws RecognitionException {
		AssignmentExprContext _localctx = new AssignmentExprContext(_ctx, getState());
		enterRule(_localctx, 40, RULE_assignmentExpr);
		try {
			setState(293);
			switch ( getInterpreter().adaptivePredict(_input,25,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(288);
				logicalOrExpr();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(289);
				unaryExpr();
				setState(290);
				((AssignmentExprContext)_localctx).op = match(T__38);
				setState(291);
				assignmentExpr();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MainContext extends ParserRuleContext {
		public TerminalNode EOF() { return getToken(MugicParser.EOF, 0); }
		public TranslationUnitContext translationUnit() {
			return getRuleContext(TranslationUnitContext.class,0);
		}
		public MainContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_main; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).enterMain(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).exitMain(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MugicVisitor ) return ((MugicVisitor<? extends T>)visitor).visitMain(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MainContext main() throws RecognitionException {
		MainContext _localctx = new MainContext(_ctx, getState());
		enterRule(_localctx, 42, RULE_main);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(296);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__39) | (1L << TypeName) | (1L << ID))) != 0)) {
				{
				setState(295);
				translationUnit();
				}
			}

			setState(298);
			match(EOF);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ClassDefinitionContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(MugicParser.ID, 0); }
		public ClassListContext classList() {
			return getRuleContext(ClassListContext.class,0);
		}
		public ClassDefinitionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_classDefinition; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).enterClassDefinition(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).exitClassDefinition(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MugicVisitor ) return ((MugicVisitor<? extends T>)visitor).visitClassDefinition(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ClassDefinitionContext classDefinition() throws RecognitionException {
		ClassDefinitionContext _localctx = new ClassDefinitionContext(_ctx, getState());
		enterRule(_localctx, 44, RULE_classDefinition);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(300);
			match(T__39);
			setState(301);
			match(ID);
			setState(302);
			match(T__1);
			setState(303);
			classList();
			setState(304);
			match(T__2);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ClassListContext extends ParserRuleContext {
		public Token type;
		public Token name;
		public List<TerminalNode> ID() { return getTokens(MugicParser.ID); }
		public TerminalNode ID(int i) {
			return getToken(MugicParser.ID, i);
		}
		public TerminalNode TypeName() { return getToken(MugicParser.TypeName, 0); }
		public List<TerminalNode> Braket() { return getTokens(MugicParser.Braket); }
		public TerminalNode Braket(int i) {
			return getToken(MugicParser.Braket, i);
		}
		public ClassListContext classList() {
			return getRuleContext(ClassListContext.class,0);
		}
		public ClassListContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_classList; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).enterClassList(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).exitClassList(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MugicVisitor ) return ((MugicVisitor<? extends T>)visitor).visitClassList(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ClassListContext classList() throws RecognitionException {
		ClassListContext _localctx = new ClassListContext(_ctx, getState());
		enterRule(_localctx, 46, RULE_classList);
		int _la;
		try {
			setState(325);
			switch ( getInterpreter().adaptivePredict(_input,29,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(306);
				((ClassListContext)_localctx).type = _input.LT(1);
				_la = _input.LA(1);
				if ( !(_la==TypeName || _la==ID) ) {
					((ClassListContext)_localctx).type = (Token)_errHandler.recoverInline(this);
				} else {
					consume();
				}
				setState(310);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==Braket) {
					{
					{
					setState(307);
					match(Braket);
					}
					}
					setState(312);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(313);
				((ClassListContext)_localctx).name = match(ID);
				setState(314);
				match(T__0);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(315);
				((ClassListContext)_localctx).type = _input.LT(1);
				_la = _input.LA(1);
				if ( !(_la==TypeName || _la==ID) ) {
					((ClassListContext)_localctx).type = (Token)_errHandler.recoverInline(this);
				} else {
					consume();
				}
				setState(319);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==Braket) {
					{
					{
					setState(316);
					match(Braket);
					}
					}
					setState(321);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(322);
				((ClassListContext)_localctx).name = match(ID);
				setState(323);
				match(T__0);
				setState(324);
				classList();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TranslationUnitContext extends ParserRuleContext {
		public TranslationUnitContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_translationUnit; }
	 
		public TranslationUnitContext() { }
		public void copyFrom(TranslationUnitContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class FuncDecContext extends TranslationUnitContext {
		public FunctionDefinitionContext functionDefinition() {
			return getRuleContext(FunctionDefinitionContext.class,0);
		}
		public FuncDecContext(TranslationUnitContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).enterFuncDec(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).exitFuncDec(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MugicVisitor ) return ((MugicVisitor<? extends T>)visitor).visitFuncDec(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ClassDec_Context extends TranslationUnitContext {
		public ClassDefinitionContext classDefinition() {
			return getRuleContext(ClassDefinitionContext.class,0);
		}
		public TranslationUnitContext translationUnit() {
			return getRuleContext(TranslationUnitContext.class,0);
		}
		public ClassDec_Context(TranslationUnitContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).enterClassDec_(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).exitClassDec_(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MugicVisitor ) return ((MugicVisitor<? extends T>)visitor).visitClassDec_(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class FuncDec_Context extends TranslationUnitContext {
		public FunctionDefinitionContext functionDefinition() {
			return getRuleContext(FunctionDefinitionContext.class,0);
		}
		public TranslationUnitContext translationUnit() {
			return getRuleContext(TranslationUnitContext.class,0);
		}
		public FuncDec_Context(TranslationUnitContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).enterFuncDec_(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).exitFuncDec_(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MugicVisitor ) return ((MugicVisitor<? extends T>)visitor).visitFuncDec_(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class VarDec_Context extends TranslationUnitContext {
		public VariableDefinition1Context variableDefinition1() {
			return getRuleContext(VariableDefinition1Context.class,0);
		}
		public TranslationUnitContext translationUnit() {
			return getRuleContext(TranslationUnitContext.class,0);
		}
		public VarDec_Context(TranslationUnitContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).enterVarDec_(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).exitVarDec_(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MugicVisitor ) return ((MugicVisitor<? extends T>)visitor).visitVarDec_(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class VarDecContext extends TranslationUnitContext {
		public VariableDefinition1Context variableDefinition1() {
			return getRuleContext(VariableDefinition1Context.class,0);
		}
		public VarDecContext(TranslationUnitContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).enterVarDec(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).exitVarDec(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MugicVisitor ) return ((MugicVisitor<? extends T>)visitor).visitVarDec(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ClassDecContext extends TranslationUnitContext {
		public ClassDefinitionContext classDefinition() {
			return getRuleContext(ClassDefinitionContext.class,0);
		}
		public ClassDecContext(TranslationUnitContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).enterClassDec(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).exitClassDec(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MugicVisitor ) return ((MugicVisitor<? extends T>)visitor).visitClassDec(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TranslationUnitContext translationUnit() throws RecognitionException {
		TranslationUnitContext _localctx = new TranslationUnitContext(_ctx, getState());
		enterRule(_localctx, 48, RULE_translationUnit);
		try {
			setState(339);
			switch ( getInterpreter().adaptivePredict(_input,30,_ctx) ) {
			case 1:
				_localctx = new FuncDecContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(327);
				functionDefinition();
				}
				break;
			case 2:
				_localctx = new VarDecContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(328);
				variableDefinition1();
				}
				break;
			case 3:
				_localctx = new ClassDecContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(329);
				classDefinition();
				}
				break;
			case 4:
				_localctx = new FuncDec_Context(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(330);
				functionDefinition();
				setState(331);
				translationUnit();
				}
				break;
			case 5:
				_localctx = new VarDec_Context(_localctx);
				enterOuterAlt(_localctx, 5);
				{
				setState(333);
				variableDefinition1();
				setState(334);
				translationUnit();
				}
				break;
			case 6:
				_localctx = new ClassDec_Context(_localctx);
				enterOuterAlt(_localctx, 6);
				{
				setState(336);
				classDefinition();
				setState(337);
				translationUnit();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FunctionDefinitionContext extends ParserRuleContext {
		public FunctionDefinition1Context functionDefinition1() {
			return getRuleContext(FunctionDefinition1Context.class,0);
		}
		public CompoundStmtContext compoundStmt() {
			return getRuleContext(CompoundStmtContext.class,0);
		}
		public FunctionDefinition2Context functionDefinition2() {
			return getRuleContext(FunctionDefinition2Context.class,0);
		}
		public FunctionDefinitionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_functionDefinition; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).enterFunctionDefinition(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).exitFunctionDefinition(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MugicVisitor ) return ((MugicVisitor<? extends T>)visitor).visitFunctionDefinition(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FunctionDefinitionContext functionDefinition() throws RecognitionException {
		FunctionDefinitionContext _localctx = new FunctionDefinitionContext(_ctx, getState());
		enterRule(_localctx, 50, RULE_functionDefinition);
		try {
			setState(347);
			switch ( getInterpreter().adaptivePredict(_input,31,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(341);
				functionDefinition1();
				setState(342);
				compoundStmt();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(344);
				functionDefinition2();
				setState(345);
				compoundStmt();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FunctionDefinition1Context extends ParserRuleContext {
		public Token funcType;
		public Token name;
		public List<TerminalNode> ID() { return getTokens(MugicParser.ID); }
		public TerminalNode ID(int i) {
			return getToken(MugicParser.ID, i);
		}
		public TerminalNode TypeName() { return getToken(MugicParser.TypeName, 0); }
		public List<TerminalNode> Braket() { return getTokens(MugicParser.Braket); }
		public TerminalNode Braket(int i) {
			return getToken(MugicParser.Braket, i);
		}
		public FunctionDefinition1Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_functionDefinition1; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).enterFunctionDefinition1(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).exitFunctionDefinition1(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MugicVisitor ) return ((MugicVisitor<? extends T>)visitor).visitFunctionDefinition1(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FunctionDefinition1Context functionDefinition1() throws RecognitionException {
		FunctionDefinition1Context _localctx = new FunctionDefinition1Context(_ctx, getState());
		enterRule(_localctx, 52, RULE_functionDefinition1);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(349);
			((FunctionDefinition1Context)_localctx).funcType = _input.LT(1);
			_la = _input.LA(1);
			if ( !(_la==TypeName || _la==ID) ) {
				((FunctionDefinition1Context)_localctx).funcType = (Token)_errHandler.recoverInline(this);
			} else {
				consume();
			}
			setState(353);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==Braket) {
				{
				{
				setState(350);
				match(Braket);
				}
				}
				setState(355);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(356);
			((FunctionDefinition1Context)_localctx).name = match(ID);
			setState(357);
			match(T__12);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FunctionDefinition2Context extends ParserRuleContext {
		public Token funcType;
		public Token name;
		public ParamListContext paramList() {
			return getRuleContext(ParamListContext.class,0);
		}
		public List<TerminalNode> ID() { return getTokens(MugicParser.ID); }
		public TerminalNode ID(int i) {
			return getToken(MugicParser.ID, i);
		}
		public TerminalNode TypeName() { return getToken(MugicParser.TypeName, 0); }
		public List<TerminalNode> Braket() { return getTokens(MugicParser.Braket); }
		public TerminalNode Braket(int i) {
			return getToken(MugicParser.Braket, i);
		}
		public FunctionDefinition2Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_functionDefinition2; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).enterFunctionDefinition2(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).exitFunctionDefinition2(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MugicVisitor ) return ((MugicVisitor<? extends T>)visitor).visitFunctionDefinition2(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FunctionDefinition2Context functionDefinition2() throws RecognitionException {
		FunctionDefinition2Context _localctx = new FunctionDefinition2Context(_ctx, getState());
		enterRule(_localctx, 54, RULE_functionDefinition2);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(359);
			((FunctionDefinition2Context)_localctx).funcType = _input.LT(1);
			_la = _input.LA(1);
			if ( !(_la==TypeName || _la==ID) ) {
				((FunctionDefinition2Context)_localctx).funcType = (Token)_errHandler.recoverInline(this);
			} else {
				consume();
			}
			setState(363);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==Braket) {
				{
				{
				setState(360);
				match(Braket);
				}
				}
				setState(365);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(366);
			((FunctionDefinition2Context)_localctx).name = match(ID);
			setState(367);
			match(T__4);
			setState(368);
			paramList();
			setState(369);
			match(T__5);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ParamListContext extends ParserRuleContext {
		public Token type;
		public Token name;
		public List<TerminalNode> ID() { return getTokens(MugicParser.ID); }
		public TerminalNode ID(int i) {
			return getToken(MugicParser.ID, i);
		}
		public TerminalNode TypeName() { return getToken(MugicParser.TypeName, 0); }
		public List<TerminalNode> Braket() { return getTokens(MugicParser.Braket); }
		public TerminalNode Braket(int i) {
			return getToken(MugicParser.Braket, i);
		}
		public ParamListContext paramList() {
			return getRuleContext(ParamListContext.class,0);
		}
		public ParamListContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_paramList; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).enterParamList(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).exitParamList(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MugicVisitor ) return ((MugicVisitor<? extends T>)visitor).visitParamList(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ParamListContext paramList() throws RecognitionException {
		ParamListContext _localctx = new ParamListContext(_ctx, getState());
		enterRule(_localctx, 56, RULE_paramList);
		int _la;
		try {
			setState(389);
			switch ( getInterpreter().adaptivePredict(_input,36,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(371);
				((ParamListContext)_localctx).type = _input.LT(1);
				_la = _input.LA(1);
				if ( !(_la==TypeName || _la==ID) ) {
					((ParamListContext)_localctx).type = (Token)_errHandler.recoverInline(this);
				} else {
					consume();
				}
				setState(375);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==Braket) {
					{
					{
					setState(372);
					match(Braket);
					}
					}
					setState(377);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(378);
				((ParamListContext)_localctx).name = match(ID);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(379);
				((ParamListContext)_localctx).type = _input.LT(1);
				_la = _input.LA(1);
				if ( !(_la==TypeName || _la==ID) ) {
					((ParamListContext)_localctx).type = (Token)_errHandler.recoverInline(this);
				} else {
					consume();
				}
				setState(383);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==Braket) {
					{
					{
					setState(380);
					match(Braket);
					}
					}
					setState(385);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(386);
				((ParamListContext)_localctx).name = match(ID);
				setState(387);
				match(T__13);
				setState(388);
				paramList();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class VariableDefinition1Context extends ParserRuleContext {
		public Token type;
		public Token name;
		public List<TerminalNode> ID() { return getTokens(MugicParser.ID); }
		public TerminalNode ID(int i) {
			return getToken(MugicParser.ID, i);
		}
		public TerminalNode TypeName() { return getToken(MugicParser.TypeName, 0); }
		public List<TerminalNode> Braket() { return getTokens(MugicParser.Braket); }
		public TerminalNode Braket(int i) {
			return getToken(MugicParser.Braket, i);
		}
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public VariableDefinition1Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_variableDefinition1; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).enterVariableDefinition1(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).exitVariableDefinition1(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MugicVisitor ) return ((MugicVisitor<? extends T>)visitor).visitVariableDefinition1(this);
			else return visitor.visitChildren(this);
		}
	}

	public final VariableDefinition1Context variableDefinition1() throws RecognitionException {
		VariableDefinition1Context _localctx = new VariableDefinition1Context(_ctx, getState());
		enterRule(_localctx, 58, RULE_variableDefinition1);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(391);
			((VariableDefinition1Context)_localctx).type = _input.LT(1);
			_la = _input.LA(1);
			if ( !(_la==TypeName || _la==ID) ) {
				((VariableDefinition1Context)_localctx).type = (Token)_errHandler.recoverInline(this);
			} else {
				consume();
			}
			setState(395);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==Braket) {
				{
				{
				setState(392);
				match(Braket);
				}
				}
				setState(397);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(398);
			((VariableDefinition1Context)_localctx).name = match(ID);
			setState(401);
			_la = _input.LA(1);
			if (_la==T__38) {
				{
				setState(399);
				match(T__38);
				setState(400);
				expr();
				}
			}

			setState(403);
			match(T__0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class VariableDefinition2Context extends ParserRuleContext {
		public Token type;
		public Token name;
		public List<TerminalNode> ID() { return getTokens(MugicParser.ID); }
		public TerminalNode ID(int i) {
			return getToken(MugicParser.ID, i);
		}
		public TerminalNode TypeName() { return getToken(MugicParser.TypeName, 0); }
		public List<TerminalNode> Braket() { return getTokens(MugicParser.Braket); }
		public TerminalNode Braket(int i) {
			return getToken(MugicParser.Braket, i);
		}
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public VariableDefinition2Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_variableDefinition2; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).enterVariableDefinition2(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).exitVariableDefinition2(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MugicVisitor ) return ((MugicVisitor<? extends T>)visitor).visitVariableDefinition2(this);
			else return visitor.visitChildren(this);
		}
	}

	public final VariableDefinition2Context variableDefinition2() throws RecognitionException {
		VariableDefinition2Context _localctx = new VariableDefinition2Context(_ctx, getState());
		enterRule(_localctx, 60, RULE_variableDefinition2);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(405);
			((VariableDefinition2Context)_localctx).type = _input.LT(1);
			_la = _input.LA(1);
			if ( !(_la==TypeName || _la==ID) ) {
				((VariableDefinition2Context)_localctx).type = (Token)_errHandler.recoverInline(this);
			} else {
				consume();
			}
			setState(409);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==Braket) {
				{
				{
				setState(406);
				match(Braket);
				}
				}
				setState(411);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(412);
			((VariableDefinition2Context)_localctx).name = match(ID);
			setState(415);
			_la = _input.LA(1);
			if (_la==T__38) {
				{
				setState(413);
				match(T__38);
				setState(414);
				expr();
				}
			}

			setState(417);
			match(T__0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ConstantContext extends ParserRuleContext {
		public TerminalNode BoolConstant() { return getToken(MugicParser.BoolConstant, 0); }
		public TerminalNode IntegerConstant() { return getToken(MugicParser.IntegerConstant, 0); }
		public TerminalNode StringConstant() { return getToken(MugicParser.StringConstant, 0); }
		public TerminalNode Null() { return getToken(MugicParser.Null, 0); }
		public ConstantContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_constant; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).enterConstant(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MugicListener ) ((MugicListener)listener).exitConstant(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MugicVisitor ) return ((MugicVisitor<? extends T>)visitor).visitConstant(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ConstantContext constant() throws RecognitionException {
		ConstantContext _localctx = new ConstantContext(_ctx, getState());
		enterRule(_localctx, 62, RULE_constant);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(419);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << Null) | (1L << BoolConstant) | (1L << IntegerConstant) | (1L << StringConstant))) != 0)) ) {
			_errHandler.recoverInline(this);
			} else {
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 8:
			return postfixExpr_sempred((PostfixExprContext)_localctx, predIndex);
		case 10:
			return multiplicativeExpr_sempred((MultiplicativeExprContext)_localctx, predIndex);
		case 11:
			return additiveExpr_sempred((AdditiveExprContext)_localctx, predIndex);
		case 12:
			return shiftExpr_sempred((ShiftExprContext)_localctx, predIndex);
		case 13:
			return relationalExpr_sempred((RelationalExprContext)_localctx, predIndex);
		case 14:
			return equalityExpr_sempred((EqualityExprContext)_localctx, predIndex);
		case 15:
			return andExpr_sempred((AndExprContext)_localctx, predIndex);
		case 16:
			return exclusiveOrExpr_sempred((ExclusiveOrExprContext)_localctx, predIndex);
		case 17:
			return inclusiveOrExpr_sempred((InclusiveOrExprContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean postfixExpr_sempred(PostfixExprContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0:
			return precpred(_ctx, 3);
		case 1:
			return precpred(_ctx, 2);
		case 2:
			return precpred(_ctx, 1);
		}
		return true;
	}
	private boolean multiplicativeExpr_sempred(MultiplicativeExprContext _localctx, int predIndex) {
		switch (predIndex) {
		case 3:
			return precpred(_ctx, 1);
		}
		return true;
	}
	private boolean additiveExpr_sempred(AdditiveExprContext _localctx, int predIndex) {
		switch (predIndex) {
		case 4:
			return precpred(_ctx, 1);
		}
		return true;
	}
	private boolean shiftExpr_sempred(ShiftExprContext _localctx, int predIndex) {
		switch (predIndex) {
		case 5:
			return precpred(_ctx, 1);
		}
		return true;
	}
	private boolean relationalExpr_sempred(RelationalExprContext _localctx, int predIndex) {
		switch (predIndex) {
		case 6:
			return precpred(_ctx, 1);
		}
		return true;
	}
	private boolean equalityExpr_sempred(EqualityExprContext _localctx, int predIndex) {
		switch (predIndex) {
		case 7:
			return precpred(_ctx, 1);
		}
		return true;
	}
	private boolean andExpr_sempred(AndExprContext _localctx, int predIndex) {
		switch (predIndex) {
		case 8:
			return precpred(_ctx, 1);
		}
		return true;
	}
	private boolean exclusiveOrExpr_sempred(ExclusiveOrExprContext _localctx, int predIndex) {
		switch (predIndex) {
		case 9:
			return precpred(_ctx, 1);
		}
		return true;
	}
	private boolean inclusiveOrExpr_sempred(InclusiveOrExprContext _localctx, int predIndex) {
		switch (predIndex) {
		case 10:
			return precpred(_ctx, 1);
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3=\u01a8\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"+
		"\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t \4!"+
		"\t!\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\5\2M\n\2\3\3\3\3\7\3Q\n\3"+
		"\f\3\16\3T\13\3\3\3\3\3\3\4\3\4\3\4\5\4[\n\4\3\4\3\4\5\4_\n\4\3\4\3\4"+
		"\5\4c\n\4\3\4\3\4\3\4\3\5\3\5\3\5\3\5\3\5\3\5\3\6\3\6\3\6\3\6\3\6\3\6"+
		"\3\6\5\6u\n\6\3\7\3\7\3\7\3\7\3\7\3\7\5\7}\n\7\3\7\5\7\u0080\n\7\3\b\3"+
		"\b\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\7\t\u008c\n\t\f\t\16\t\u008f\13\t\3"+
		"\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\7\t\u009f\n\t\f"+
		"\t\16\t\u00a2\13\t\5\t\u00a4\n\t\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3"+
		"\n\3\n\3\n\3\n\7\n\u00b3\n\n\f\n\16\n\u00b6\13\n\3\13\3\13\3\13\5\13\u00bb"+
		"\n\13\3\f\3\f\3\f\3\f\3\f\3\f\7\f\u00c3\n\f\f\f\16\f\u00c6\13\f\3\r\3"+
		"\r\3\r\3\r\3\r\3\r\7\r\u00ce\n\r\f\r\16\r\u00d1\13\r\3\16\3\16\3\16\3"+
		"\16\3\16\3\16\7\16\u00d9\n\16\f\16\16\16\u00dc\13\16\3\17\3\17\3\17\3"+
		"\17\3\17\3\17\7\17\u00e4\n\17\f\17\16\17\u00e7\13\17\3\20\3\20\3\20\3"+
		"\20\3\20\3\20\7\20\u00ef\n\20\f\20\16\20\u00f2\13\20\3\21\3\21\3\21\3"+
		"\21\3\21\3\21\7\21\u00fa\n\21\f\21\16\21\u00fd\13\21\3\22\3\22\3\22\3"+
		"\22\3\22\3\22\7\22\u0105\n\22\f\22\16\22\u0108\13\22\3\23\3\23\3\23\3"+
		"\23\3\23\3\23\7\23\u0110\n\23\f\23\16\23\u0113\13\23\3\24\3\24\3\24\3"+
		"\24\3\24\5\24\u011a\n\24\3\25\3\25\3\25\3\25\3\25\5\25\u0121\n\25\3\26"+
		"\3\26\3\26\3\26\3\26\5\26\u0128\n\26\3\27\5\27\u012b\n\27\3\27\3\27\3"+
		"\30\3\30\3\30\3\30\3\30\3\30\3\31\3\31\7\31\u0137\n\31\f\31\16\31\u013a"+
		"\13\31\3\31\3\31\3\31\3\31\7\31\u0140\n\31\f\31\16\31\u0143\13\31\3\31"+
		"\3\31\3\31\5\31\u0148\n\31\3\32\3\32\3\32\3\32\3\32\3\32\3\32\3\32\3\32"+
		"\3\32\3\32\3\32\5\32\u0156\n\32\3\33\3\33\3\33\3\33\3\33\3\33\5\33\u015e"+
		"\n\33\3\34\3\34\7\34\u0162\n\34\f\34\16\34\u0165\13\34\3\34\3\34\3\34"+
		"\3\35\3\35\7\35\u016c\n\35\f\35\16\35\u016f\13\35\3\35\3\35\3\35\3\35"+
		"\3\35\3\36\3\36\7\36\u0178\n\36\f\36\16\36\u017b\13\36\3\36\3\36\3\36"+
		"\7\36\u0180\n\36\f\36\16\36\u0183\13\36\3\36\3\36\3\36\5\36\u0188\n\36"+
		"\3\37\3\37\7\37\u018c\n\37\f\37\16\37\u018f\13\37\3\37\3\37\3\37\5\37"+
		"\u0194\n\37\3\37\3\37\3 \3 \7 \u019a\n \f \16 \u019d\13 \3 \3 \3 \5 \u01a2"+
		"\n \3 \3 \3!\3!\3!\2\13\22\26\30\32\34\36 \"$\"\2\4\6\b\n\f\16\20\22\24"+
		"\26\30\32\34\36 \"$&(*,.\60\62\64\668:<>@\2\13\4\2..99\3\2\23\24\3\2\23"+
		"\30\3\2\31\33\3\2\27\30\3\2\34\35\3\2\36!\3\2\"#\4\2\63\64\678\u01c0\2"+
		"L\3\2\2\2\4N\3\2\2\2\6W\3\2\2\2\bg\3\2\2\2\nm\3\2\2\2\f\177\3\2\2\2\16"+
		"\u0081\3\2\2\2\20\u00a3\3\2\2\2\22\u00a5\3\2\2\2\24\u00ba\3\2\2\2\26\u00bc"+
		"\3\2\2\2\30\u00c7\3\2\2\2\32\u00d2\3\2\2\2\34\u00dd\3\2\2\2\36\u00e8\3"+
		"\2\2\2 \u00f3\3\2\2\2\"\u00fe\3\2\2\2$\u0109\3\2\2\2&\u0119\3\2\2\2(\u0120"+
		"\3\2\2\2*\u0127\3\2\2\2,\u012a\3\2\2\2.\u012e\3\2\2\2\60\u0147\3\2\2\2"+
		"\62\u0155\3\2\2\2\64\u015d\3\2\2\2\66\u015f\3\2\2\28\u0169\3\2\2\2:\u0187"+
		"\3\2\2\2<\u0189\3\2\2\2>\u0197\3\2\2\2@\u01a5\3\2\2\2BM\5\4\3\2CM\5\6"+
		"\4\2DM\5\b\5\2EM\5\n\6\2FM\5\f\7\2GM\5> \2HI\5\16\b\2IJ\7\3\2\2JM\3\2"+
		"\2\2KM\7\3\2\2LB\3\2\2\2LC\3\2\2\2LD\3\2\2\2LE\3\2\2\2LF\3\2\2\2LG\3\2"+
		"\2\2LH\3\2\2\2LK\3\2\2\2M\3\3\2\2\2NR\7\4\2\2OQ\5\2\2\2PO\3\2\2\2QT\3"+
		"\2\2\2RP\3\2\2\2RS\3\2\2\2SU\3\2\2\2TR\3\2\2\2UV\7\5\2\2V\5\3\2\2\2WX"+
		"\7\6\2\2XZ\7\7\2\2Y[\5\16\b\2ZY\3\2\2\2Z[\3\2\2\2[\\\3\2\2\2\\^\7\3\2"+
		"\2]_\5\16\b\2^]\3\2\2\2^_\3\2\2\2_`\3\2\2\2`b\7\3\2\2ac\5\16\b\2ba\3\2"+
		"\2\2bc\3\2\2\2cd\3\2\2\2de\7\b\2\2ef\5\2\2\2f\7\3\2\2\2gh\7\t\2\2hi\7"+
		"\7\2\2ij\5\16\b\2jk\7\b\2\2kl\5\2\2\2l\t\3\2\2\2mn\7\n\2\2no\7\7\2\2o"+
		"p\5\16\b\2pq\7\b\2\2qt\5\2\2\2rs\7\13\2\2su\5\2\2\2tr\3\2\2\2tu\3\2\2"+
		"\2u\13\3\2\2\2vw\7\f\2\2w\u0080\7\3\2\2xy\7\r\2\2y\u0080\7\3\2\2z|\7\16"+
		"\2\2{}\5\16\b\2|{\3\2\2\2|}\3\2\2\2}~\3\2\2\2~\u0080\7\3\2\2\177v\3\2"+
		"\2\2\177x\3\2\2\2\177z\3\2\2\2\u0080\r\3\2\2\2\u0081\u0082\5*\26\2\u0082"+
		"\17\3\2\2\2\u0083\u00a4\79\2\2\u0084\u0085\79\2\2\u0085\u00a4\7\17\2\2"+
		"\u0086\u0087\79\2\2\u0087\u0088\7\7\2\2\u0088\u008d\5\16\b\2\u0089\u008a"+
		"\7\20\2\2\u008a\u008c\5\16\b\2\u008b\u0089\3\2\2\2\u008c\u008f\3\2\2\2"+
		"\u008d\u008b\3\2\2\2\u008d\u008e\3\2\2\2\u008e\u0090\3\2\2\2\u008f\u008d"+
		"\3\2\2\2\u0090\u0091\7\b\2\2\u0091\u00a4\3\2\2\2\u0092\u00a4\5@!\2\u0093"+
		"\u0094\7\7\2\2\u0094\u0095\5\16\b\2\u0095\u0096\7\b\2\2\u0096\u00a4\3"+
		"\2\2\2\u0097\u0098\7+\2\2\u0098\u00a0\t\2\2\2\u0099\u009a\7\21\2\2\u009a"+
		"\u009b\5\16\b\2\u009b\u009c\7\22\2\2\u009c\u009f\3\2\2\2\u009d\u009f\7"+
		",\2\2\u009e\u0099\3\2\2\2\u009e\u009d\3\2\2\2\u009f\u00a2\3\2\2\2\u00a0"+
		"\u009e\3\2\2\2\u00a0\u00a1\3\2\2\2\u00a1\u00a4\3\2\2\2\u00a2\u00a0\3\2"+
		"\2\2\u00a3\u0083\3\2\2\2\u00a3\u0084\3\2\2\2\u00a3\u0086\3\2\2\2\u00a3"+
		"\u0092\3\2\2\2\u00a3\u0093\3\2\2\2\u00a3\u0097\3\2\2\2\u00a4\21\3\2\2"+
		"\2\u00a5\u00a6\b\n\1\2\u00a6\u00a7\5\20\t\2\u00a7\u00b4\3\2\2\2\u00a8"+
		"\u00a9\f\5\2\2\u00a9\u00aa\7\21\2\2\u00aa\u00ab\5\16\b\2\u00ab\u00ac\7"+
		"\22\2\2\u00ac\u00b3\3\2\2\2\u00ad\u00ae\f\4\2\2\u00ae\u00af\7-\2\2\u00af"+
		"\u00b3\5\20\t\2\u00b0\u00b1\f\3\2\2\u00b1\u00b3\t\3\2\2\u00b2\u00a8\3"+
		"\2\2\2\u00b2\u00ad\3\2\2\2\u00b2\u00b0\3\2\2\2\u00b3\u00b6\3\2\2\2\u00b4"+
		"\u00b2\3\2\2\2\u00b4\u00b5\3\2\2\2\u00b5\23\3\2\2\2\u00b6\u00b4\3\2\2"+
		"\2\u00b7\u00bb\5\22\n\2\u00b8\u00b9\t\4\2\2\u00b9\u00bb\5\24\13\2\u00ba"+
		"\u00b7\3\2\2\2\u00ba\u00b8\3\2\2\2\u00bb\25\3\2\2\2\u00bc\u00bd\b\f\1"+
		"\2\u00bd\u00be\5\24\13\2\u00be\u00c4\3\2\2\2\u00bf\u00c0\f\3\2\2\u00c0"+
		"\u00c1\t\5\2\2\u00c1\u00c3\5\24\13\2\u00c2\u00bf\3\2\2\2\u00c3\u00c6\3"+
		"\2\2\2\u00c4\u00c2\3\2\2\2\u00c4\u00c5\3\2\2\2\u00c5\27\3\2\2\2\u00c6"+
		"\u00c4\3\2\2\2\u00c7\u00c8\b\r\1\2\u00c8\u00c9\5\26\f\2\u00c9\u00cf\3"+
		"\2\2\2\u00ca\u00cb\f\3\2\2\u00cb\u00cc\t\6\2\2\u00cc\u00ce\5\26\f\2\u00cd"+
		"\u00ca\3\2\2\2\u00ce\u00d1\3\2\2\2\u00cf\u00cd\3\2\2\2\u00cf\u00d0\3\2"+
		"\2\2\u00d0\31\3\2\2\2\u00d1\u00cf\3\2\2\2\u00d2\u00d3\b\16\1\2\u00d3\u00d4"+
		"\5\30\r\2\u00d4\u00da\3\2\2\2\u00d5\u00d6\f\3\2\2\u00d6\u00d7\t\7\2\2"+
		"\u00d7\u00d9\5\30\r\2\u00d8\u00d5\3\2\2\2\u00d9\u00dc\3\2\2\2\u00da\u00d8"+
		"\3\2\2\2\u00da\u00db\3\2\2\2\u00db\33\3\2\2\2\u00dc\u00da\3\2\2\2\u00dd"+
		"\u00de\b\17\1\2\u00de\u00df\5\32\16\2\u00df\u00e5\3\2\2\2\u00e0\u00e1"+
		"\f\3\2\2\u00e1\u00e2\t\b\2\2\u00e2\u00e4\5\32\16\2\u00e3\u00e0\3\2\2\2"+
		"\u00e4\u00e7\3\2\2\2\u00e5\u00e3\3\2\2\2\u00e5\u00e6\3\2\2\2\u00e6\35"+
		"\3\2\2\2\u00e7\u00e5\3\2\2\2\u00e8\u00e9\b\20\1\2\u00e9\u00ea\5\34\17"+
		"\2\u00ea\u00f0\3\2\2\2\u00eb\u00ec\f\3\2\2\u00ec\u00ed\t\t\2\2\u00ed\u00ef"+
		"\5\34\17\2\u00ee\u00eb\3\2\2\2\u00ef\u00f2\3\2\2\2\u00f0\u00ee\3\2\2\2"+
		"\u00f0\u00f1\3\2\2\2\u00f1\37\3\2\2\2\u00f2\u00f0\3\2\2\2\u00f3\u00f4"+
		"\b\21\1\2\u00f4\u00f5\5\36\20\2\u00f5\u00fb\3\2\2\2\u00f6\u00f7\f\3\2"+
		"\2\u00f7\u00f8\7$\2\2\u00f8\u00fa\5\36\20\2\u00f9\u00f6\3\2\2\2\u00fa"+
		"\u00fd\3\2\2\2\u00fb\u00f9\3\2\2\2\u00fb\u00fc\3\2\2\2\u00fc!\3\2\2\2"+
		"\u00fd\u00fb\3\2\2\2\u00fe\u00ff\b\22\1\2\u00ff\u0100\5 \21\2\u0100\u0106"+
		"\3\2\2\2\u0101\u0102\f\3\2\2\u0102\u0103\7%\2\2\u0103\u0105\5 \21\2\u0104"+
		"\u0101\3\2\2\2\u0105\u0108\3\2\2\2\u0106\u0104\3\2\2\2\u0106\u0107\3\2"+
		"\2\2\u0107#\3\2\2\2\u0108\u0106\3\2\2\2\u0109\u010a\b\23\1\2\u010a\u010b"+
		"\5\"\22\2\u010b\u0111\3\2\2\2\u010c\u010d\f\3\2\2\u010d\u010e\7&\2\2\u010e"+
		"\u0110\5\"\22\2\u010f\u010c\3\2\2\2\u0110\u0113\3\2\2\2\u0111\u010f\3"+
		"\2\2\2\u0111\u0112\3\2\2\2\u0112%\3\2\2\2\u0113\u0111\3\2\2\2\u0114\u011a"+
		"\5$\23\2\u0115\u0116\5$\23\2\u0116\u0117\7\'\2\2\u0117\u0118\5&\24\2\u0118"+
		"\u011a\3\2\2\2\u0119\u0114\3\2\2\2\u0119\u0115\3\2\2\2\u011a\'\3\2\2\2"+
		"\u011b\u0121\5&\24\2\u011c\u011d\5&\24\2\u011d\u011e\7(\2\2\u011e\u011f"+
		"\5(\25\2\u011f\u0121\3\2\2\2\u0120\u011b\3\2\2\2\u0120\u011c\3\2\2\2\u0121"+
		")\3\2\2\2\u0122\u0128\5(\25\2\u0123\u0124\5\24\13\2\u0124\u0125\7)\2\2"+
		"\u0125\u0126\5*\26\2\u0126\u0128\3\2\2\2\u0127\u0122\3\2\2\2\u0127\u0123"+
		"\3\2\2\2\u0128+\3\2\2\2\u0129\u012b\5\62\32\2\u012a\u0129\3\2\2\2\u012a"+
		"\u012b\3\2\2\2\u012b\u012c\3\2\2\2\u012c\u012d\7\2\2\3\u012d-\3\2\2\2"+
		"\u012e\u012f\7*\2\2\u012f\u0130\79\2\2\u0130\u0131\7\4\2\2\u0131\u0132"+
		"\5\60\31\2\u0132\u0133\7\5\2\2\u0133/\3\2\2\2\u0134\u0138\t\2\2\2\u0135"+
		"\u0137\7,\2\2\u0136\u0135\3\2\2\2\u0137\u013a\3\2\2\2\u0138\u0136\3\2"+
		"\2\2\u0138\u0139\3\2\2\2\u0139\u013b\3\2\2\2\u013a\u0138\3\2\2\2\u013b"+
		"\u013c\79\2\2\u013c\u0148\7\3\2\2\u013d\u0141\t\2\2\2\u013e\u0140\7,\2"+
		"\2\u013f\u013e\3\2\2\2\u0140\u0143\3\2\2\2\u0141\u013f\3\2\2\2\u0141\u0142"+
		"\3\2\2\2\u0142\u0144\3\2\2\2\u0143\u0141\3\2\2\2\u0144\u0145\79\2\2\u0145"+
		"\u0146\7\3\2\2\u0146\u0148\5\60\31\2\u0147\u0134\3\2\2\2\u0147\u013d\3"+
		"\2\2\2\u0148\61\3\2\2\2\u0149\u0156\5\64\33\2\u014a\u0156\5<\37\2\u014b"+
		"\u0156\5.\30\2\u014c\u014d\5\64\33\2\u014d\u014e\5\62\32\2\u014e\u0156"+
		"\3\2\2\2\u014f\u0150\5<\37\2\u0150\u0151\5\62\32\2\u0151\u0156\3\2\2\2"+
		"\u0152\u0153\5.\30\2\u0153\u0154\5\62\32\2\u0154\u0156\3\2\2\2\u0155\u0149"+
		"\3\2\2\2\u0155\u014a\3\2\2\2\u0155\u014b\3\2\2\2\u0155\u014c\3\2\2\2\u0155"+
		"\u014f\3\2\2\2\u0155\u0152\3\2\2\2\u0156\63\3\2\2\2\u0157\u0158\5\66\34"+
		"\2\u0158\u0159\5\4\3\2\u0159\u015e\3\2\2\2\u015a\u015b\58\35\2\u015b\u015c"+
		"\5\4\3\2\u015c\u015e\3\2\2\2\u015d\u0157\3\2\2\2\u015d\u015a\3\2\2\2\u015e"+
		"\65\3\2\2\2\u015f\u0163\t\2\2\2\u0160\u0162\7,\2\2\u0161\u0160\3\2\2\2"+
		"\u0162\u0165\3\2\2\2\u0163\u0161\3\2\2\2\u0163\u0164\3\2\2\2\u0164\u0166"+
		"\3\2\2\2\u0165\u0163\3\2\2\2\u0166\u0167\79\2\2\u0167\u0168\7\17\2\2\u0168"+
		"\67\3\2\2\2\u0169\u016d\t\2\2\2\u016a\u016c\7,\2\2\u016b\u016a\3\2\2\2"+
		"\u016c\u016f\3\2\2\2\u016d\u016b\3\2\2\2\u016d\u016e\3\2\2\2\u016e\u0170"+
		"\3\2\2\2\u016f\u016d\3\2\2\2\u0170\u0171\79\2\2\u0171\u0172\7\7\2\2\u0172"+
		"\u0173\5:\36\2\u0173\u0174\7\b\2\2\u01749\3\2\2\2\u0175\u0179\t\2\2\2"+
		"\u0176\u0178\7,\2\2\u0177\u0176\3\2\2\2\u0178\u017b\3\2\2\2\u0179\u0177"+
		"\3\2\2\2\u0179\u017a\3\2\2\2\u017a\u017c\3\2\2\2\u017b\u0179\3\2\2\2\u017c"+
		"\u0188\79\2\2\u017d\u0181\t\2\2\2\u017e\u0180\7,\2\2\u017f\u017e\3\2\2"+
		"\2\u0180\u0183\3\2\2\2\u0181\u017f\3\2\2\2\u0181\u0182\3\2\2\2\u0182\u0184"+
		"\3\2\2\2\u0183\u0181\3\2\2\2\u0184\u0185\79\2\2\u0185\u0186\7\20\2\2\u0186"+
		"\u0188\5:\36\2\u0187\u0175\3\2\2\2\u0187\u017d\3\2\2\2\u0188;\3\2\2\2"+
		"\u0189\u018d\t\2\2\2\u018a\u018c\7,\2\2\u018b\u018a\3\2\2\2\u018c\u018f"+
		"\3\2\2\2\u018d\u018b\3\2\2\2\u018d\u018e\3\2\2\2\u018e\u0190\3\2\2\2\u018f"+
		"\u018d\3\2\2\2\u0190\u0193\79\2\2\u0191\u0192\7)\2\2\u0192\u0194\5\16"+
		"\b\2\u0193\u0191\3\2\2\2\u0193\u0194\3\2\2\2\u0194\u0195\3\2\2\2\u0195"+
		"\u0196\7\3\2\2\u0196=\3\2\2\2\u0197\u019b\t\2\2\2\u0198\u019a\7,\2\2\u0199"+
		"\u0198\3\2\2\2\u019a\u019d\3\2\2\2\u019b\u0199\3\2\2\2\u019b\u019c\3\2"+
		"\2\2\u019c\u019e\3\2\2\2\u019d\u019b\3\2\2\2\u019e\u01a1\79\2\2\u019f"+
		"\u01a0\7)\2\2\u01a0\u01a2\5\16\b\2\u01a1\u019f\3\2\2\2\u01a1\u01a2\3\2"+
		"\2\2\u01a2\u01a3\3\2\2\2\u01a3\u01a4\7\3\2\2\u01a4?\3\2\2\2\u01a5\u01a6"+
		"\t\n\2\2\u01a6A\3\2\2\2+LRZ^bt|\177\u008d\u009e\u00a0\u00a3\u00b2\u00b4"+
		"\u00ba\u00c4\u00cf\u00da\u00e5\u00f0\u00fb\u0106\u0111\u0119\u0120\u0127"+
		"\u012a\u0138\u0141\u0147\u0155\u015d\u0163\u016d\u0179\u0181\u0187\u018d"+
		"\u0193\u019b\u01a1";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}