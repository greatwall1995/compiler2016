package Checker;

import lib.*;
import java.util.*;
import AST.*;

/**
 * Created by Administrator on 2016/4/3.
 */
public class FuncTable {
	private static Map dict = new HashMap();
	public FuncTable() {}
	public void put(Symbol name, List<Type> type) {
		dict.put(name, type);
	}
	public void put(FuncDec f) {
		List<Type> list = new ArrayList<>();
		for (int i = 0; i < f.param.size(); ++i) {
			list.add(f.param.get(i).type);
		}
		put(f.name, list);
	}
	public boolean exist(Symbol name) {
		return dict.get(name) != null;
	}
	public boolean check(Symbol name, List<Type> type) {
		List<Type> list = (List<Type>)dict.get(name);
		if (list == null || list.size() != type.size()) return false;
		for (int i = 0; i < list.size(); ++i) {
			if (!list.get(i).equals(type.get(i))) {
				return false;
			}
		}
		return true;
	}
}
